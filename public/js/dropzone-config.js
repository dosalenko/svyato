var total_photos_counter = 0;
Dropzone.options.myDropzone = {
    uploadMultiple: false,
    maxFiles: 10,
    dictMaxFilesExceeded: 'Ви не можете завантажити більше файлів',

    parallelUploads: 2,
    maxFilesize: 10,
    previewTemplate: document.querySelector('#preview').innerHTML,
    addRemoveLinks: false,
    dictRemoveFile: 'видалити',
    dictFileTooBig: 'Максимум 10 Мб',
    timeout: 10000,

    init: function () {
        this.on("removedfile", function (file) {
            alert(file.name);
            // $.post({
            //     url: '/images-delete',
            //     data: {id: file.name, _token: $('[name="_token"]').val()},
            //     dataType: 'json',
            //     success: function (data) {
            //         total_photos_counter--;
            //         $("#counter").text("# " + total_photos_counter);
            //     }
            // });
        });
    },
    success: function (file, done) {


        if (done.limit === 10) {
            alert(done.limit_message);
        }

        //alert(done.message);


           window.location.reload();


        //total_photos_counter++;
        //$("#counter").text("# " + total_photos_counter);
    }
};