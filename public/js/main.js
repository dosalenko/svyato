$(document).ready(function () {


    $("#open-avatar, #open-bg").click(function () {
        $('#save_image').css('display','block');
    });

    $(".make_messages_read").click(function () {

       let from_id= $(this).attr("data-from") ;

        $.ajax({
            url: '/message/make_messages_read',
            data: {
                'from_id':from_id,

            },
            type: "GET",
            success: function (url) {


            },
        });

    });



    if($('span#error').length) {
      $('#authorization-modal').modal('show');
      if ($('span#error').data('tab') === 'login') {
        $('#authorization-registration').tab('show');
        $('#authorization-tab').tab('show');
      } else {
        $('#registration-tab').tab('show');
      }
    }
  $('.help-user .log-in').on('click', function (e) {
    e.preventDefault();
    $('#authorization-tab').tab('show')
  });
  $('.help-user .register-user').on('click', function (e) {
    e.preventDefault();
    $('#registration-tab').tab('show')
  });
  function onHashChange() {
    var hash = window.location.hash;

    if (hash) {
      // using ES6 template string syntax
      $(`[data-toggle="tab"][href="${hash}"]`);
    }

  }

    onHashChange();
    
    var url = location.href.replace(/\/$/, "");
    if (location.hash) {
    const hash = url.split("#");
    $('.user-tab-content a[href="#'+hash[1]+'"]').tab("show");
    url = location.href.replace(/\/#/, "#");
    history.replaceState(null, null, url);
  }

    $('.user-tab-content a[data-toggle="tab"]').on("click", function() {
    var newUrl;
    const hash = $(this).attr("href");
    if(hash == "#photo") {
      newUrl = url.split("#")[0] + hash;
    } else {
      newUrl = url.split("#")[0] + hash;
    }
    newUrl += "";
    history.replaceState(null, null, newUrl);
  });
    //hide listCategory
$('.bottom-listCategory').each(function(){
 var max = 7;
    if ($(this).find('li').length > max) {$(this).find('li:gt('+max+')').addClass('hide-b-category').hide().end().append('<li class="sub_listCategory"><span class="show-category"><i class="fas fa-plus"></i></span></li>');
        $('.sub_listCategory').click( function(){   $(this).siblings(':gt('+max+')').toggle('500');
            if ( $('.show-category').length ) {
                $(this).html('<span class="hide-category"><i class="fas fa-minus"></i></span>');
                 $(".search-content").addClass("active");
            } else {
                $(this).html('<span class="show-category"><i class="fas fa-plus"></i></span>');
                $(".search-content").removeClass("active");
            };
            
        });
    };
       
});
        
            $('.show-u-pro').click( function(){
                $('.header-center').toggleClass('pro-active');
                $('.pro-block').toggleClass('show-pro');
                $('.show-u-pro').toggleClass('hide-u-pro');
              
            })

 
    
  $(".toggle-cities").click(function () {
        $(".additionally-city").addClass("active");
        $(".s-btn").addClass("active");
        $("header").addClass("active");
        $(".additionally-category").addClass("active");
        $(".toggle-s-cities").addClass("active");
        $(".toggle-s-calculation").addClass("active");
        $("fieldset").addClass("active");
        $(".mask").addClass("active");
    });
    $(".city-select .fa-city").click(function () {
        $(".additionally-city").addClass("active");
        $(".s-btn").addClass("active");
        $("header").addClass("active");
        $(".additionally-category").addClass("active");
        $(".toggle-s-cities").addClass("active");
        $(".toggle-s-calculation").addClass("active");
        $("fieldset").addClass("active");
        $(".mask").addClass("active");
    });
    $(".close-city-additionally").click(function () {
        $(".additionally-city").removeClass("active");
        $(".s-btn").removeClass("active");
        $("header").removeClass("active");
        $(".additionally-category").removeClass("active");
        $(".toggle-s-cities").removeClass("active");
        $(".toggle-s-calculation").removeClass("active");
        $("fieldset").removeClass("active");
        $(".mask").removeClass("active");
    });
    $(".category-select .fa-user-tag").click(function () {
        $(".additionally-f-category ").addClass("active");
        $(".s-btn").addClass("active");
        $("header").addClass("active");
        /*$(".additionally-category").addClass("active");*/
        $(".toggle-s-cities").addClass("active");
        $(".toggle-s-calculation").addClass("active");
        $(".search-cities-block").addClass("active");
        $(".mask").addClass("active");
        
    });
  $(".close-category-additionally").click(function () {
        $(".additionally-f-category ").removeClass("active");
        $(".s-btn").removeClass("active");
        $("header").removeClass("active");
        $(".additionally-category").removeClass("active");
        $(".toggle-s-cities").removeClass("active");
        $(".toggle-s-calculation").removeClass("active");
        $(".search-cities-block").removeClass("active");
        $(".mask").removeClass("active");
    });
    $(".toggle-s-calculation").click(function () {
        $(".calculation-modal").addClass("show");
        $("header").addClass("active");
        $(".header-center ").addClass("opacity");
         $(".mask").addClass("active");
    });
     $(".close-calculation-modal").click(function () {
        $(".calculation-modal").removeClass("show");
         $("header").removeClass("active");
         $(".header-center ").removeClass("opacity");
          $(".mask").removeClass("active");
    });
      $("input[name$='rating-01']").click(function() {
        var raiting = $(this).val();
        var r_description = $(this).data("description");
        $("#rating").text(raiting);
        $("#rating-txt").text(r_description);
       
      });
    $(".user-active .nav-link").click(function() {
        $(".user-active  .drop-menu").toggleClass("active");
      });
    if ($(".body").hasClass("modal-open") ) {
        $(".header").addClass("active");
      };


    
   /* 
    $('.select-category').selectpicker({
        noneSelectedText: 'Оберіть категорію для пошуку...',
        noneResultsText: '"Нема результатів за словом - {0}"',
        selectAllText: 'Відмітити всі',
        deselectAllText: 'Скинути відмічені'
    });
    $('.select-sity').selectpicker({
        noneSelectedText: 'Оберіть місто для пошуку...',
        noneResultsText: '"Нема результатів за словом - {0}"',
        selectAllText: 'Відмітити всі',
        deselectAllText: 'Скинути відмічені',
        enableClickableOptGroups: true
    });
*/
/**
  var typed = new Typed(".category-dropdown .multiselect__input", {
    strings: ["Фотограф", "Відеоператор", "Тамада", "Ресторан"],  
    typeSpeed: 100,
    backSpeed: 75,
    backDelay: 100,
    startDelay: 500,
    loop: true
  });

document.querySelector(' .multiselect__input').addEventListener('click', function() {
  typed.destroy();
});
var typed = new Typed(".city-dropdown .multiselect__input", {
    strings: ["Київ", "Одеса", "Львів", "Тернопіль"],  
    typeSpeed: 100,
    backSpeed: 75,
    backDelay: 100,
    startDelay: 500,
    loop: true
  });
document.querySelector('.multiselect__input').addEventListener('click', function() {
  typed.destroy();
});*/
/*
*/
//печатающий текст
    /*
  var typed = new Typed(".typed", {
    strings: ["Фотографи", "Відеоператори", "Музиканти", "Тамада"],  
    typeSpeed: 100,
    backSpeed: 75,
    backDelay: 100,
    startDelay: 500,
    loop: true
  });
document.querySelector('.typed').addEventListener('click', function() {
  typed.destroy();
});
document.querySelector('.dropdown-toggle').addEventListener('click', function() {
  typed.reset();
  typed.start();
});*/


    //зв'язок селектів


    //mask header
    var image = document.querySelector('.main-img-mask');
    var imageCanvas = document.createElement('canvas');
    var imageCanvasContext = imageCanvas.getContext('2d');
    var lineCanvas = document.createElement('canvas');
    var lineCanvasContext = lineCanvas.getContext('2d');
    var pointLifetime = 1000;
    var points = [];

    if (image.complete) {
        start();
    } else {
        image.onload = start;
    }

    /**
     * Attaches event listeners and starts the effect.
     */
    function start() {
        document.addEventListener('mousemove', onMouseMove);
        window.addEventListener('resize', resizeCanvases);
        document.querySelector('.dot-background').appendChild(imageCanvas);
        resizeCanvases();
        tick();
    }

    /**
     * Records the user's cursor position.
     *
     * @param {!MouseEvent} event
     */
    function onMouseMove(event) {
        points.push({
            time: Date.now(),
            x: event.clientX,
            y: event.clientY
        });
    }

    /**
     * Resizes both canvases to fill the window.
     */
    function resizeCanvases() {
        imageCanvas.width = lineCanvas.width = window.innerWidth;
        imageCanvas.height = lineCanvas.height = window.innerHeight;
    }

    /**
     * The main loop, called at ~60hz.
     */
    function tick() {
        // Remove old points
        points = points.filter(function (point) {
            var age = Date.now() - point.time;
            return age < pointLifetime;
        });

        drawLineCanvas();
        drawImageCanvas();
        requestAnimationFrame(tick);
    }

    /**
     * Draws a line using the recorded cursor positions.
     *
     * This line is used to mask the original image.
     */
    function drawLineCanvas() {
        var minimumLineWidth = 25;
        var maximumLineWidth = 100;
        var lineWidthRange = maximumLineWidth - minimumLineWidth;
        var maximumSpeed = 50;

        lineCanvasContext.clearRect(0, 0, lineCanvas.width, lineCanvas.height);
        lineCanvasContext.lineCap = 'round';
        lineCanvasContext.shadowBlur = 30;
        lineCanvasContext.shadowColor = '#000';

        for (var i = 1; i < points.length; i++) {
            var point = points[i];
            var previousPoint = points[i - 1];

            // Change line width based on speed
            var distance = getDistanceBetween(point, previousPoint);
            var speed = Math.max(0, Math.min(maximumSpeed, distance));
            var percentageLineWidth = (maximumSpeed - speed) / maximumSpeed;
            lineCanvasContext.lineWidth = minimumLineWidth + percentageLineWidth * lineWidthRange;

            // Fade points as they age
            var age = Date.now() - point.time;
            var opacity = (pointLifetime - age) / pointLifetime;
            lineCanvasContext.strokeStyle = 'rgba(0, 0, 0, ' + opacity + ')';

            lineCanvasContext.beginPath();
            lineCanvasContext.moveTo(previousPoint.x, previousPoint.y);
            lineCanvasContext.lineTo(point.x, point.y);
            lineCanvasContext.stroke();
        }
    }

    /**
     * @param {{x: number, y: number}} a
     * @param {{x: number, y: number}} b
     * @return {number} The distance between points a and b
     */
    function getDistanceBetween(a, b) {
        return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
    }

    /**
     * Draws the original image, masked by the line drawn in drawLineToCanvas.
     */
    function drawImageCanvas() {
        // Emulate background-size: cover
        var width = imageCanvas.width;
        var height = imageCanvas.width / image.naturalWidth * image.naturalHeight;

        if (height < imageCanvas.height) {
            width = imageCanvas.height / image.naturalHeight * image.naturalWidth;
            height = imageCanvas.height;
        }

        imageCanvasContext.clearRect(0, 0, imageCanvas.width, imageCanvas.height);
        imageCanvasContext.globalCompositeOperation = 'source-over';
        imageCanvasContext.drawImage(image, 0, 0, width, height);
        imageCanvasContext.globalCompositeOperation = 'destination-in';
        imageCanvasContext.drawImage(lineCanvas, 0, 0);
    }

    if (window.location.pathname.includes('blog') && document.querySelector('#article-ckeditor')) {
      CKEDITOR.replace( 'article-ckeditor', {   toolbar: [
          { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
          { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
          { name: 'insert', items: [ 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
          '/',
          { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
          { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
          { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        ]} );
    }

    $('.js-add-like').on('click', function(ev) {

        let user_id_liker=$(this).closest("form").find("input[name='user_id']").val();
        let artist_id_liked=$(this).closest("form").find("input[name='artist_id']").val();


        $.ajax({
            url: '/add-like',
            data: {
                'artist_id':artist_id_liked,

            },
            type: "GET",
            success: function (url) {

            },
        });



       // $(ev.target).parent().submit();
    });
    if ( $('.js-search').length) {

      $('.js-price').on('click', function () {
        $('.range-value.min').data('price', 1);
        $('.range-value.max').data('price', 1);
      });

      var  searchAction = function(event) {
        event.preventDefault();
        var $form = $('.js-search');
        $('.js-category').find('.multiselect__tag').each(function () {
          $form.append(`<input type="hidden" name="category[]" value="${$(this).text().trim()}">`);
        });

        $('.js-city').find('.multiselect__tag').each(function (index) {
          $form.append(`<input type="hidden" name="cities[]" value="${$(this).text().trim()}">`);
        });

        if ($form.find('.range-value.min').data('price') == 1) {
          $form.append(`<input type="hidden" name="price_min" value="${$form.find('.range-value.min').text().trim()}">`);
          $form.append(`<input type="hidden" name="price_max" value="${$form.find('.range-value.max').text().trim()}">`);
        }
        $form.submit();
      };
      $('.search-btn').on('click', searchAction);
      $('.send-btn').on('click', searchAction);
    }

    $('.js-like-modal').on('click', function (ev) {
      $('#likes_cnt').text(ev.target.dataset.number);
      $('.js-modal-like-content').html('');
      var users = JSON.parse(ev.target.dataset.users);
      if (users) {
        for(var k in users) {
          $('.js-modal-like-content').append(`<div class="user-block"><a href="/user-page/${users[k].email}"><img src="${users[k].image}">${users[k].email}</a></div>`);

        }
      }
    });

    if (window.location.pathname.includes('user-page')) {
        var widget = SC.Widget(document.getElementById('audio-user'));
        $('a.iframe').click(function() {
          widget.isPaused(function(pause) {
            if (pause == false) {
              widget.pause();
            }
          });
        });
        $("a.lightbox").fancybox({
          'transitionIn': 'elastic',
          'transitionOut': 'elastic',
          'speedIn': 600,
          'speedOut': 200,
          'overlayShow': false
        });
    }
});
