let vm = new Vue({
    components: {
        Multiselect: VueMultiselect.default,
        'vueSlider': window['vue-slider-component'],
    },
    data() {
        let lang;
        if (window.location.href.indexOf("ru") > -1) {
            lang = 'ru';
        } else {
            lang = 'ua';
        }

        let dataObject = {
            message: "Vue is loaded.",
            // multiselect
            min: 10,
            max: 2000,
            minValue: 50,
            maxValue: 2000,
            step: 5,
            totalSteps: 0,
            percentPerStep: 1,
            trackWidth: null,
            isDragging: false,
            pos: {
                curTrack: null
            },
            //selected: {category_ua:'Тамада'},
            selected: null,
            // listCity: [{
            //     region: "Область 1",
            //     img_region: "https://images.pexels.com/photos/356830/pexels-photo-356830.jpeg",
            //     city: [{
            //         name: "Город 1"
            //     },
            //         {
            //             name: "Город 2"
            //         },
            //         {
            //             name: "Город 3"
            //         }
            //     ]
            // },

            //
            //
            // ],
            listCity: [],
            valueCity: [],
            listCategory: [],
            valueCategory: [],
        };
        fetch('/categories/' + lang, {
            method: 'get',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
            },
        }).then(function (response) {
            return response.json()
        })
            .then((json) => {
                dataObject.listCategory = json;
                return dataObject;
            });

        fetch('/cities/' + lang, {
            method: 'get',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content,
            },
        }).then(function (response) {
            return response.json()
        })
            .then((json) => {
                dataObject.listCity = json;
                return dataObject;
            });
        return dataObject;
    },
    methods: {

        dispatchAction(option){
            //alert(this.selected);
                this.selected = null;


        },
        updateSelected (newSelected) {
           this.selected = newSelected;
            console.log(this.selected);
         },
        setCategorybox(event, el) {
            if (event.target.checked)
                this.valueCategory.push(el);
            else
                this.valueCategory = this.valueCategory.filter(i => i !== el)
        },
        setRegionbox(event, el) {
            if (event.target.checked)
                this.valueCity.push(...el.city);
            else
                this.valueCity = this.valueCity.filter(i => !el.city.find(k => k.name === i.name))
        },
        moveTrack(track, ev) {

            let percentInPx = this.getPercentInPx();

            let trackX = Math.round(this.$refs._vpcTrack.getBoundingClientRect().left);
            let clientX = ev.clientX;
            let moveDiff = clientX - trackX;

            let moveInPct = moveDiff / percentInPx
            // console.log(moveInPct)

            if (moveInPct < 1 || moveInPct > 100) return;
            let value = (Math.round(moveInPct / this.percentPerStep) * this.step) + this.min;
            if (track === 'track1') {
                if (value >= (this.maxValue - this.step)) return;
                this.minValue = value;
            }

            if (track === 'track2') {
                if (value <= (this.minValue + this.step)) return;
                this.maxValue = value;
            }

            this.$refs[track].style.left = moveInPct + '%';
            this.setTrackHightlight()

        },
        mousedown(ev, track) {

            if (this.isDragging) return;
            this.isDragging = true;
            this.pos.curTrack = track;
        },

        touchstart(ev, track) {
            this.mousedown(ev, track)
        },

        mouseup(ev, track) {
            if (!this.isDragging) return;
            this.isDragging = false
        },

        touchend(ev, track) {
            this.mouseup(ev, track)
        },

        mousemove(ev, track) {
            if (!this.isDragging) return;
            this.moveTrack(track, ev)
        },

        touchmove(ev, track) {
            this.mousemove(ev.changedTouches[0], track)
        },

        valueToPercent(value) {
            return ((value - this.min) / this.step) * this.percentPerStep
        },

        setTrackHightlight() {
            if (this.$refs.trackHighlight) {
                this.$refs.trackHighlight.style.left = this.valueToPercent(this.minValue) + '%';
                this.$refs.trackHighlight.style.width = (this.valueToPercent(this.maxValue) - this.valueToPercent(this.minValue)) + '%';
            }
        },

        getPercentInPx() {
            let trackWidth = this.$refs._vpcTrack.offsetWidth;
            let oneStepInPx = trackWidth / this.totalSteps;
            // 1 percent in px
            let percentInPx = oneStepInPx / this.percentPerStep;

            return percentInPx;
        },

        setClickMove(ev) {
            let track1Left = this.$refs.track1.getBoundingClientRect().left;
            let track2Left = this.$refs.track2.getBoundingClientRect().left;
            // console.log('track1Left', track1Left)
            if (ev.clientX < track1Left) {
                this.moveTrack('track1', ev)
            } else if ((ev.clientX - track1Left) < (track2Left - ev.clientX)) {
                this.moveTrack('track1', ev)
            } else {
                this.moveTrack('track2', ev)
            }
        },
    },
    mounted() {
        // calc per step value
        this.totalSteps = (this.max - this.min) / this.step;

        // percent the track button to be moved on each step
        this.percentPerStep = 100 / this.totalSteps;
        // console.log('percentPerStep', this.percentPerStep)

        let track1 = document.querySelector('.track1');
        if (track1) {
            // set track1 initilal
            track1.style.left = this.valueToPercent(this.minValue) + '%';
        }
        let track2 = document.querySelector('.track2');
        if (track2) {
            // track2 initial position
            track2.style.left = this.valueToPercent(this.maxValue) + '%';
        }

        // set initila track highlight
        this.setTrackHightlight()

        var self = this;

        ['mouseup', 'mousemove'].forEach(type => {
            document.body.addEventListener(type, (ev) => {
                // ev.preventDefault();
                if (self.isDragging && self.pos.curTrack) {
                    self[type](ev, self.pos.curTrack)
                }
            })
        });

        ['mousedown', 'mouseup', 'mousemove', 'touchstart', 'touchmove', 'touchend'].forEach(type => {
            if (document.querySelector('.track1')) {
                document.querySelector('.track1').addEventListener(type, (ev) => {
                    ev.stopPropagation();
                    self[type](ev, 'track1')
                })
            }

            if (document.querySelector('.track2')) {
                document.querySelector('.track2').addEventListener(type, (ev) => {
                    ev.stopPropagation();
                    self[type](ev, 'track2')
                })
            }
        })

        // on track clik
        // determine direction based on click proximity
        // determine percent to move based on track.clientX - click.clientX
        if (document.querySelector('.track')) {
            document.querySelector('.track').addEventListener('click', function (ev) {
                ev.stopPropagation();
                self.setClickMove(ev)

            });
        }

        if (document.querySelector('.track-highlight')) {
            document.querySelector('.track-highlight').addEventListener('click', function (ev) {
                ev.stopPropagation();
                self.setClickMove(ev)

            });
        }
    },

    el: "#main-app",

})
