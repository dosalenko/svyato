(function ($) {
    "use strict";
    /* -------------------------------------------------------------------------

    	FORM ELEMENTS

    ------------------------------------------------------------------------- */
    // SELECT BOX
    $.fn.uouSelectBox = function () {
        var self = $(this)
            , select = self.find('select');
        self.prepend('<ul class="select-clone custom-list"></ul>');
        var placeholder = select.data('placeholder') ? select.data('placeholder') : select.find('option:eq(0)').text()
            , clone = self.find('.select-clone');
        self.prepend('<input class="value-holder" type="text" disabled="disabled" placeholder="' + placeholder + '"><i class="fa fa-chevron-down"></i>');
        var value_holder = self.find('.value-holder');
        // INPUT PLACEHOLDER FIX FOR IE
        if ($.fn.placeholder) {
            self.find('input, textarea').placeholder();
        }
        // CREATE CLONE LIST
        select.find('option').each(function () {
            if ($(this).attr('value')) {
                clone.append('<li data-value="' + $(this).val() + '">' + $(this).text() + '</li>');
            }
        });
        // TOGGLE LIST
        self.click(function () {
            var media_query_breakpoint = uouMediaQueryBreakpoint();
            if (media_query_breakpoint > 991) {
                clone.slideToggle(100);
                self.toggleClass('active');
            }
        });
        // CLICK
        clone.find('li').click(function () {
            value_holder.val($(this).text());
            select.find('option[value="' + $(this).attr('data-value') + '"]').attr('selected', 'selected');
            // IF LIST OF LINKS
            if (self.hasClass('links')) {
                window.location.href = select.val();
            }
        });
        // HIDE LIST
        self.bind('clickoutside', function (event) {
            clone.slideUp(100);
        });
        // LIST OF LINKS
        if (self.hasClass('links')) {
            select.change(function () {
                window.location.href = select.val();
            });
        }
    };
    /* -------------------------------------------------------------------------

    	LIGHTBOX

    ------------------------------------------------------------------------- */
    // FUNCTION
    $.fn.uouInitLightboxes = function () {
        if ($.fn.magnificPopup) {
            $(this).find('a.lightbox').each(function () {
                var self = $(this)
                    , lightbox_group = self.data('lightbox-group') ? self.data('lightbox-group') : false;
                if (lightbox_group) {
                    $('a.lightbox[data-lightbox-group="' + lightbox_group + '"]').magnificPopup({
                        type: 'image'
                        , removalDelay: 300
                        , mainClass: 'mfp-fade'
                        , gallery: {
                            enabled: true
                        }
                    });
                }
                else {
                    self.magnificPopup({
                        type: 'image'
                    });
                }
            });
        }
    };
    /* -------------------------------------------------------------------------

    	MEDIA QUERY BREAKPOINT

    ------------------------------------------------------------------------- */
    var uouMediaQueryBreakpoint = function () {
        if ($('#media-query-breakpoint').length < 1) {
            $('body').append('<var id="media-query-breakpoint"><span></span></var>');
        }
        var value = $('#media-query-breakpoint').css('content');
        if (typeof value !== 'undefined') {
            value = value.replace("\"", "").replace("\"", "").replace("\'", "").replace("\'", "");
            if (isNaN(parseInt(value, 10))) {
                $('#media-query-breakpoint span').each(function () {
                    value = window.getComputedStyle(this, ':before').content;
                });
                value = value.replace("\"", "").replace("\"", "").replace("\'", "").replace("\'", "");
            }
        }
        else {
            value = 1199;
        }
        return value;
    };
    $(document).ready(function () {
        /* -----------------------------------------------------------------------------



        	GENERAL



        ----------------------------------------------------------------------------- */
        // GET ACTUAL MEDIA QUERY BREAKPOINT
        var media_query_breakpoint = uouMediaQueryBreakpoint();
        $('.radio-input').each(function () {
            $(this).uouRadioInput();
        });
        $('.select-box').each(function () {
            $(this).uouSelectBox();
        });
        $('.calendar-input').each(function () {
            var input = $(this).find('input')
                , dateformat = input.data('dateformat') ? input.data('dateformat') : 'm/d/y'
                , icon = $(this).find('.fa')
                , widget = input.datepicker('widget');
            input.datepicker({
                dateFormat: dateformat
                , minDate: 0
                , beforeShow: function () {
                    input.addClass('active');
                }
                , onClose: function () {
                    input.removeClass('active');
                    // TRANSPLANT WIDGET BACK TO THE END OF BODY IF NEEDED
                    widget.hide();
                    if (!widget.parent().is('body')) {
                        widget.detach().appendTo($('body'));
                    }
                }
            });
            icon.click(function () {
                input.focus();
            });
        });
        /* -----------------------------------------------------------------------------



        	BANNER



        ----------------------------------------------------------------------------- */
        /* -------------------------------------------------------------------------

        	BANNER BG

        ------------------------------------------------------------------------- */
        $('#banner .banner-bg').each(function () {
            var self = $(this)
                , images = self.find('.banner-bg-item');
            // SET BG IMAGES
            images.each(function () {
                var img = $(this).find('img');
                if (img.length > 0) {
                    $(this).css('background-image', 'url(' + img.attr('src') + ')');
                    img.hide();
                }
            });
            // INIT SLIDER
            if ($.fn.owlCarousel) {
                self.owlCarousel({
                    slideSpeed: 300
                    , pagination: false
                    , paginationSpeed: 400
                   
                    , singleItem: true
                    , addClassActive: true
                    , autoWidth:true
                    , afterMove: function () {
                        // ACTIVATE TAB
                        var active_index = self.find('.owl-item.active').index();
                        $('.banner-search-inner .tab-title:eq(' + active_index + ')').trigger('click');
                    }
                });
            }
            // SET DEFAULT IF NEEDED
            var active_tab_index = $('.banner-search-inner .tab-title.active').index();
            if (active_tab_index !== 0) {
                self.trigger('owl.jumpTo', active_tab_index);
            }
        });
        /* -------------------------------------------------------------------------

        	BANNER SEARCH

        ------------------------------------------------------------------------- */
        $('.banner-search-inner').each(function () {
            var self = $(this)
                , tabs = self.find('.tab-title')
                , contents = self.find('.tab-content');
            // TAB CLICK
            tabs.click(function () {
                if (!$(this).hasClass('active')) {
                    var index = $(this).index();
                    tabs.filter('.active').removeClass('active');
                    $(this).addClass('active');
                    contents.filter('.active').hide().removeClass('active');
                    contents.filter(':eq(' + index + ')').show().addClass('active');
                    // CHANGE BG
                    if ($.fn.owlCarousel) {
                        $('#banner .banner-bg').trigger('owl.goTo', index);
                    }
                }
            });
        });
    });
})(jQuery);