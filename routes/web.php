<?php

Route::group(['prefix' => LaravelLocalization::setLocale()], function () {


    Route::get('/', 'MainController@index')->name('main');
    Route::get('/about', 'AboutController@index')->name('about');
    Route::get('/contacts', 'ContactsController@index')->name('contacts');
    Route::get('/advertising', 'AdvertisingController@index')->name('ads');
    Route::post('/feedback', 'FeedbackController@save')->name('feedback');
    Route::get('/user-page/{user_id}', 'UserController@page')->name('user-page');

    //user cabinet

    Route::get('/lk-page/{user_id}', 'UserController@lk_page')->name('lk-page')->where(['user_id' => '[0-9]{1,8}']);

    Route::post('/lk-page/save_images/{user_id}', 'UserController@save_images')->name('save_images')->middleware(['auth']);
    Route::get('/lk-page/save_data/{user_id}', 'UserController@save_data')->name('save_data');

    Route::post('/lk-page/save_images_gallery/{user_id}', 'UserController@save_images_gallery')->name('save_images_gallery')->middleware(['auth']);
    Route::post('/lk-page/delete_images_gallery/{user_id}', 'UserController@delete_images_gallery')->name('delete_images_gallery')->middleware(['auth']);

    Route::post('/lk-page/save_video_gallery/{user_id}', 'UserController@save_video_gallery')->name('save_video_gallery')->middleware(['auth']);
    Route::post('/lk-page/delete_video_gallery/{user_id}', 'UserController@delete_video_gallery')->name('delete_video_gallery')->middleware(['auth']);

    Route::post('/lk-page/save_audio_gallery/{user_id}', 'UserController@save_audio_gallery')->name('save_audio_gallery')->middleware(['auth']);
    Route::post('/lk-page/delete_audio_gallery/{user_id}', 'UserController@delete_audio_gallery')->name('delete_audio_gallery')->middleware(['auth']);
    Route::post('/lk-page/edit_article/{user_id}', 'UserController@edit_article')->name('edit_article')->middleware(['auth']);

    //Route::post('/lk-page/feedback/{user_id}', 'UserController@feedback')->name('feedback')->middleware(['auth']);

    Route::post('/lk-page/messages/{user_id}', 'UserController@messages')->name('messages')->middleware(['auth']);

    Route::post('/message/create', 'MessageController@create')->name('message_create')->middleware(['auth']);
    Route::get('/message/make_messages_read', 'MessageController@make_messages_read')->name('make_messages_read')->middleware(['auth']);


    Route::get('/categories', 'CategoryController@list')->name('categories')->middleware('web');
    Route::get('/categories/ru', 'CategoryController@list')->name('categories')->middleware('web');
    Route::get('/categories/ua', 'CategoryController@list')->name('categories')->middleware('web');
    Route::get('/cities', 'CityController@list')->name('cities')->middleware('web');
    Route::get('/cities/ru', 'CityController@list')->name('cities')->middleware('web');
    Route::get('/cities/ua', 'CityController@list')->name('cities')->middleware('web');


    Route::post('/review-add', 'ArrtistFeedbackController@add')->name('add-review')->middleware(['web', 'auth']);
    Route::get('/blog', 'BlogController@index')->name('blog');
    Route::post('/add-post', 'ArticleController@save')->name('add-article')->middleware(['web', 'auth']);
    Route::get('/blog-post/{post_id}', 'BlogController@show')->name('blog-post');
    Route::get('/catalog', 'CatalogController@index')->name('catalog');
    Route::post('add-comment', 'CommentsController@add')->name('save-comment')->middleware(['web', 'auth']);
    Route::get('add-like', 'ArtistsHaveLikesController@add')->name('save-like')->middleware('auth');


});


Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {

    Route::get('/', 'AdminController@index');

    Route::resource('/artist', 'Admin\\ArtistController');
    Route::resource('/category', 'Admin\\CategoryController');
    Route::resource('/region', 'Admin\\RegionController');
    Route::resource('/article', 'Admin\\ArticleController');
    Route::resource('/feedback', 'Admin\\FeedbackController');
    Route::resource('/about', 'Admin\\AboutController');
    Route::resource('/contact', 'Admin\\ContactController');
    Route::resource('/seo', 'Admin\\SeoController');
});


Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');





