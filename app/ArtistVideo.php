<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistVideo extends Model
{
    protected $fillable = [
        'artist_id', 'video', 'likes'
    ];
}
