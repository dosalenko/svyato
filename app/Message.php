<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'from_id', 'to_id', 'text', 'is_admin', 'is_read'
    ];
}
