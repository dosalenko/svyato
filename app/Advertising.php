<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertising extends Model
{
    protected $fillable = [
        'year_price', 'month_price', 'active_pro',
    ];

    public function descriptions()
    {
        return $this->hasManyThrough('App\Descriptions', 'App\AdvertisingHaveDescription', 'advertising_id', 'id', 'id', 'description_id');
    }

    public function advertising_have_descriptions()
    {
        return $this->hasMany('App\AdvertisingHaveDescription', 'advertising_id', 'id');
    }
}
