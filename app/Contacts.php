<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $fillable = [
        'facebook', 'instagram', 'youtube', 'phone', 'email', 'viber', 'telegramm'
    ];
}
