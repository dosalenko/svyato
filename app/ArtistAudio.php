<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistAudio extends Model
{
    protected $fillable = [
        'artist_id', 'audio', 'likes'
    ];
}
