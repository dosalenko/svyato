<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticlesHaveCategory extends Model
{
    protected $fillable = [
        'article_category_id', 'article_id',
    ];
}
