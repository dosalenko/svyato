<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistFeedback extends Model
{
    protected $fillable = [
        'artist_id', 'text', 'likes', 'from_user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'from_user_id', 'id');
    }
}
