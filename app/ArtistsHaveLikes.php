<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistsHaveLikes extends Model
{
    protected $fillable = [
        'user_id', 'artist_id'
    ];
}
