<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistHaveCategory extends Model
{
    protected $fillable = [
        'artist_id', 'category_id'
    ];
}
