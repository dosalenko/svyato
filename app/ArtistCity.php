<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistCity extends Model
{
    protected $fillable = [
        'artist_id', 'city_id', 'region_id'
    ];

}
