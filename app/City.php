<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'name_ua', 'name_ru',  'name_en',  'region_id',
    ];

    public function region()
    {
        return $this->belongsTo('App\Region', 'id', 'region_id');
    }
}
