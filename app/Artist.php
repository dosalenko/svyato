<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $fillable = [
        'name', 'description', 'instagram', 'facebook', 'youtube', 'vimeo',
        'viber',  'facebook-messenger', 'telegramm', 'email', 'whatsapp',
        'phone', 'region_id',  'city_id', 'user_id',  'category_id', 'price', 'likes_cnt',
        'add_category_id', 'old_price', 'show_on_main',
        'name_ua','name_ru', 'description_ua','description_ru'
    ];

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function artistsCategories()
    {
        return $this->hasMany('App\ArtistHaveCategory');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function cities()
    {
        return $this->hasManyThrough('App\City', 'App\ArtistCity', 'city_id', 'id','id','artist_id');
    }

    public function articles()
    {
        return $this->hasManyThrough('App\Article', 'App\User','id','user_id','user_id','id');
    }

    public function photos()
    {
        return $this->hasMany('App\ArtistPhoto');
    }

    public function categories()
    {
        return $this->hasManyThrough('App\Category', 'App\ArtistHaveCategory','category_id','id','id', 'artist_id');
    }

    public function partyCategories()
    {
        return $this->hasManyThrough('App\PartyCategory','App\ArtistHavePartyCategory','party_category_id','id','id','artist_id');
    }

    public function userLikes()
    {
        return $this->hasManyThrough('App\User', 'App\ArtistsHaveLikes', 'user_id', 'id', 'id', 'artist_id');
    }

    public function ads()
    {
        return $this->belongsTo('App\Advertising', 'advertising_id','id');
    }


    public static function artistinfo($artist_id){

        $user = self::all()->where('id', $artist_id)->first();

        return $user;

    }
}
