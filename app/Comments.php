<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = [
        'user_id', 'text', 'article_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function Article()
    {
        return $this->belongsTo('App\Article');
    }

    public function artist()
    {
        return $this->hasOne('App\Artist', 'user_id', 'user_id');
    }
}
