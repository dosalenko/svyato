<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descriptions extends Model
{
    protected $fillable = [
        'text',
    ];

    public function AdverisingHaveDescription()
    {
        return $this->belongsTo('App\AdvertisingHaveDescription');
    }
}
