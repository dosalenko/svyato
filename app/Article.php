<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title', 'author', 'image', 'text', 'user_id', 'comments_cnt'
    ];

    public function categories()
    {
        return $this->hasManyThrough('App\ArticleCategory', 'App\ArticlesHaveCategory','article_id','id','id','article_category_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }
}
