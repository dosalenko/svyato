<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisingHaveDescription extends Model
{
    protected $fillable = [
        'advertising_id', 'description_id', 'status'
    ];

}
