<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'feedback_name', 'feedback_addres', 'feedback_phone', 'feedback_text'
    ];

    public function getFillable()
    {
        return $this->fillable;
    }
}
