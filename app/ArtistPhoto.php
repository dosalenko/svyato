<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistPhoto extends Model
{
    protected $fillable = [
        'artist_id', 'photo', 'likes',
    ];

    public function bestWeekPhoto()
    {
        return $this::select('photo', 'artist_photos.id as photo_id', 'name','artist_id')
            ->orderByRaw('artist_photos.updated_at DESC', 'likes DESC')
            ->join('artists', 'artists.id', '=', 'artist_photos.artist_id')
            ->limit(1)
            ->get();
    }
}
