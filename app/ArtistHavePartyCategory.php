<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistHavePartyCategory extends Model
{
    protected $fillable = [
        'artist_id', 'party_category_id'
    ];
}
