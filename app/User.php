<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'artist_id', 'image','background_image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function userinfo($artist_id){

        $user = self::all()->where('artist_id', $artist_id)->first();

        return $user;

    }

    public static function getUserById($UserId){

        $user = self::all()->where('id', $UserId)->first();

        return $user;

    }


    public static function getNameByLang($artist_id,$lang){

        $artist=Artist::where('id', $artist_id)->first();

       if ($lang=='ru') {

        if ($artist->name_ru!=NULL) {

            $name='name_ru';

        }else{
            $name='name';
        }

       } else {
           if ($artist->name_ua!=NULL) {

               $name='name_ua';

           }else{
               $name='name';
           }
       }

       return $name;

    }





}
