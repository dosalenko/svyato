<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartyCategory extends Model
{
    protected $fillable = [
        'name','name_ua','name_ru',
    ];
}
