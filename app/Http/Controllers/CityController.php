<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;
use Lang;

class CityController extends Controller
{

    public function list(Request $request)
    {
        if (!$request->ajax()) {
            return response()->json([], 403);
        }

//        if ( app()->getLocale()==='ua'){
//            $lang='ua';
//        }


        if ($_SERVER['REDIRECT_URL'] == '/cities/ru') {
            $lang_select = 'ru';
        } else {
            $lang_select = 'ua';
        }


        return response()->json(Region::
        select(['id', 'region_' . $lang_select, 'img_region'])
            ->with('city:id,name_' . $lang_select . ',region_id')
            ->get());




    }
}
