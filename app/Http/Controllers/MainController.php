<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;
use App\Category;
use App\Artist;
use App\ArtistCity;
use App\ArtistsHaveLikes;
use App\PartyCategory;

class MainController extends Controller
{
    public function index()
    {
        $artistsByRegion = Region::select('regions.id','img_region', 'region_'.app()->getLocale().'', \DB::raw('count(*) as count'))->join('artists', function ($join) {
            $join->on('artists.region_id', '=', 'regions.id');
        }) ->groupBy('regions.id', 'img_region','region_'.app()->getLocale().'')->get();

        $artistLikes = ArtistsHaveLikes::join('users', 'users.id', '=', 'artists_have_likes.user_id')
            ->select('users.image', 'users.email', 'artists_have_likes.artist_id', 'artists_have_likes.user_id')
            ->get();

        $artists = Artist::with('user')
            ->with('photos:artist_id,photo,likes')
            ->with('cities:artist_id,city_id')
            ->with('artistsCategories:artist_id,category_id')
            ->join('advertisings', 'advertisings.id', '=', 'artists.advertising_id')
            ->whereRaw('artists.show_on_main=1 AND artists.is_top=1 OR artists.is_pro=1')
            ->orderByRaw('artists.is_top DESC, advertisings.month_price DESC')
            ->select('artists.*' ,'advertisings.month_price')
        ->get()
        ;



        $bestArtists =

            Artist::with('user')
                ->with('photos:artist_id,photo,likes')
                ->with('cities:artist_id,city_id')
                ->with('artistsCategories:artist_id,category_id')
                ->join('advertisings', 'advertisings.id', '=', 'artists.advertising_id')
                ->whereRaw('artists.show_on_main=1 AND artists.is_top=1 OR artists.is_pro=1')
                ->orderByRaw('artists.is_top DESC, advertisings.month_price DESC')
                ->select('artists.*' ,'advertisings.month_price')

            ->leftJoin('artist_feedbacks', 'artists.id', '=', 'artist_feedbacks.artist_id')
            ->select('artists.id','artists.name','advertisings.month_price', \DB::raw('avg(likes) as likes'))
            ->groupBy('artists.id','artists.name','advertisings.month_price')->get();

       // dd($bestArtists);


         $topCategory = Category::inRandomOrder()->limit(4)->get();
        return view('main', [
            'artistsByRegion' => $artistsByRegion,
            'topCategory' => $topCategory,
            'artists' => $artists,
            'artistCities' => ArtistCity::join('cities', 'artist_cities.city_id', '=', 'cities.id')->select('city_id', 'artist_id', 'name_'.app()->getLocale().'')->get(),
            'artistLikes' => $artistLikes,
            'bestArtists' => $bestArtists,
            'partyCategories' => PartyCategory::all(['id', 'name']),
        ]);
    }

    public function index1()
    {

        return view('main', [

        ]);
    }
}

