<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function save(Request $request)
    {
        $request->validate([
            'feedback_name' => 'string|max:250',
            'feedback_addres' => 'string|max:250',
            'feedback_phone' => 'string|max:250',
            'feedback_text' => 'string|max:2500',
        ]);
        $feedback = new Feedback();
        $feedback->feedback_name = $request->feedback_name;
        $feedback->feedback_addres = $request->feedback_addres;
        $feedback->feedback_phone = $request->feedback_phone;
        $feedback->feedback_text = $request->feedback_text;
        $feedback->save();

        return redirect()->route('main');
    }
}
