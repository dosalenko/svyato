<?php

namespace App\Http\Controllers;

use App\ArtistFeedback;
use Illuminate\Http\Request;

class ArrtistFeedbackController extends Controller
{
    public function add(Request $request)
    {
        $request->validate([
            'artist_id' => 'required|integer',
            'text' => 'required|string|max:2000',
            'rating-01' => 'required|integer',
            'from_user_id' => 'required|integer',
            'user_id' => 'required|integer',
        ]);

        if ($request->input('from_user_id') != \Auth::id()) {
            return redirect()->back();
        }

        $feedback = new ArtistFeedback;
        $feedback->artist_id = $request->input('artist_id');
        $feedback->from_user_id = \Auth::id();
        $feedback->likes = $request->input('rating-01');
        $feedback->text =  strip_tags(htmlspecialchars($request->input('text'));
        $feedback->save();
        return redirect(route('user-page', ['user_id' => $request->input('user_id')]) . '#reviews');
    }
}
