<?php

namespace App\Http\Controllers;

use App\Article;
use App\Artist;
use App\ArticlesHaveCategory;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ArticleController extends Controller
{
    public function save(Request$request)
    {
        $request->validate([
            'title' => 'required|string|max:2000',
            'text' => 'required|string|max:2000',
            'user_id' => 'required|integer',
            'article_category_id' => 'integer',
        ]);
        $article = new Article();
        $text = \preg_replace('/\bon\w+=\S+(?=.*>)/', '', $request->input('text'));
        $text = \preg_replace('/(?:<[^>]+\s)(on\S+)=["\']?((?:.(?!["\']?\s+(?:\S+)=|[>"\']))+.)["\']?/', '', $text);
        $article->title = $request->input('title');
        $article->user_id = $request->input('user_id');
        $article->text = \strip_tags($text, '<p><b><h1><h2><h3><pre>');
        $name = \md5($article->title);
        //$path = "public/upload/title-{$name}.jpg";
        $path = public_path('/upload/title-'.$name.'.jpg');
        Image::make($request->file('image'))->resize(500, 500)->save($path, 80);
        $article->image = '/upload/title-'.$name.'.jpg';
        $article->author = Artist::where('user_id', $article->user_id)->select('name')->get()->first()->name;
        $article->save();
        if(!empty($request->article_category_id)) {
            $articleHaveCategory = new ArticlesHaveCategory();
            $articleHaveCategory->article_id = $article->id;
            $articleHaveCategory->article_category_id = $request->input('article_category_id');
            $articleHaveCategory->save();
        }
        return redirect()->back();
    }
}
