<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;
use App\Category;
use App\Artist;
use App\ArtistCity;
use App\ArtistsHaveLikes;
use App\PartyCategory;

class AdminController extends Controller
{

    public function index()
    {

        return view('admin/main', [

        ]);
    }

    public function artists()
    {

        return view('admin/artists', [

        ]);
    }
}

