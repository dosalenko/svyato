<?php

namespace App\Http\Controllers;

use App\Artist;
use App\ArtistsHaveLikes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ArtistsHaveLikesController extends Controller
{
    public function add(Request $request)
    {
        $request->validate([
            //'user_id' => 'required|integer',
            'artist_id' => 'required:integer',
        ]);

        $like = ArtistsHaveLikes::where(['user_id' => Auth::id(), 'artist_id' => $request->artist_id])->first();
        if ($like) {
            Artist::find($like->artist_id)->decrement('likes_cnt');
            $like->delete();
        } else {
            $artistLikes = new ArtistsHaveLikes();
            $artistLikes->artist_id = $request->input('artist_id');
            $artistLikes->user_id = Auth::id();
            $artistLikes->save();

            Artist::find($artistLikes->artist_id)->increment('likes_cnt');
        }


        return response()->json(array('success'=> true), 200);

        //return redirect()->back();


    }
}
