<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use App\Artist;
use App\Comments;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {

        $articles= Article::
        with('categories')
            ->latest()
            ->paginate(8);



        return view('blog', [
            'articles' => $articles,


            'article_categories' => ArticleCategory::all(['id', 'name_'.app()->getLocale()]),
        ]);
    }

    public function show($postId)
    {
        return view('blog-post', [
            'article' => Article::where('id', $postId)->with('categories')->get()->first(),
            'article_categories' => ArticleCategory::limit(10)->inRandomOrder()->get(),
            'latest_news' => Article::latest()->limit(2)->with('categories')->get(),
            'top_authors' => Artist::withCount('articles')->with('user')->orderBy('articles_count', 'desc')->limit(3)->get(),
            'comments' => Comments::where('article_id', $postId)->with('user')->paginate(2),
        ]);
    }
}
