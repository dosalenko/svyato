<?php

namespace App\Http\Controllers;

use App\Advertising;
use Illuminate\Http\Request;

class AdvertisingController extends Controller
{
    public function index()
    {
        return view('ads', [
            'ads' => Advertising::limit(3)->with('descriptions')->with('advertising_have_descriptions')->get()
        ]);
    }
}
