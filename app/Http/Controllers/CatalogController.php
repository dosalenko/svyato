<?php

namespace App\Http\Controllers;

use App\Artist;
use App\ArtistHaveCategory;
use App\ArtistHavePartyCategory;
use App\ArtistsHaveLikes;
use App\Category;
use App\City;
use App\PartyCategory;
use App\Region;
use App\Seo;
use App\ArtistCity;
use Illuminate\Http\Request;
use App\Session;
use Stringy\Stringy;
use Lang;

class CatalogController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        if (Lang::getLocale() === 'ru') {
            $lang_select = 'ru';
        } else {
            $lang_select = 'ua';
        }


        $artists = Artist::with('user')
            ->with('cities:artist_id,city_id')
            ->with('artistsCategories:artist_id,category_id')
            ->where('price', '>', 0)
            ->where('category_id', '>', 0)
            ->where('region_id', '>', 0);

        $artistLikes = ArtistsHaveLikes::join('users', 'users.id', '=', 'artists_have_likes.user_id')
            ->select('users.image', 'users.email', 'artists_have_likes.artist_id', 'artists_have_likes.user_id')
            ->get();
        $order = $request->input('sort_user');
        if (!empty($order)) {
            if ($order == 'new') {
                $artists->latest();
            } elseif ($order == 'likes') {
                $artists->orderByDesc('artists.likes_cnt');
            } elseif ($order == 'cheap') {
                $artists->orderBy('artists.price');
            } elseif ($order == 'expensive') {
                $artists->orderByDesc('artists.price');
            }
        } else {
            $artists->inRandomOrder();
        }
        $top = $request->input('top');
        if ($top == 1) {
            $artists->where('is_top', 1);
        }

        $pro = $request->input('pro');
        if ($pro == 1) {
            $artists->where('is_pro', 1);
        }

        $online = $request->input('online');
        if ($online == 1) {
            $onlineIds = Session::registered()->get()->pluck('user_id');
            $artists->whereIn('artists.user_id', $onlineIds);
        }
        $likes = $request->input('rating-01');
        if (!empty($likes)) {
            $artists->where('artists.likes_cnt', '>=', $likes);
        }

        $currentCategory = trans('mes.Виконавці');
        $category = $request->input('category');
        $categoryId = $request->input('categoryId');


        if (!empty($category) && !is_array($category)) {
            $artistsIds = ArtistHaveCategory::where('category_id', $category)->pluck('artist_id');
            $currentCategory = Category::find($category)->category_{$lang_select};
            $artists->whereIn('id', $artistsIds);
        } elseif (\is_array($category) && end($category)) {
            $categoryIds = Category::whereIn('category_' . $lang_select, $category)->get();
            $artistsIds = ArtistHaveCategory::whereIn('category_id', $categoryIds)->pluck('artist_id');
            $artists->whereIn('id', $artistsIds);
            $currentCategory = implode(',', $category);
        }

        $currentPartyCategory = trans('mes.свято');
        $partyCategory = $request->input('party_category');
        if (!empty($partyCategory)) {
            $artistsIds = ArtistHavePartyCategory::where('party_category_id', $partyCategory)->pluck('artist_id');
            $currentPartyCategory = PartyCategory::find($partyCategory)->{'name_' . $lang_select};
            $artists->whereIn('id', $artistsIds);
        }

        $minPrice = $request->input('price_min');
        if (!empty($minPrice)) {
            $artists->where('artists.price', '>=', (int)$minPrice);
        }

        $maxPrice = $request->input('price_max');
        if (!empty($maxPrice)) {
            $artists->where('artists.price', '<=', (int)$maxPrice);
        }

        $currentCities = $request->input('cities');

        if ($request->input('region_name') != NULL) {
            $currentPlace = $request->input('region_name') . ' ';

            $artists->where('region_id', intval($request->input('region_id')));
        } else {


            $currentPlace = trans('mes.по всій Україні');


            if (!empty($currentCities) && !is_array($currentCities) || is_array($currentCities) && end($currentCities)) {
                if (!is_array($currentCities)) {
                    $artistsIds = ArtistCity::where('city_id', $currentCities)->pluck('artist_id');
                    $regions = Region::join('cities', 'regions.id', '=', 'cities.region_id')
                        ->where('cities.id', '=', $currentCities)
                        ->select('regions.id as region_id', 'region_' . $lang_select . '', 'cities.id as city_id', 'cities.name_' . $lang_select . ' as city_name')
                        ->get()
                        ->groupBy('region_' . $lang_select . '');
                } else {
                    $cityIds = City::whereIn('name_' . $lang_select . '', $currentCities)->get();
                    $artistsIds = ArtistCity::whereIn('city_id', $cityIds)->pluck('artist_id');
                    $regions = Region::join('cities', 'regions.id', '=', 'cities.region_id')
                        ->whereIn('cities.id', $cityIds)
                        ->select('regions.id as region_id', 'region_' . $lang_select . '', 'cities.id as city_id', 'cities.name_' . $lang_select . ' as city_name')
                        ->get()
                        ->groupBy('region_' . $lang_select . '');
                    //var_dump($cityIds); die();
                }
                $tempCurrentPlace = '';
                foreach ($regions as $region => $regionCities) {
                    //dd($regionCities);

                    $tempCities = $regionCities->pluck('city_name')->implode(',');


                    if ($region == 'Автономна Республіка Крим' or $region == 'Автономная Республика Крым') {
                        $obl = '';

                    } else {
                        //$obl = 'обл.';
                        $obl = '';
                    }

                    $tempCitiesGet = implode(',', array_slice($currentCities, 0, 5));

                        if(is_array($currentCities) && count($currentCities)>5){
                            $add_points='...';

                        }else{
                            $add_points='';
                        }

                    //$tempCurrentPlace .= " $region ($tempCities),";
                    $tempCurrentPlace .= " $region ($tempCitiesGet),".$add_points;
                    //$tempCurrentPlace .= " $region";


                }
                $artists->whereIn('id', $artistsIds);
                $currentPlace = 'в ' . Stringy::create($tempCurrentPlace)->removeRight(',')->__toString();
                // $currentPlace = 'в ' . Stringy::create($region.$obl.$tempCurrentPlace)->removeRight(',')->__toString();

            }
        }

        $artistsOnParty = Category::select('categories.id', 'category_' . $lang_select)
            ->join('artist_have_categories', 'categories.id', '=', 'artist_have_categories.category_id')
            ->when($partyCategory, function ($query, $partyCategory) {
                return $query->join('artist_have_party_categories', function ($join) use ($partyCategory) {
                    $join->on('artist_have_party_categories.artist_id', '=', 'artist_have_categories.artist_id')->where('artist_have_party_categories.party_category_id', '=', $partyCategory);
                });
            })
            ->get()->groupBy('category_' . $lang_select);

        $artistsByRegion = Region::select('regions.id', 'regions.region_' . $lang_select . '', \DB::raw('count(*) as count'))->join('artists', function ($join) {
            $join->on('artists.region_id', '=', 'regions.id');
        })->when($partyCategory, function ($query, $partyCategory) {
            return $query->join('artist_have_party_categories', function ($join) use ($partyCategory) {
                $join->on('artist_have_party_categories.artist_id', '=', 'artists.id')->where('artist_have_party_categories.party_category_id', '=', $partyCategory);
            });
        })->groupBy('regions.id', 'regions.region_' . $lang_select . '')->get();

        return view('catalog', [
            'artists' => $artists->paginate(21),
            'artistCities' => ArtistCity::join('cities', 'artist_cities.city_id', '=', 'cities.id')->select('city_id', 'artist_id', 'name_' . $lang_select . '')->get(),
            'seo' => Seo::all('text_ua', 'text_ru')->first(),
            'partyCategories' => PartyCategory::all(['id', 'name','name_ua','name_ru']),
            'currentPartyCategory' => $currentPartyCategory,
            'currentCategory' => $currentCategory,
            'currentPlace' => $currentPlace,
            'artistsOnParty' => $artistsOnParty,
            'artistsByRegion' => $artistsByRegion,
            'artistLikes' => $artistLikes,
        ]);
    }
}
