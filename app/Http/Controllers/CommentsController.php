<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comments;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function add(Request $request)
    {
        $request->validate([
            'user_id' => 'required|integer',
            'article_id' => 'required:integer',
            'text' => 'required|string|max:500',
        ]);
        $comment = new Comments();
        $comment->article_id = $request->input('article_id');
        $comment->user_id = $request->input('user_id');
        $comment->text = $request->input('text');
        $comment->save();

        Article::find($comment->article_id)->increment('comments_cnt');

        return redirect()->back();
    }
}
