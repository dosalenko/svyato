<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Region;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $region = Region::where('region_ua', 'LIKE', "%$keyword%")
                ->orWhere('', 'LIKE', "%$keyword%")
                ->orWhere('img_region', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $region = Region::latest()->paginate($perPage);
        }

        return view('admin.region.index', compact('region'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.region.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        if ($request->hasFile('img_region')) {
            $requestData['img_region'] = $request->file('img_region')
                ->store('uploads', 'public');
        }

        Region::create($requestData);

        return redirect('admin/region')->with('flash_message', 'Region added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $region = Region::findOrFail($id);

        return view('admin.region.show', compact('region'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $region = Region::findOrFail($id);

        return view('admin.region.edit', compact('region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();



        if ($request->hasFile('img_region')) {

            $name = '/upload/img_region/' . md5(time()) .'.'. $request->img_region->getClientOriginalExtension();
            Image::make($request->file('img_region'))->resize(500, 500)->save(public_path($name), 80);
            $requestData['img_region'] = $name;

        }




        $region = Region::findOrFail($id);
        $region->update($requestData);

        return redirect('admin/region')->with('flash_message', 'Region updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Region::destroy($id);

        return redirect('admin/region')->with('flash_message', 'Region deleted!');
    }
}
