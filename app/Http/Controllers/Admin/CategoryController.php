<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $category = Category::where('name_ua', 'LIKE', "%$keyword%")
                ->orWhere('name_ru', 'LIKE', "%$keyword%")
                ->orWhere('img_category', 'LIKE', "%$keyword%")
                ->orWhere('icon_category', 'LIKE', "%$keyword%")
                ->orWhere('category_ua', 'LIKE', "%$keyword%")
                ->orWhere('category_ru', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $category = Category::latest()->paginate($perPage);
        }

        return view('admin.category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        if ($request->hasFile('img_category')) {

            $name = '/upload/img_category/' . md5(time()) .'.'. $request->img_category->getClientOriginalExtension();
            Image::make($request->file('img_category'))->resize(500, 500)->save(public_path($name), 80);
            $requestData['img_category'] = $name;

        }

        if ($request->hasFile('icon_category')) {

            $name = '/upload/icon_category/' . md5(time()) .'.'. $request->icon_category->getClientOriginalExtension();

            Image::make($request->file('icon_category'))->resize(120, 120)->save(public_path($name), 80);
            $requestData['icon_category'] = $name;

        }


        Category::create($requestData);

        return redirect('admin/category')->with('flash_message', 'Category added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);

        return view('admin.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        if ($request->hasFile('img_category')) {


            $name = '/upload/img_category/' . md5(time()) .'.'. $request->img_category->getClientOriginalExtension();
            Image::make($request->file('img_category'))->resize(500, 500)->save(public_path($name), 80);
            $requestData['img_category'] = $name;

        }
        if ($request->hasFile('icon_category')) {

            $name = '/upload/icon_category/' . md5(time()) .'.'. $request->icon_category->getClientOriginalExtension();

            Image::make($request->file('icon_category'))->resize(120, 120)->save(public_path($name), 80);
            $requestData['icon_category'] = $name;

        }

        $category = Category::findOrFail($id);
        $category->update($requestData);

        return redirect('admin/category')->with('flash_message', 'Category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Category::destroy($id);

        return redirect('admin/category')->with('flash_message', 'Category deleted!');
    }
}
