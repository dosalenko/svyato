<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Artist;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:3', 'max:250',],
            'confidentality' => ['required', 'integer',],
        ]);
    }

    public function messages()
    {
        return [
            'confidentality.required' => 'Потрібно прийняти умови',
            'email.required' => 'Потрібно ввести логін',
            'email.max' => 'Дуже довгий логін',
            'email.max:255' => 'Дуже довгий логін',
            'email.unique' => 'Такий логін вже існує',
            'password.required' => 'Потрібно ввести пароль',
            'password.min' => 'Дуже короткий пароль ',
            'password.min:3' => 'Дуже короткий пароль ',
            'password.max' => 'Дуже довгий пароль ',
            'password.max:250' => 'Дуже довгий пароль ',
        ];
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $validator = \Validator::make($data, [
            'email' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:4', 'max:250'],
            'confidentality' => ['required', 'integer'],

        ]);


        $user = User::create([

            'email' => $data['email'],
            'password' => bcrypt($data['password']),

        ]);

        $id = $user->id;

        $artist =Artist::create([
            'name' => $data['email'],
            //'description' => '',
            'user_id' => $id,
            'region_id' => 0,
            'category_id' => 0,

        ]);




        $user_upd = User::find($id);
        $user_upd->artist_id = $artist->id;
        $user_upd->save();


        return $user;


    }
}
