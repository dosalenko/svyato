<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Lang;

class CategoryController extends Controller
{
    public function  list(Request $request)
    {



        if($_SERVER['REDIRECT_URL']=='/categories/ru'){
            $lang_select='ru';
        } else {
            $lang_select='ua';
        }


        if (!$request->ajax()) {
            return response()->json([], 403);
        }




         return response()->json(Category::limit(50)->select(['id', 'category_' . $lang_select, 'img_category', 'icon_category'])->get());
        // return response()->json(Category::limit(50)->select(['id', 'category', 'img_category', 'icon_category'])->get());
    }
}
