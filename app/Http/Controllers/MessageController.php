<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function create(Request $request)
    {

        if ($request->to_id != Auth::id()) {

            $mes = new Message();
            $mes->to_id = $request->to_id;
            $mes->from_id = Auth::id();
            $mes->text = strip_tags(htmlspecialchars($request->text));
            $mes->save();

        }

        //return redirect('/lk-page/' . Auth::id() . '#message-user-'.$request->to_id);
        return redirect('/lk-page/' . Auth::id() . '#message');


    }

    public function make_messages_read(Request $request)
    {
        $request->validate([

            'from_id' => 'required:integer',
        ]);

        $mes_to = Message::where(['from_id' => $request->from_id, 'to_id' => Auth::id()])
            ->update([
                'is_read' => 1,
            ]);


        return response()->json(array('success' => true), 200);


    }


}
