<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;

use App\Artist;
use App\ArtistAudio;
use App\ArtistCity;
use App\ArtistFeedback;
use App\ArtistHaveCategory;
use App\ArtistPhoto;
use App\ArtistVideo;
use App\PartyCategory;
use App\Session;
use App\Region;
use App\User;
use App\Article;
use App\ArticleCategory;
use App\ArtistsHaveLikes;
use App\ArtistHavePartyCategory;
use App\Category;
use App\City;

class UserController extends Controller
{
    public function page($userId, Request $request)
    {
        if(is_numeric($userId)){
            $artist = Artist::
            where('user_id', $userId)
                ->with('region:id,region_' . app()->getLocale())
                ->with('user:id,image,background_image')
                ->with('category:id,name_' . app()->getLocale() . ',category_' . app()->getLocale())
                ->with('partyCategories')
                ->first();
        } else {

            $getUserIdByEmail=User::where('email',$userId)->first();



            $artist = Artist::
            where('user_id', $getUserIdByEmail->id)
                ->with('region:id,region_' . app()->getLocale())
                ->with('user:id,image,background_image')
                ->with('category:id,name_' . app()->getLocale() . ',category_' . app()->getLocale())
                ->with('partyCategories')
                ->first();
        }






        $categoryId = $request->input('category') ?: $artist->category_id;

        if ($artist->partyCategories) {
            $checkPartyCategory = 1;
        } else {
            $checkPartyCategory = $artist->partyCategories->first()->id;
        }


        $PartyCategory = PartyCategory::find($request->input('party_category') ?: $checkPartyCategory);

        return view('user_page', [
            'artist' => $artist,
            'photos' => ArtistPhoto::where('artist_id', $artist->id)->select(['id', 'artist_id', 'photo', 'likes'])->orderBy('id', 'desc')->paginate(6),
            'videos' => ArtistVideo::where('artist_id', $artist->id)->select(['id', 'artist_id', 'video', 'likes'])->orderBy('id', 'desc')->paginate(6),
            'audio' => ArtistAudio::where('artist_id', $artist->id)->select(['id', 'artist_id', 'audio', 'likes'])->orderBy('id', 'desc')->paginate(6),
            'active' => Session::registered()->get()->pluck('user_id'),
            'reviews' => ArtistFeedback::where('artist_id', $artist->id)->with('user:id,email,image')->orderByDesc('updated_at')->paginate(2),
            'artists' => ArtistHaveCategory::where('artist_have_categories.category_id', $categoryId)
                ->join('artists', 'artists.id', '=', 'artist_have_categories.artist_id')
                ->join('users', 'artists.user_id', '=', 'users.id')
                ->join('categories', 'artist_have_categories.category_id', '=', 'categories.id')
                ->select('artists.id', 'artists.name', 'artists.is_pro', 'artists.user_id', 'users.image', 'categories.id as category_id', 'categories.name_' . app()->getLocale() . ' as category', 'artists.price', 'artists.old_price', 'artists.likes_cnt')
                ->where('artists.user_id', '!=', $artist->user_id)
                ->where('artists.region_id', '=', $artist->region_id)
                ->get(),
            'artistCities' => ArtistCity::join('cities', 'artist_cities.city_id', '=', 'cities.id')->select('artist_id', 'name_' . app()->getLocale(), 'city_id')->get(),
            'regions' => Region::select('regions.id', 'regions.region_' . app()->getLocale(), \DB::raw('count(*) as count'))->join('artists', function ($join) use ($categoryId) {
                $join->on('artists.region_id', '=', 'regions.id')->where('artists.category_id', '=', $categoryId);
            })->groupBy('regions.id', 'regions.region_' . app()->getLocale() . '')->get(),
            'partyCategory' => $PartyCategory
        ]);
    }

    public function lk_page($userId)
    {
        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);

        $artist = Artist::where('user_id', $userId)
            ->with('region:id,region_' . app()->getLocale())
            ->with('user:id,image')
            ->with('category:id,name_' . app()->getLocale() . ',category_' . app()->getLocale())
            ->with('partyCategories')
            ->first();

        $artistPartyCategories = ArtistHavePartyCategory::where('artist_id', $artist->id)->pluck('party_category_id')->implode(',');



        $messages_from_users = [];

        $messages = Message::
             where('to_id', $userId)
            //->where('to_id', $userId)
            ->get();

        foreach ($messages as $key => $mes) {
            $messages_from_users[$mes->from_id] = $mes->from_id;
        }





        return view('lk-page', [
            'artist' => $artist,
            'photos' => ArtistPhoto::where('artist_id', $artist->id)->select(['id', 'artist_id', 'photo', 'likes'])->orderBy('id', 'desc')->paginate(6),
            'videos' => ArtistVideo::where('artist_id', $artist->id)->select(['id', 'artist_id', 'video', 'likes'])->orderBy('id', 'desc')->paginate(6),
            'audio' => ArtistAudio::where('artist_id', $artist->id)->select(['id', 'artist_id', 'audio', 'likes'])->orderBy('id', 'desc')->paginate(6),
            'active' => Session::registered()->get()->pluck('user_id'),
            'reviews' => ArtistFeedback::where('artist_id', $artist->id)->with('user:id,email,image')->orderByDesc('updated_at')->paginate(2),
            'articles' => Article::where('user_id', $userId)->orderByDesc('updated_at')->paginate(6),
            'partyCategories' => PartyCategory::all(['id', 'name','name_ua','name_ru']),
            'artistCities' => ArtistCity::join('cities', 'artist_cities.city_id', '=', 'cities.id')->select('artist_id', 'name_' . app()->getLocale(), 'city_id')->get(),
            'artistPartyCategories' => $artistPartyCategories,
            'article_categories' => ArticleCategory::all(['id', 'name_' . app()->getLocale()]),
            'user_likes' => ArtistsHaveLikes::where('user_id', $userId)->count(),
            'messages_from_users' => $messages_from_users,
           // 'categories' => Category::limit(50)->select(['id', 'category_' . $lang_select, 'img_category', 'icon_category'])->get(),

        ]);


    }

    public function save_images($userId, Request $request)
    {


        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);

        $requestData = [];

        if ($request->hasFile('image')) {

            $name = '/upload/' . md5(time()) . '.' . $request->image->getClientOriginalExtension();
            Image::make($request->file('image'))->resize(120, 120)->save(public_path($name), 80);
            $requestData['image'] = $name;

        }

        if ($request->hasFile('background_image')) {

            $name = '/upload/' . md5(time()) . '.' . $request->background_image->getClientOriginalExtension();
            Image::make($request->file('background_image'))->resize(500, 500)->save(public_path($name), 80);
            $requestData['background_image'] = $name;

        }


        $user = User::findOrFail($userId);
        $user->update($requestData);


        return redirect('/lk-page/' . $userId);
    }

    public function save_data($userId, Request $request)
    {


        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);

        $requestData = $request->all();

        if ($request->cities!=NULL) {
            $city_id = City::where('name_' . app()->getLocale(), $request->cities[0])->first();
             $region_id = Region::where('id', $city_id->region_id)->first();
             $requestData['city_id']=$city_id->id;
             $requestData['region_id']=$region_id->id;


        }

        if ($request->category!=NULL) {
            $category_id = Category::where('category_' . app()->getLocale(), $request->category[0])->first();
            $requestData['category_id']=$category_id->id;
        }




       // dd($requestData);




        $validator = \Validator::make($request->all(), [
            'name_ua' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'description_ua' => 'required|max:5000',
            'description_ru' => 'required|max:5000',
            'price' => 'integer',

        ]);


        if ($validator->fails()) {

            return redirect('/lk-page/' . $userId)
                //->withErrors($validator)
                //->withInput()
            ;

        } else {

            $artist = Artist::where('user_id', $userId)->first();
            $artist->update($requestData);


            $artistPartyCategoriesDel = ArtistHavePartyCategory::where('artist_id', $artist->id);
            $artistPartyCategoriesDel->delete();

            $artistPartyCategoriesSave = ArtistHavePartyCategory::create([
                'artist_id' => $artist->id,
                'party_category_id' => $request->party_category,
            ]);

            return redirect('/lk-page/' . $userId);
        }
    }


    public function save_images_gallery($userId, Request $request)
    {


        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);
        $artist = Artist::where('user_id', $userId)->first();

        $path = '/upload/gallery/';
        $photos_path = public_path($path);

        $photos = $request->file('file');

        if (!is_array($photos)) {
            $photos = [$photos];
        }

        $count_user_uploaded_photos = ArtistPhoto::where('artist_id', $artist->id)->count();

        if ($count_user_uploaded_photos + count($photos) > 10) {
            $limit = 10;
            $limit_message = trans('mes.Ви можете завантажити не більше 10 фото');

        } else {
            $limit = 0;
            $limit_message = null;

            for ($i = 0; $i < count($photos); $i++) {
                $photo = $photos[$i];
                $name = sha1(date('YmdHis') . str_random(30));
                $save_name = $name . '.' . $photo->getClientOriginalExtension();
                $resize_name = 'sm_' . $name . '.' . $photo->getClientOriginalExtension();

                Image::make($photo)
                    ->resize(500, null, function ($constraints) {
                        $constraints->aspectRatio();
                    })
                    ->save($photos_path . '/' . $resize_name);

                $photo->move($photos_path, $save_name);

                $upload = new ArtistPhoto();
                $upload->photo = $save_name;
                $upload->artist_id = $artist->id;
                $upload->save();
            }

        }

        return Response::json([
            'message' => 'Image saved Successfully',
            'limit' => $limit,
            'limit_message' => $limit_message
        ], 200);

    }

    public function delete_images_gallery($userId, Request $request)
    {

        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);

        $artist = Artist::where('user_id', $userId)->first();


        $photo = ArtistPhoto::
        where('id', $request->gallery_id)->
        where('artist_id', $artist->id)->
        first();

        $photo->delete();

        return redirect('/lk-page/' . $userId);

    }


    public function save_video_gallery($userId, Request $request)
    {

        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);

        $artist = Artist::where('user_id', $userId)->first();

        $count_user_uploaded_videos = ArtistVideo::where('artist_id', $artist->id)->count();


        if ($count_user_uploaded_videos > 10) {

            $video_limit_message = trans('mes.Ви можете завантажити не більше 10 відео');

            $request->session()->flash('video_limit_message', $video_limit_message);


            return redirect('/lk-page/' . $userId . '#video');

        } else {


            $upload = new ArtistVideo();
            $upload->video = strip_tags(htmlspecialchars($request->video));
            $upload->artist_id = $artist->id;
            $upload->save();

            return redirect('/lk-page/' . $userId . '#video');

        }

    }


    public function delete_video_gallery($userId, Request $request)
    {

        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);

        $artist = Artist::where('user_id', $userId)->first();


        $video = ArtistVideo::
        where('id', $request->video_id)->
        where('artist_id', $artist->id)->
        first();

        $video->delete();

        return redirect('/lk-page/' . $userId . '#video');

    }

    public function save_audio_gallery($userId, Request $request)
    {

        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);

        $artist = Artist::where('user_id', $userId)->first();

        $count_user_uploaded_videos = ArtistAudio::where('artist_id', $artist->id)->count();


        if ($count_user_uploaded_videos > 100) {

            $video_limit_message = trans('mes.Ви можете завантажити не більше 100 аудіо');

            $request->session()->flash('audio_limit_message', $audio_limit_message);


            return redirect('/lk-page/' . $userId . '#music');

        } else {


            $upload = new ArtistAudio();
            $upload->audio = strip_tags(htmlspecialchars($request->audio));
            $upload->artist_id = $artist->id;
            $upload->save();

            return redirect('/lk-page/' . $userId . '#music');

        }

    }

    public function delete_audio_gallery($userId, Request $request)
    {

        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);

        $artist = Artist::where('user_id', $userId)->first();


        $audio = ArtistAudio::
        where('id', $request->audio_id)->
        where('artist_id', $artist->id)->
        first();

        $audio->delete();

        return redirect('/lk-page/' . $userId . '#music');

    }

    public function edit_article($userId, Request $request)
    {

        if ($userId != Auth::id()) return redirect('/user-page/' . $userId);

        $artist = Artist::where('user_id', $userId)->first();


        $article = Article::
        where('id', $request->article_id)->
        where('user_id', Auth::id())->
        first();

        $text = \preg_replace('/\bon\w+=\S+(?=.*>)/', '', $request->input('text'));
        $text = \preg_replace('/(?:<[^>]+\s)(on\S+)=["\']?((?:.(?!["\']?\s+(?:\S+)=|[>"\']))+.)["\']?/', '', $text);
        $article->title = $request->input('title');

        $article->text = \strip_tags($text, '<p><b><h1><h2><h3><pre>');

        if (!empty($request->image)) {
            $name = \md5(time());

            $path = public_path('/upload/title-' . $name . '.jpg');

            Image::make($request->file('image'))->resize(500, 500)->save($path, 80);
            $article->image = '/upload/title-' . $name . '.jpg';
        }


        $article->save();

        return redirect('/lk-page/' . $userId . '#publications');

    }


    public function message($userId)
    {


    }


}
