<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = [
        'region_ua', 'region_ru',  'img_region',
        ];

    public function city()
    {
       return $this->hasMany('App\City');
    }

}
