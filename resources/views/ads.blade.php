@extends('layouts.app')

@section('content')
    <div id="main-app">
        @include('partial.week_photo', ['title' => trans('mes.Реклама на порталі').' "'.trans('mes.Моє Свято').'"'])
        @include('partial.breadcrumb', ['current' => trans('mes.Реклама')])


        {{----}}
        {{--<div class="advertising-page container">--}}
            {{--<div class="row">--}}
                {{--@foreach($ads as $ad)--}}
                    {{--<div class="price-block @if($ad->active_pro == 1)active-pro @endif">--}}
                        {{--<div class="price-head">--}}

                            {{--<div class="head-title">--}}
                                {{--<div class="bg-title"></div>--}}
                                {{--<span>{{ $ad->name }}</span>--}}
                            {{--</div>--}}

                            {{--<div class="price">--}}
                                {{--<span class="currency-icon">₴</span>--}}
                                {{--<div class="price-detail">--}}
                                    {{--<span class="currency">{{ $ad->month_price }}</span>--}}
                                    {{--<span class="year">міс</span>--}}
                                {{--</div>--}}
                                {{--<div class="price-detail year-detail">--}}
                                    {{--<span class="currency">{{ $ad->year_price }}</span>--}}
                                    {{--<span class="year">рік</span>--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="price-list">--}}
                            {{--<ul>--}}
                                {{--@foreach($ad->descriptions as $description)--}}
                                    {{--@if($ad->advertising_have_descriptions->where('description_id', $description->id)->first()->status == 1)--}}
                                        {{--<li class="active"><i class="fas fa-check-circle"></i> {{ $description->text }}</li>--}}
                                    {{--@else--}}
                                        {{--<li class="disable"><i class="fas fa-times-circle"></i> {{ $description->text }}</li>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                        {{--</div>--}}

                        {{--<div class="price-order">--}}
                            {{--<div class="bg-btn"></div>--}}
                            {{--@if(Auth::check())--}}
                                {{--если пакет куплен - надпись Активен - иначе--}}
                                {{--<a class="order-btn" href="#"> {{trans('mes.Придбати пакет')}}</a>--}}
                            {{--@else--}}
                                {{--<a class="order-btn" href="#" data-toggle="modal" data-target="#authorization-modal"> {{trans('mes.Зареєструватись')}}</a>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--</div>--}}
        {{--</div>--}}
  {{----}}
  {{----}}


        <ul class="breadcrumb-list">
            <li class="breadcrumb-item">
                <a href="#" class="breadcrumb-link">Головна</a>
            </li>

            <li class="breadcrumb-item active">
                <a href="#" class="breadcrumb-link">Реклама</a>
            </li>
        </ul>
    </div>
    <div class="advertising-page container">
        <div class="row">
            <div class="price-block">
                <div class="price-head">

                    <div class="head-title">
                        <div class="bg-title"></div>
                        <span>Start</span>
                    </div>

                    <div class="price">
                        <span class="currency-icon">₴</span>
                        <div class="price-detail">
                            <span class="currency">99</span>
                            <span class="year">міс</span>
                        </div>
                        <div class="price-detail year-detail">
                            <span class="currency">99</span>

                            <span class="year">рік</span>
                        </div>

                    </div>
                </div>
                <div class="price-list">
                    <ul>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Опис</span> до 500</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Кількість фото</span> до 10</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Кількість відео</span> до 5</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розміщення профіля в 1 напрямку(Весілля, корпоративи..)</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розміщення профіля</span>в 1 категорії</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розміщення профіля</span>в 1 області</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Доступ до ТОП аукціону</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Посилання на Ваш сайт</span></li>

                        <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Розміщення профіля на головній сторінці</span></li>
                        <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Пріоритетне розміщення профіля в каталозі</span></li>
                        <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Розширена статистика відвідувань</span></li>
                        <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Посилання на соціальні мережі</span></li>
                        <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Посилання в месенджери</span></li>
                        <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Отримання сповіщень у месенджери</span></li>
                    </ul>
                </div>
                <div class="price-order">
                    <div class="bg-btn"></div>
                    <a class="order-btn" href="#">Зареєструватись</a>
                </div>


            </div>
            <div class="price-block">
                <div class="price-head">
                    <div class="head-title">
                        <div class="bg-title"></div>
                        <span>Standart</span>
                    </div>

                    <div class="price">
                        <span class="currency-icon">₴</span>
                        <div class="price-detail">
                            <span class="currency">99</span>
                            <span class="year">міс</span>
                        </div>
                        <div class="price-detail year-detail">
                            <span class="currency">99</span>

                            <span class="year">рік</span>
                        </div>

                    </div>
                </div>
                <div class="price-list">
                    <ul>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Опис</span> до 500</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Необмежена</span> кількість фото</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Необмежена </span> кількість відео</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розміщення профіля у всіх напрямках(Весілля, корпоративи..)</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розміщення профіля</span>в 3 категоріях</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розміщення профіля</span>в 3 областях</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Доступ до ТОП аукціону</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Посилання на Ваш сайт</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розміщення профіля на головній сторінці</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Пріоритетне розміщення профіля в каталозі</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розширена статистика відвідувань</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Посилання на соціальні мережі</span></li>
                        <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Посилання в месенджери</span></li>
                        <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Отримання сповіщень у месенджери</span></li>
                    </ul>
                </div>
                <div class="price-order">
                    <div class="bg-btn"></div>
                    <a class="order-btn" href="#">Придбати пакет</a>
                </div>


            </div>
            <div class="price-block active-pro">
                <div class="price-head">

                    <div class="head-title">
                        <div class="bg-title"></div>
                        <span>recommend</span>
                    </div>

                    <div class="price">
                        <span class="currency-icon">₴</span>
                        <div class="price-detail">
                            <span class="currency">99</span>
                            <span class="year">міс</span>
                        </div>
                        <div class="price-detail year-detail">
                            <span class="currency">99</span>

                            <span class="year">рік</span>
                        </div>

                    </div>
                </div>
                <div class="price-list">
                    <ul>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Опис</span> до 500</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Необмежена</span> кількість фото</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Необмежена </span> кількість відео</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розміщення профіля у всіх напрямках(Весілля, корпоративи..)</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Необмежене</span> розміщення в категоріях</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Необмежене</span> розміщення в областях</li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Доступ до ТОП аукціону</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Посилання на Ваш сайт</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розміщення профіля на головній сторінці</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Пріоритетне розміщення профіля в каталозі</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Розширена статистика відвідувань</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Посилання на соціальні мережі</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Посилання в месенджери</span></li>
                        <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Отримання сповіщень у месенджери</span></li>
                    </ul>
                </div>
                <div class="price-order">
                    <div class="bg-btn"></div>
                    <a class="order-btn" href="#">Придбати пакет</a>
                </div>


            </div>
            <!--<div class="price-block ">
                    <div class="price-head">
                            <div class="head-title">
                               <div class="bg-title"></div>
                                <span>maximum</span>
                            </div>

                        <div class="price">
                           <span class="currency-icon">₴</span>
                            <div class="price-detail">
                                <span class="currency">99</span>
                                <span class="year">міс</span>
                            </div>
                               <div class="price-detail year-detail">
                                <span class="currency">99</span>

                                <span class="year">рік</span>
                            </div>

                        </div>
                    </div>
                    <div class="price-list">
                        <ul>
                            <li class="active">
                           <i class="fas fa-check-circle"></i>
                           <span>Опис</span> до 500</li>
                            <li class="active">
                            <i class="fas fa-check-circle"></i>
                            <span>Опис</span> до 500</li>
                            <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Опис</span> до 500</li>
                            <li class="disable">
                            <i class="fas fa-times-circle"></i>
                            <span>Опис</span> до 500</li>
                        </ul>
                    </div>
                    <div class="price-order">
                        <div class="bg-btn"></div>
                        <a class="order-btn" href="#">Діючий пакет</a>
                    </div>


            </div>-->
        </div>
    </div>



    </div>
@endsection
