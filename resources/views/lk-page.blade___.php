@extends('layouts.app')

<?php
use App\User;
use App\Artist;
use App\Message;
$unreadMessages = Message::where('to_id', Auth::id())->where('is_read', 0)->count();

?>


@section('content')

<?php
$region_lang = 'region_' . Lang::getLocale();
$category_lang = 'category_' . Lang::getLocale();
$city_lang = 'name' . Lang::getLocale();
?>

<div class="user-page">

    <div class="main-window">
        <img class="main-img-mask" src="{{ Auth::user()->background_image }}"/>
        <div class="mask"></div>
        <div class="dot-background"></div>
        <div class="main-img" style="background-image: url({{ Auth::user()->background_image }});
                    background-position: top center;
                    background-size: cover;"></div>
        <div class="container">
            <div class="user-detail lk-detail">
                <div class="img-block">
                    <div class="img-item">
                        <img class="user-img" src="{{ (Auth::user()->image)?:'/img/bg/home.jpg' }}">
                    </div>
                    <form class="body-calc js-search" method="POST"
                          action="{{ url('/lk-page/save_images/' . Auth::id()) }}"
                          accept-charset="UTF-8"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="btn-group">


                            <a href="#"><label for="open-avatar">{{ trans('mes.Змінити аватарку') }}

                                    <input type="file" name="image" id="open-avatar">
                                </label></a>
                            <a href="#"><label for="open-bg">{{ trans('mes.Змінити задній фон') }}
                                    <input type="file" name="background_image" id="open-bg">
                                </label>
                            </a>

                            <input id="save_image" style="display:none;" class="btn btn-primary" type="submit"
                                   value="{{ trans('mes.Зберегти зображення') }}">


                            {{--<a href="#" class="modal-link" data-toggle="modal" data-target="#settings-modal">Налаштування</a>--}}


                        </div>
                    </form>
                </div>


                <div class="description-block">

                    <div id="main-app">
                        <form class="body-calc js-search" action="{{ url('/lk-page/save_data/' . Auth::id()) }}"
                              method="GET">


                            {{ csrf_field() }}
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="ua-description" data-toggle="tab"
                                       href="#description-ua" role="tab" aria-controls="ua-description"
                                       aria-selected="true">{{ trans('mes.Інформація на українській') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="ru-description" data-toggle="tab" href="#description-ru"
                                       role="tab" aria-controls="ru-description" aria-selected="true">{{ trans('mes.Інформація на російській') }}</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="description-ua" role="tabpanel"
                                     aria-labelledby="ua-description">
                                    <div class="text-user">
                                        <label>
                                            <input type="text" required="required" name="name_ua"
                                                   value="{{ isset($artist->name_ua) ? $artist->name_ua : ''}}"
                                                   placeholder="{{ trans('mes.Введіть назву: наприклад Фірман Сергій, Агенція Ваша Плівка') }}">
                                        </label>

                                        <textarea name="description_ua" required="required" rows="3"
                                                  placeholder="{{ trans('mes.Коротко опишіть про свою послугу') }}">{{ isset($artist->description_ua) ? $artist->description_ua : ''}}</textarea>
                                        {!! $errors->first('description_ua', '<p class="help-block">   </p>') !!}


                                    </div>
                                </div>
                                <div class="tab-pane fade " id="description-ru" role="tabpanel"
                                     aria-labelledby="ru-description">
                                    <div class="text-user">

                                        <label>
                                            <input type="text" name="name_ru" required="required"
                                                   value="{{ isset($artist->name_ru) ? $artist->name_ru : ''}}"
                                                   placeholder="Введите название: например Кучер Сергей, Агентство Ваша Пленка">
                                        </label>

                                        <textarea name="description_ru" rows="3" required="required"
                                                  placeholder="Кратко опишите о своей услуге">{{ isset($artist->description_ru) ? $artist->description_ru : ''}}</textarea>
                                        {!! $errors->first('description_ru', '<p class="help-block">  </p>') !!}


                                    </div>
                                </div>
                            </div>
                            <div class="list-info">
                                <ul class="detail-user">
                                    <li class="detail-user-item">
                                        <i class="far fa-money-bill-alt"></i>
                                        <input type="text" name="price" placeholder="{{ trans('mes.ціна в грн') }}"
                                               value="{{ isset($artist->price) ? $artist->price : ''}}">
                                    </li>
                                    <li class="detail-user-item">
                                        <i class="fas fa-phone-volume"></i>
                                        <input class="phone" name="phone" type="text" placeholder="{{ trans('mes.Номер телефону') }}"
                                               value="{{ isset($artist->phone) ? $artist->phone : ''}}">
                                    </li>
                                    <li class="detail-user-item">
                                        <i class="fas fa-paper-plane"></i>
                                        <input class="email" name="email" type="email" placeholder="{{ trans('mes.E-mail адреса') }}"
                                               value="{{ isset($artist->email) ? $artist->email : ''}}">
                                    </li>
                                    <li class="detail-user-item category-user-item">
                                        <h4>{{ trans('mes.Категорії свят для яких надаєте послуги') }}: </h4>


                                        @foreach($partyCategories as $category)

                                        <input {{ $artistPartyCategories==$category->id  ? 'checked' : '' }}  class="category-check"
                                        type="radio" id="radio{{ $category->id }}" name="party_category"
                                        value="{{ $category->id }}">
                                        <label for="radio{{ $category->id }}">
                                            <span class="check"></span>{{ $category->name }}</label>

                                        @endforeach


                                        <p class="info-category">
                                            {{ trans('mes.Щоб обрати декілька категорій придбайте один із') }}
                                            <a href="/ads">PRO</a>
                                            {{ trans('mes.акаунтів') }}
                                        </p>


                                    </li>

                                    <li class="detail-user-item">

                                        {{--<multiselect  :options="listCategory" :multiple="true" :close-on-select="false"  values="category" :value="{'category':'Тамада'}" placeholder="Оберіть категорію" class="category-dropdown" label="category" track-by="category">--}}
                                            {{--<span slot="noResult">Oops! No elements found. Consider changing the search query.</span>--}}
                                            {{--</multiselect>--}}



                                        {{--<select--}}
                                        {{--style="width: 100%;--}}
                                        {{--border: 0;--}}
                                        {{--border-bottom: 1.5px solid red;--}}
                                        {{--background: rgba(255,0,0,.7);--}}
                                        {{--color: #fff;--}}
                                        {{--padding: 5px 10px;--}}
                                        {{--display: inline;"--}}
                                        {{--name="hero"  --}}

                                        {{--class="multiselect"--}}


                                        {{-->--}}
                                        {{--<option>Выберите героя</option>--}}
                                        {{--<option value="t1"></option>--}}
                                        {{--<option value="t2"></option>--}}
                                        {{--<option value="t3"></option>--}}
                                        {{--<option selected value="t4"> </option>--}}
                                        {{--</select>--}}





                                        {{--<multiselect  v-model="valueCategory" :options="listCategory" :multiple="true" :close-on-select="true" :resetAfter="true"  values="<?=$category_lang?>" placeholder="{{ trans('mes.Оберіть категорію') }}" class="category-dropdown" label="<?=$category_lang?>" track-by="<?=$category_lang?>" @open="dispatchAction">--}}
                                            {{--<span slot="noResult">{{ trans('mes.Нічого не знайдено') }}</span>--}}
                                            {{--</multiselect>--}}

                                        {{--<multiselect v-model="valueCategory" :options="listCategory" :multiple="true" :close-on-select="false" values="category" placeholder="Оберіть категорію" class="category-dropdown" label="category" track-by="category">--}}
                                            {{--<span slot="noResult">Oops! No elements found. Consider changing the search query.</span>--}}
                                            {{--</multiselect>--}}

                                        <multiselect v-model="valueCategory" :options="listCategory" :multiple="true" :close-on-select="false"  values="<?=$category_lang?>" placeholder="Оберіть категорію" class="category-dropdown" label="<?=$category_lang?>" track-by="<?=$category_lang?>">
                                            <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
                                        </multiselect>


                                    </li>

                                    <li class="detail-user-item">

                                        {{--<multiselect :value="{<?=$city_lang?>:'Алушта'}" v-model="valueCity" name="{{$city_lang}}" :options="listCity" :multiple="true"--}}
                                                         {{--:close-on-select="false" group-values="{{$city_lang}}" group-label="{{$region_lang}}"--}}
                                                         {{--:group-select="true" placeholder="{{ trans('mes.Оберіть місто') }}"--}}
                                                         {{--track-by="{{$city_lang}}" label="{{$city_lang}}" class="city-dropdown">--}}
                                            {{--<span slot="noResult">{{ trans('mes.Нічого не знайдено') }}</span>--}}
                                            {{--</multiselect>--}}






                                    </li>
                                    <li class="detail-user-item">

                                        {{--<div class="select-calc js-category">--}}


                                            {{--@if ( app()->getLocale() == 'ua')--}}
                                            {{--<multiselect--}}

                                            {{--v-model="valueCategory" name="category"--}}
                                            {{--:options="listCategory"--}}
                                            {{--:max="3"--}}



                                            {{--:multiple="true" :close-on-select="true"--}}

                                            {{--placeholder="{{ trans('mes.Оберіть категорію') }}"--}}
                                            {{--class="category-dropdown" label="category_ua"--}}
                                            {{--track-by="id">--}}
                                            {{--<span slot="noResult">{{ trans('mes.Нічого не знайдено') }}--}}

                                            {{--</span>--}}
                                            {{--</multiselect>--}}

                                            {{--@elseif ( app()->getLocale() == 'ru')--}}
                                            {{--<multiselect v-model="valueCategory" name="category_ru"--}}
                                                             {{--:options="listCategory"--}}
                                                             {{--:multiple="true" :close-on-select="true"--}}
                                                             {{--placeholder="{{ trans('mes.Оберіть категорію') }}"--}}
                                                             {{--class="category-dropdown" label="category_ru"--}}
                                                             {{--track-by="category_ru">--}}
                                                {{--<span slot="noResult">{{ trans('mes.Нічого не знайдено') }}--}}
                                            {{--.</span>--}}
                                                {{--</multiselect>--}}


                                            {{--@endif--}}
                                            {{--</div>--}}
                                    </li>

                                    <li class="detail-user-item">
                                        {{--<div class="select-calc js-city">--}}


                                            {{--@if ( app()->getLocale() == 'ua')--}}
                                            {{--<multiselect v-model="valueCity" name="city_ua" :options="listCity"--}}
                                                             {{--:multiple="true"--}}
                                                             {{--:close-on-select="true" group-values="city"--}}
                                                             {{--group-label="region_ua"--}}
                                                             {{--:group-select="true"--}}
                                                             {{--placeholder="{{ trans('mes.Оберіть місто') }}"--}}
                                                             {{--track-by="id" label="name_ua" class="city-dropdown">--}}
                                                {{--<span slot="noResult">{{ trans('mes.Нічого не знайдено') }}</span>--}}
                                                {{--</multiselect>--}}

                                            {{--@elseif ( app()->getLocale() == 'ru')--}}
                                            {{--<multiselect v-model="valueCity" name="city_ru" :options="listCity"--}}
                                                             {{--:multiple="true"--}}
                                                             {{--:close-on-select="true" group-values="city"--}}
                                                             {{--group-label="region_ru"--}}
                                                             {{--:group-select="true"--}}
                                                             {{--placeholder="{{ trans('mes.Оберіть місто') }}"--}}
                                                             {{--track-by="id" label="name_ru" class="city-dropdown">--}}
                                                {{--<span slot="noResult">{{ trans('mes.Нічого не знайдено') }}</span>--}}
                                                {{--</multiselect>--}}


                                            {{--@endif--}}


                                            {{--</div>--}}
                                    </li>

                                    <li class="detail-user-item category-user-item">


                                        <div class="send-calculation">


                                            <input id="save_date" style="display:block;"
                                                   class="btn btn-primary send-btn" type="submit"
                                                   value="{{ trans('mes.Зберегти') }}">

                                        </div>


                                    </li>

                                </ul>

                            </div>


                        </form>
                    </div>


                </div>

            </div>
        </div>


    </div>
    <div class="container">
        <div class="user-tab lk-tab">
            <ul class="nav nav-tabs user-tab-content" role="tablist" id="tab-lk">
                <li class="nav-item">
                    <a class="nav-link active" id="photo-tab" data-toggle="tab" href="#photo" role="tab"
                       aria-controls="photo" aria-selected="true">{{ trans('mes.Фото') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="video-tab" data-toggle="tab" href="#video" role="tab"
                       aria-controls="video"
                       aria-selected="false">{{ trans('mes.Відео') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="video-tab" data-toggle="tab" href="#music" role="tab"
                       aria-controls="music"
                       aria-selected="false">{{ trans('mes.Аудіо') }}</a>
                </li>
                <li class="nav-item rev-item">
                    <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab"
                       aria-controls="reviews" aria-selected="false">{{ trans('mes.Відгуки') }} <span
                                class="number">{{count($reviews)}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="publications-tab" data-toggle="tab" href="#publications" role="tab"
                       aria-controls="publications" aria-selected="false">{{ trans('mes.Публікації') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="award-tab" data-toggle="tab" href="#award" role="tab"
                       aria-controls="award"
                       aria-selected="false">Нагороди</a>
                </li>
                <li class="nav-item message-item">
                    <a class="nav-link" id="message-tab" data-toggle="tab" href="#message" role="tab"
                       aria-controls="message" aria-selected="false">{{ trans('mes.Повідомлення') }} <span class="number">{{ $unreadMessages }}</span></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="photo" role="tabpanel" aria-labelledby="photo-tab">


                    <form method="post" action="{{ url('/lk-page/save_images_gallery/' . Auth::id()) }}"
                          enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                        {{ csrf_field() }}
                        <div class="dz-message">
                            <div class="col-xs-8">
                                <div class="new-photo-user">
                                    <p> {{ trans('mes.блок для загрузки нових фото') }} </p>
                                </div>
                            </div>
                        </div>
                        <div class="fallback">
                            <input type="file" name="file" multiple="multiple"
                                   accept="image/jpg, image/jpeg, image/png">
                        </div>
                    </form>


                    <div class="gallery-show">
                        <div class="gallery-container">
                            {{--<div class="gallery-item">--}}
                                {{--<a data-fancybox="group-photo" class="lightbox fancybox" href="/img/bg/home.jpg"--}}
                                       {{--data-lightbox="roadtrip">--}}
                                    {{--<img class="img-fluid" src="/img/bg/home.jpg">--}}
                                    {{--</a>--}}
                                {{--<form class="like-photo"><input id="photo-1" type="checkbox"><label for="photo-1"--}}
                                                                                                        {{--aria-label="like">❤--}}
                                        {{--<span class="number">1027</span>--}}
                                        {{--</label>--}}
                                    {{--</form>--}}
                                {{--</div>--}}
                            {{--<div class="gallery-item">--}}
                                {{--<a data-fancybox="group-photo" class="lightbox fancybox" href="/img/bg/home.jpg"--}}
                                       {{--data-lightbox="roadtrip">--}}
                                    {{--<img class="img-fluid" src="/img/bg/home.jpg">--}}
                                    {{--</a>--}}

                                {{--</div>--}}


                            @foreach($photos as $photo)



                            <div class="gallery-item">


                                <a data-fancybox="group-photo" class="lightbox fancybox"
                                   href="/upload/gallery/{{ $photo->photo }}" data-lightbox="roadtrip">


                                    <img class="img-fluid" src="/upload/gallery/sm_{{ $photo->photo }}">
                                </a>

                                <form class="like-photo"><input id="photo-{{$photo->id}}" type="checkbox"><label
                                            for="photo-{{ $photo->id }}" aria-label="like">❤
                                        <span class="number">{{ $photo->likes }}</span>
                                    </label>
                                </form>
                                <br>

                                <form class="" method="post"
                                      action="{{ url('/lk-page/delete_images_gallery/' . Auth::id()) }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="gallery_id" value="{{$photo->id}}">
                                    <input type="submit" title="{{ trans('mes.видалити фото') }}"

                                           style="position: absolute;top: 0px;  left: 0px;"
                                           class="btn btn-sm btn-default" value="x">

                                </form>


                            </div>


                            @endforeach

                        </div>

                        {{--Dropzone Preview Template--}}
                        <div id="preview" style="display: none;">

                            <div class="dz-preview dz-file-preview">
                                <div class="dz-image"><img data-dz-thumbnail/></div>

                                <div class="dz-details">
                                    <div class="dz-size"><span data-dz-size></span></div>
                                    <div class="dz-filename"><span data-dz-name></span></div>
                                </div>
                                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span>
                                </div>
                                <div class="dz-error-message"><span data-dz-errormessage></span></div>


                                <div class="dz-success-mark">

                                    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                        <title>Check</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd" sketch:type="MSPage">
                                            <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                  id="Oval-2" stroke-opacity="0.198794158" stroke="#747474"
                                                  fill-opacity="0.816519475" fill="#FFFFFF"
                                                  sketch:type="MSShapeGroup"></path>
                                        </g>
                                    </svg>

                                </div>
                                <div class="dz-error-mark">

                                    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                        <title>error</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd" sketch:type="MSPage">
                                            <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474"
                                               stroke-opacity="0.198794158" fill="#FFFFFF"
                                               fill-opacity="0.816519475">
                                                <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                      id="Oval-2" sketch:type="MSShapeGroup"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        {{ $photos->links() }}


                    </div>
                </div>
                <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
                    <div class="new-video-user">


                        @if(Session::has('video_limit_message'))
                        <p class="alert alert-info">{{ Session::get('video_limit_message') }}</p>
                        @endif

                        <p> {{ trans('mes.Додати відео можна з <span>Youtube </span> та <span>Vimeo</span>') }} </p>

                        <form class="" action="{{ url('/lk-page/save_video_gallery/' . Auth::id()) }}"
                              method="POST">


                            {{ csrf_field() }}
                            <input type="text" name="video" required="required"
                                   placeholder="наприклад: https://www.youtube.com/watch?v=p4Bwko0CqVI або https://vimeo.com/257923996">
                            <button class="add-video">
                                <svg>
                                    <rect x="0" y="0" fill="none" width="170" height="45"></rect>
                                </svg>
                                {{ trans('mes.Додати відео') }}
                            </button>
                        </form>
                    </div>


                    <div class="gallery-show video-gallery">
                        <div class="gallery-container">
                            @foreach($videos as $video)
                            <div class="gallery-item">
                                <a data-fancybox="group-video" class="lightbox iframe"
                                   href="{{ $video->video }}">
                                    @php $videoId = \explode('/', $video->video) @endphp
                                    @if(\mb_strpos($video->video, 'youtu') !== false)
                                    <img class="img-fluid"
                                         src="//img.youtube.com/vi/{{ \end($videoId) }}/sddefault.jpg">
                                    @elseif(\mb_strpos($video->video, 'vimeo') !== false)
                                    <img class="img-fluid"
                                         src="https://i.vimeocdn.com/video/{{ \end($videoId) }}_640x480.jpg">
                                    @endif
                                </a>
                                <form class="like-photo"><input id="video-{{ $video->id }}"
                                                                type="checkbox"><label
                                            for="video-{{ $video->id }}" aria-label="like">❤
                                        <span class="number">{{ $video->likes }}</span>
                                    </label>
                                </form>
                                <form class="" method="post"
                                      action="{{ url('/lk-page/delete_video_gallery/' . Auth::id()) }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="video_id" value="{{$video->id}}">
                                    <input type="submit" title="{{ trans('mes.видалити відео') }}"

                                           style="position: absolute;top: 0px;  left: 0px;"
                                           class="btn btn-sm btn-default" value="x">

                                </form>
                            </div>
                            @endforeach
                        </div>
                        {{ $videos->fragment('video')->links() }}
                    </div>

                </div>
                <div class="tab-pane fade" id="music" role="tabpanel" aria-labelledby="music-tab">
                    <div class="new-music-user">
                        <p>{{ trans('mes.Добавити музичні треки можна Soundcloud. Для цього потрібно в поле що знизу вставити iframe сервісу. Не знаєте як це зробити? Скористайтесь наступною') }}
                            <a
                                    href="#"> {{ trans('mes.підказкою') }}</a></p>
                        <form class="" action="{{ url('/lk-page/save_audio_gallery/' . Auth::id()) }}"
                              method="POST">


                            {{ csrf_field() }}
                            <input type="text" name="audio"
                                   placeholder='наприклад: https://api.soundcloud.com/users/281724041'>
                            <button class="add-video">
                                <svg>
                                    <rect x="0" y="0" fill="none" width="170" height="45"></rect>
                                </svg>
                                {{ trans('mes.Додати аудіо') }}
                            </button>
                        </form>
                    </div>
                    <div class="music-block">
                        @foreach($audio as $au)
                        <iframe id="audio-user" scrolling="no" frameborder="no"
                                src="https://w.soundcloud.com/player/?url={{ $au->audio }}&color=%23ff0000&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>

                        <br>

                        <form class="" method="post"
                              action="{{ url('/lk-page/delete_audio_gallery/' . Auth::id()) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="audio_id" value="{{$au->id }}">
                            <input type="submit" title="{{ trans('mes.видалити це аудіо') }}"

                                   style=""
                                   class="btn btn-sm btn-default" value="{{ trans('mes.видалити') }}">

                        </form>
                        <br>  <br>
                        @endforeach

                    </div>
                </div>

                <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">

                    <div class="rev-block">

                        @foreach($reviews as $review)
                        <div class="rev-item">
                            <div class="rev-user">
                                <img src="{{ $review->user->image }}">
                                <h4>{{ $review->user->email }}</h4>
                                <p><span class="count-rev">1 </span> {{ trans('mes.відгук') }}</p>

                            </div>
                            <div class="rev-description">
                                <span class="count-rating">{{ $review->likes }}.0</span> <br>
                                <span class="date-rev">{{ $review->updated_at->format('d.m.Y') }}</span>
                                <p>{{ $review->text }}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    {{ $reviews->fragment('reviews')->links() }}

                </div>


                <div class="tab-pane fade" id="publications" role="tabpanel" aria-labelledby="publications-tab">
                    {{--<div class="new-publications">--}}
                        {{--<p>Lorem Ipsum - <span>это текст</span>-"рыба", часто используемый в печати и вэб-дизайне.--}}
                            {{--Lorem--}}
                            {{--Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время--}}
                            {{--некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя--}}
                            {{--Lorem--}}
                            {{--Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных--}}
                            {{--изменений--}}
                            {{--пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время--}}
                            {{--послужили--}}
                            {{--публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее--}}
                            {{--время,--}}
                            {{--программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется--}}
                            {{--Lorem--}}
                            {{--Ipsum.</p>--}}
                        {{--<a href="#" class="edit-publications" data-toggle="modal" data-target="#new-news">--}}
                            {{--<svg>--}}
                                {{--<rect x="0" y="0" fill="none" width="166" height="45"></rect>--}}
                                {{--</svg>--}}
                            {{--Опублікувати новину--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    <div class="all-publications">
                        @foreach($articles as $article)


                        <div class="publications-item">
                            <a class="publication-link" href="#" data-toggle="modal"
                               data-target="#edit_news_{{$article->id}}">
                                <img class="img-fluid" src="{{$article->image}}">
                                <h3>{{$article->title}}</h3>
                            </a>
                        </div>


                        @endforeach

                        {{ $articles->links() }}

                    </div>

                </div>
                <div class="tab-pane fade" id="award" role="tabpanel" aria-labelledby="award-tab">
                    <div class="award-description">
                        {{--<p>Lorem Ipsum - <span>это текст</span>-"рыба", часто используемый в печати и вэб-дизайне.--}}
                            {{--Lorem--}}
                            {{--Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время--}}
                            {{--некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя--}}
                            {{--Lorem--}}
                            {{--Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных--}}
                            {{--изменений--}}
                            {{--пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время--}}
                            {{--послужили--}}
                            {{--публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее--}}
                            {{--время,--}}
                            {{--программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется--}}
                            {{--Lorem--}}
                            {{--Ipsum.</p>--}}
                    </div>
                    <div class="all-award">
                        <div class="award-item">
                            <form>
                                <h4>{{ trans('mes.Отримайте') }} <span>100</span> {{ trans('mes.лайків для профіля') }}</h4>
                                <div class="award-img">
                                    <img class="img-fluid" src="/img/award/like.svg">
                                    <div class="award-complate"
                                         style="-webkit-clip-path: polygon(0 {{$user_likes}}%, 100% {{$user_likes}}%, 100% 100%, 0% 100%); clip-path: polygon(0 {{100-$user_likes}}%, 100% {{100-$user_likes}}%, 100% 100%, 0% 100%);">
                                        <img class="img-fluid" src="/img/award/complate/like.svg">
                                    </div>
                                </div>
                                <div class="progress award-progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{$user_likes}}%;"
                                         aria-valuenow="{{$user_likes}}"
                                         aria-valuemin="0" aria-valuemax="100">{{$user_likes}} / 100
                                    </div>
                                </div>
                                <p>{{ trans('mes.Нагорода') }}: <span>5</span> {{ trans('mes.монет') }}</p>
                                <div class="btn-block">
                                    <button class="award-btn">{{ trans('mes.Перевести на рахунок') }}</button>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>


                <div class="tab-pane fade" id="message" role="tabpanel" aria-labelledby="message-tab">
                    <div class="message-container">


                        @if (count($messages_from_users)>0)
                        <ul class="nav nav-tabs" role="tablist">

                            @php $min_key= min($messages_from_users); @endphp

                            @foreach($messages_from_users as $key=>$from)
                            <li class="nav-item">
                                <a class="make_messages_read nav-link {{$key==$min_key ? 'active':''}} "
                                   id="message-user-{{$from}}-tab" data-toggle="tab"
                                   href="#message-user-{{$from}}" role="tab"
                                   data-from="{{$from}}"
                                   aria-controls="message-user-{{$from}}"
                                   aria-selected="false">
                                    <img src="{{ User::getUserById($from)->image ?:'/img/bg/home.jpg' }}">
                                    <span>

                                                @php $artist_from=Artist::where('user_id',$from)->first(); @endphp

                                                {{  $artist_from->{User::getNameByLang($artist_from->id, Lang::getLocale())}  }}
                                            </span>
                                </a>
                            </li>



                            @endforeach


                        </ul>


                        <div class="tab-content">
                            @foreach($messages_from_users as $key=>$from)

                            @php
                            $artist_from=Artist::where('user_id',$from)->first();

                            $mes_arr=[];




                            $all_mes_from=Message::
                            where('from_id',$from)->where('to_id',Auth::id())
                            ->get();

                            $all_mes_to=Message::
                            where('from_id',Auth::id())->where('to_id',$from)
                            ->get();

                            foreach ($all_mes_from as $mes_from){
                            $mes_arr[strtotime($mes_from->created_at)]['from'] =$from;
                            $mes_arr[strtotime($mes_from->created_at)]['text'] =$mes_from->text;
                            }

                            foreach ($all_mes_to as $mes_to){
                            $mes_arr[strtotime($mes_to->created_at)]['from'] =$artist->id;
                            $mes_arr[strtotime($mes_to->created_at)]['text'] =$mes_to->text;
                            }

                            ksort($mes_arr);











                            @endphp

                            <div class="tab-pane fade  {{$key==$min_key? 'show active':''}}  "
                                 id="message-user-{{$from}}" role="tabpanel"
                                 aria-labelledby="message-user-{{$from}}-tab">
                                <ul>

                                    @foreach($mes_arr as $key1=>$value1)

                                    <li class="{{$value1['from']==$artist->id?'send':'received'}} ">
                                        <img class="user-img-msg"
                                             src="{{ User::getUserById($value1['from'])->image ?:'/img/bg/home.jpg' }}">
                                        <h5 class="user-msg-name">
                                            {{

                                            $value1['from']==$artist->id
                                            ?
                                            'Ви'
                                            :
                                            $artist_from->{User::getNameByLang($value1['from'], Lang::getLocale())}

                                            }}

                                        </h5>
                                        <p class="text-msg">
                                            {{$value1['text']}}
                                        </p>
                                        <span class="date-message">
                                                        {{date('d-m-Y H:i',$key1)}}
                                                    </span>
                                    </li>


                                    @endforeach






                                </ul>
                                <div class="send-msg-block">
                                    <form method="post" enctype="multipart/form-data" action="/message/create">
                                        @csrf
                                        <input type="hidden" name="to_id" value="{{  $from }}" required>
                                        <textarea name="text" placeholder="{{ trans('mes.Напишіть повідомлення') }}..."></textarea>
                                        <button class="send-msg">
                                            <svg>
                                                <rect x="0" y="0" fill="none" width="166" height="45"></rect>
                                            </svg>
                                            {{ trans('mes.Надіслати') }}
                                        </button>
                                    </form>
                                </div>
                            </div>

                            @endforeach


                        </div>
                        @endif

                    </div>
                </div>


            </div>
        </div>

    </div>
</div>


<script src="/js/dropzone.js"></script>
<script src="/js/dropzone-config.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<link rel="stylesheet" href="/css/dropzone.css">



@if(Auth::check())
@foreach($articles as $article)
<div class="modal fade modal-edit-news" id="edit_news_{{$article->id}}" tabindex="-1" role="dialog"
     aria-labelledby="new-news" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ trans('mes.Редагування публікації') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" enctype="multipart/form-data"
                  action="/lk-page/edit_article/{{$article->user_id}}">
                @csrf
                <input type="hidden" name="user_id" value="{{ Auth::id() }}" required>
                <div class="modal-body">
                    <div class="media"></div>
                    <label>{{ trans('mes.Заголовок новини') }}
                        <input type="text" name="title" value="{{$article->title}}" required>
                        <input type="hidden" name="article_id" value="{{$article->id}}">
                    </label>

                    <label>{{ trans('mes.Фото-прев\'ю новини') }}
                        <input type="file" id="img-news" name="image">
                        <img width="64px" src="{{$article->image}}">
                        <span class="check-img">{{ trans('mes.Обрати інше фото') }}</span>
                    </label>
                    <label>{{ trans('mes.Текст новини') }}
                        <textarea required id="article-edit-ckeditor-{{$article->id}}" name="text">

                                    {{$article->text}}

                                </textarea>
                        <script>
                            CKEDITOR.replace('article-edit-ckeditor-{{$article->id}}');
                        </script>
                    </label>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="new-news-btn">
                        <svg>
                            <rect x="0" y="0" fill="none" width="166" height="45"></rect>
                        </svg> {{ trans('mes.Зберегти') }}</button>
                </div>
            </form>
        </div>


    </div>
</div>
@endforeach
@endif



@endsection










