@extends('layouts.app')

@section('content')
    <div id="main-app">
        @include('partial.week_photo', ['title' => trans('mes.Про сервіс') ])
        @include('partial.breadcrumb', ['current' => trans('mes.Про нас')  ])
        <div class="about-page container">

            @if ( Lang::getLocale() == 'ua')
                {!! $html_ua !!}
            @elseif ( Lang::getLocale() == 'ru')
                {!! $html_ru !!}
            @endif
        </div>
    </div>
@endsection