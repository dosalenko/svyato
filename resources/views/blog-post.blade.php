@extends('layouts.app')

<?php
$lang= 'name_'.Lang::getLocale();
?>

@section('content')
    <div id="main-app">
        @include('partial.week_photo', ['title' =>  trans('mes.Блог порталу').' "'.trans('mes.Моє Свято').'"'])
        <div class="breadcrumb-block">
            <ul class="breadcrumb-list">
                <li class="breadcrumb-item">
                    <a href="{{ route('main') }}" class="breadcrumb-link">{{trans('mes.Головна')}}</a>
                </li>
                <li class="breadcrumb-item ">
                    <a href="{{ route('blog') }}" class="breadcrumb-link">{{trans('mes.Блог')}}</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="" class="breadcrumb-link">{{ $article->title }}</a>
                </li>
            </ul>

        </div>

        <div class="blog-page">
            <div class="container">
                <div class="row">
                    <div class="blog-detail">
                        <h2 class="blog-post-title">{{ $article->title }}</h2>
                        <ul class="post-info">
                            <li>{{trans('mes.Дата')}}: <span>{{ $article->created_at->format('d.m.Y') }}</span></li>
                            <li>{{trans('mes.Автор')}}: <a href="{{ route('user-page', ['user_id' => $article->user_id]) }}">{{ $article->author }}</a> </li>
                            <li>{{trans('mes.Комментарі')}}: <a href="">{{ $article->comments_cnt }}</a></li>
                            <li>{{trans('mes.Рубрики')}}: @foreach($article->categories as $category)<a href="/blog?category={{$category->id}}"><?=$category->{$lang}; ?></a>, @endforeach</li>
                        </ul>
                        <hr class="divider">
                        <div>{!! $article->text !!}</div>

                        <div class="comment-container">
                            <h3>{{trans('mes.Коментарі')}} <span>({{ $article->comments->count() }})</span>:</h3>
                        </div>
                        <div class="container">
                            @foreach($comments as $comment)
                                <div class="card">
                                    <div class="row ">
                                        <div class="col-md-4 rounded-circle">
                                            <img src="{{ asset($comment->user->image) }}" class="w-100" />
                                            <span class="w-100 text-center d-inline-block">{{ $comment->user->email }}</span>
                                        </div>
                                        <div class="col-md-6 p-3">
                                            <div class="text-danger text-right">{{ $comment->created_at->format('H:i d.m.Y') }}</div>
                                            <hr>
                                            <div class="card-block px-3">
                                                <p class="card-text">{{ $comment->text }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            {{ $comments->links() }}
                        </div>
                        @if(Auth::check())
                            <div class="col-md-12 text-center p-relative">
                                <h2>{{trans('mes.Додати коментар')}}:</h2>
                            <form method="post" action="{{ route('save-comment') }}">
                                @csrf
                                <input type="hidden" name="article_id" required value="{{$article->id}}">
                                <input type="hidden" name="user_id" required value="{{Auth::id()}}">
                                <textarea class="w-100" name="text" placeholder="{{trans('mes.Текст коментаря')}}" style="height: 200px;" required title="{{trans('mes.Блог')}}Введіть текст"></textarea>
                                <p><button type="submit">{{trans('mes.Додати')}}</button></p>

                            </form>
                            </div>
                        @endif
                    </div>
                    <!-- /.blog-main -->

                    <div class="blog-sidebar">
                        <div class="sidebar-module">
                            <h4>{{trans('mes.Рубрики')}}:</h4>
                            @foreach($article_categories as $tag)
                            <a href="/blog?category={{$tag->id}}" class="tags-list">

                                <?=$tag->{$lang}; ?>

                            </a>
                            @endforeach
                        </div>

                        <div class="sidebar-module">
                            <h4>{{trans('mes.Останні новини')}}:</h4>
                            @foreach($latest_news as $oneArticle)
                            <div class="popular-post">
                                <a href="{{ route('blog-post', ['post_id' => $oneArticle->id]) }}"><img src="{{ asset($oneArticle->image) }}" alt="" /></a>
                                <div class="description-post-user">
                                    <a href="{{ route('blog-post', ['post_id' => $oneArticle->id]) }}" class="title-post"><h3>{{ $oneArticle->title }}</h3></a>
                                    <p class="description-min">{!! Stringy\Stringy::create($oneArticle->text)->safeTruncate(120, '...') !!}</p>
                                    <span>
                                        <i class="fas fa-tag"></i>
                                        @foreach($oneArticle->categories as $i => $tag)
                                            <a href="/blog?category={{$category->id}}"><?= $tag->{$lang}?></a>
                                            @if($i > 2)
                                                    @break
                                             @endif
                                        @endforeach
                                    </span>
                                    <span>
                                        <i class="fas fa-user"></i>
                                        <a href="{{ route('user-page', ['user_id' => $oneArticle->user_id]) }}">{{ $oneArticle->author }}</a>
                                    </span>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="sidebar-module">
                            <h4>{{trans('mes.Кращі автори')}}:</h4>
                            @foreach($top_authors as $topAuthor)
                                @if(empty($topAuthor->articles_count))
                                    @continue
                                @endif
                                <div class="popular-author">
                                    <a href="{{ route('user-page', ['user_id' => $topAuthor->user_id]) }}"><img src="{{ asset($topAuthor->user->image)  }}" alt="" /></a>
                                    <a href="{{ route('user-page', ['user_id' => $topAuthor->user_id]) }}"><h3 class="title-author">{{ $topAuthor->name }}</h3></a>
                                    <div class="number-publications">
                                        <i class="fab fa-leanpub"></i> {{trans('mes.зробив')}} <a href="#" class="number">{{ $topAuthor->articles_count }} </a> {{trans('mes.публікацій в блозі')}}
                                    </div>
                                </div>
                            @endforeach
                        </div>


                    </div>
                    <!-- /.blog-sidebar -->
                </div>
            </div>


        </div>
    </div>
@endsection    