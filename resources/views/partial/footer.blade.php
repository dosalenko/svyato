<div class="footer">
    <div class="container">
        <div class="row">
            <ul class="footer-language">

                <?php
                $link = explode('/',$_SERVER['REQUEST_URI']);

                ($link[1]=='ru' or $link[1]=='ua') ? $output_link = implode('/',array_slice($link, 2)) :   $output_link = implode('/',array_slice($link, 1)) ;
                ?>

                <li  @if ( app()->getLocale() == 'ua')  class="active"  @endif >

                    <a href="/ua/{{$output_link}}"><img src="/img/language/ukraine.svg">Ua</a>
                </li>

                <li  @if ( app()->getLocale() == 'ru')  class="active"  @endif >
                    <a href="/ru/{{$output_link}}"><img src="/img/language/russia.svg"> Ru</a>
                </li>

            </ul>
            <ul class="footer-link">
                <li><a href="{{ route('about') }}">{{ trans('mes.Про сервіс') }}</a></li>
                <li><a href="{{ route('about') }}">{{ trans('mes.Політика конфіденційності') }}</a></li>
                <li><a href="{{ route('ads') }}">{{ trans('mes.Умови реклами') }}</a></li>

            </ul>
            @inject('Contacts', 'App\Contacts')
            @php $firstContact = $Contacts::limit(1)->first() @endphp
            <ul class="footer-social">
                <li><a href="{{ $firstContact->instagram }}"><i class="fab fa-instagram"></i></a></li>
                <li><a href="{{ $firstContact->facebook }}"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="{{ $firstContact->youtube }}"><i class="fab fa-youtube"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="copy container-fluid">
    <div class="row">
        <p>&#169 2018 <a href="{{ route('main') }}">{{ trans('mes.Моє Свято') }}</a></p>
        <p class="text-copyright">{{ trans('mes.Всі матеріали на порталі') }} <span>{{ trans('mes.Моє Свято') }}</span>
            {{ trans('mes.захищаються законом про авторські
            права. Використання і копіювання матеріалів без відома адміністрації сайту заборонено') }}</p>
    </div>
</div>