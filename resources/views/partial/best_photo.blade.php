
<!-- user-like-modal -->
<div class="modal fade user-like-modal" id="user-like-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ trans('mes.Оцінили') }} <span id="likes_cnt"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-like-content js-modal-like-content">
                </div>
            </div>
        </div>
    </div>
</div>