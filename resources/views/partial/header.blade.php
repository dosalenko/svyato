<header>
    <nav class="navbar navbar-expand-md">
        <a class="navbar-brand" href="/">{{ trans('mes.Моє Свято') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#svyato_nav"
                aria-controls="svyato_nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="svyato_nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('main') }}"> {{ trans('mes.Головна') }}</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('about') }}">{{ trans('mes.Про сервис') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('catalog') }}">{{ trans('mes.Каталог послуг') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('blog') }}">{{ trans('mes.Блог') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ads') }}">{{ trans('mes.Реклама') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contacts') }}">{{ trans('mes.Контакти') }}</a>
                </li>
                @if(Auth::check())
                    <li class="nav-item">
                        <a class="nav-link order-pro" href="{{ route('ads') }}">{{ trans('mes.купити PRO') }}</a>
                    </li>
                    <li class="nav-item user-authorization user-active">
                        <a class="nav-link" href="#">

                            <img class="user-img" src="{{ Auth::user()->image }}">
                            @inject('Messages', 'App\Message')
                            @php $unreadMessages = $Messages::where('to_id', Auth::id())->where('is_read', 0)->count(); @endphp
                            <div class="notification">
                                <span class="number">{{ $unreadMessages }}</span>
                            </div>

                            <span class="user-name">{{ Auth::user()->email }}</span>
                            <i class="fas fa-chevron-down"></i>
                        </a>
                        <ul class="drop-menu">
                            <li><a href="/lk-page/{{Auth::id()}}#message"><i class="fas fa-paper-plane"></i>{{ trans('mes.Повідомлення') }} :
                                    <div class="mail-info"><span class="number">{{ $unreadMessages }}</span></div>
                                </a></li>
                            <li><a href="{{ route('user-page',['id' => Auth::id()])}}"><i class="fas fa-user-alt"></i>{{ trans('mes.Мій профіль') }} </a></li>
                            <li><a href="{{ route('lk-page',['id' => Auth::id()])}}"><i class="fas fa-dollar-sign"></i>{{ trans('mes.Мій рахунок') }}</a></li>
                            <li><a href="{{ route('lk-page',['id' => Auth::id()])}}"><i class="fas fa-user-edit"></i>{{ trans('mes.Редагувати профіль') }} </a></li>
                            <li><a href='/lk-page/{{Auth::id()}}/#publications'><i class="fab fa-leanpub"></i>{{ trans('mes.Мої публікації') }} </a></li>
                            <li><a href="{{ route('lk-page',['id' => Auth::id()])}}"><i class="fas fa-gavel"></i> {{ trans('mes.Аукціон TOP-позицій') }}</a></li>
                            <li><a href="{{ route('ads') }}"><i class="fas fa-credit-card"></i>{{ trans('mes.Придбати PRO-акаунт') }} </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                            class="fas fa-sign-out-alt"></i>{{ trans('mes.Вихід') }} </a></li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                @csrf
                            </form>
                        </ul>
                    </li>
                @else
                    <li class="nav-item user-authorization"><a class="nav-link" href="#" data-toggle="modal"
                                                               data-target="#authorization-modal"><i
                                    class="far fa-user"></i>{{ trans('mes.Вхід / Реєстрація') }} </a></li>
                @endif
            </ul>
        </div>
    </nav>
</header>
<!-- Modal links -->

@if($errors->any())
    <span id="error" class="hidden" data-tab="{{ $errors->first('route') }}"></span>
@endif

<div class="modal fade" id="authorization-modal" tabindex="-1" role="dialog" aria-labelledby="authorization-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <ul class="nav nav-tabs" role="tablist" id="authorization-registration">
                    <li class="nav-item">
                        <a class="nav-link active" id="authorization-tab" data-toggle="tab" href="#authorization"
                           role="tab" aria-controls="authorization" aria-selected="true">
                            <h5 class="modal-title">{{ trans('mes.Вхід в особистий кабінет') }}</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="registration-tab" data-toggle="tab" href="#registration" role="tab"
                           aria-controls="registration" aria-selected="false">
                            <h5 class="modal-title">{{ trans('mes.Реєстрація нового користувача') }}</h5>
                        </a>
                    </li>

                </ul>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="authorization" role="tabpanel"
                     aria-labelledby="authorization-tab">
                    <form method="post" action="{{ route('login') }}">
                        @csrf
                        <div class="modal-body">
                            <div class="site-authorization">
                                <label>
                                    <i class="far fa-user"></i>
                                    <input type="text" placeholder="{{ trans('mes.Введіть Ваш логін / email / номер телефону') }}" required
                                           id="email" name="email" value="{{ old('email') }}" autofocus autocomplete>
                                    @if($errors->has('email'))<span class="text-danger font-italic">{{ $errors->first('email')  }}</span>@endif
                                </label>
                                <label>
                                    <i class="fas fa-unlock-alt"></i>
                                    <input type="password" placeholder="{{ trans('mes.Введіть Ваш пароль') }}" required name="password" autocomplete>
                                    @if($errors->has('password'))<span class="text-danger font-italic">{{ $errors->first('password')  }}</span>@endif
                                </label>
                                <div class="info-authorization">
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : ''}} >
                                    <label for="remember"><span></span>{{ trans('mes.Запам\'ятати мене') }}</label>
                                </div>
                            </div>
                            <div class="or">
                                <p>{{ trans('mes.Або') }}</p>
                            </div>
                            <div class="social-authorization">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                            <span>Facebook</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-google"></i>
                                            <span>Gmail</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-instagram"></i>
                                            <span>Instagram</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="help-user">
                                <a href="#" data-toggle="modal" data-target="#recover-password" data-dismiss="modal"
                                   aria-label="Close">{{ trans('mes.Забули пароль?') }}</a>
                                <a href="#" class="register-user" id="register-user">{{ trans('mes.Реєстрація нового користувача') }}</a>

                            </div>
                            <button type="submit" class="authorization-btn">
                                <svg><rect x="0" y="0" fill="none" width="166" height="45"></rect></svg>
                                {{ trans('mes.Увійти') }}
                            </button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="registration" role="tabpanel" aria-labelledby="registration-tab">
                    <form method="post" action="{{ route('register') }}">
                        @csrf
                        <div class="modal-body">
                            <div class="site-authorization">
                                {{--<label>--}}
                                    {{--<i class="far fa-user"></i>--}}
                                    {{--<input type="text" placeholder="{{ trans('mes.Введіть Ваше  ім\'я') }}" required--}}
                                           {{--name="name" value="{{ old('name') }}" autofocus autocomplete>--}}
                                    {{--@if($errors->has('name'))<span class="text-danger font-italic">{{ $errors->first('name')  }}</span>@endif--}}
                                {{--</label>--}}
                                <label>
                                    <i class="far fa-user"></i>
                                    <input type="email" placeholder="{{ trans('mes.Введіть Ваш  email') }}" required
                                           name="email" value="{{ old('email') }}" autofocus autocomplete>
                                    @if($errors->has('email'))<span class="text-danger font-italic">{{ $errors->first('email')  }}</span>@endif
                                </label>
                                <label>
                                    <i class="fas fa-unlock-alt"></i>
                                    <input type="password" placeholder="{{ trans('mes.Введіть Ваш пароль') }}" required name="password" autocomplete>
                                    @if($errors->has('password'))<span class="text-danger font-italic">{{ $errors->first('password')  }}</span>@endif
                                </label>
                                <div class="info-authorization">
                                    <input type="checkbox" id="confidentality" name="confidentality" required value="1">
                                    <label for="confidentality">
                                        <span></span>
                                        @if($errors->has('confidentality'))
                                            <span class="text-danger font-italic">{{ $errors->first('confidentality')  }}</span>
                                        @else
                                            {{ trans('mes.Я погоджуюсь з') }} <a href="{{ route('about') }}" target="_blank">{{ trans('mes.політикою конфіденційності') }}</a>
                                        @endif

                                    </label>

                                </div>
                            </div>
                            <div class="or">
                                <p> {{ trans('mes.Або') }}</p>
                            </div>
                            <div class="social-authorization">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                            <span>Facebook</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-google"></i>
                                            <span>Gmail</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-instagram"></i>
                                            <span>Instagram</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="help-user">
                                <h5 class="modal-title">{{ trans('mes.Вже зареєстровані?') }}</h5>
                                <a href="" class="log-in" id="log-in">{{ trans('mes.Увійти в особистий кабінет') }}</a>
                            </div>
                            <button type="submit" class="authorization-btn">
                                <svg><rect x="0" y="0" fill="none" width="166" height="45"></rect></svg>
                                {{ trans('mes.Зареєструватись') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="recover-password" tabindex="-1" role="dialog" aria-labelledby="recover-password"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ trans('mes.Відновлення паролю') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                    <p>{{ trans('mes.Забули свій пароль? Не біда! Напишіть e-mail, що вказали при реєстрації і ми надішлемо Вам листа для відновлення') }}</p>
                    <label>
                        <i class="fas fa-at"></i>
                        <input type="text" placeholder="{{ trans('mes.Введіть Ваш  email') }}">
                    </label>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="authorization-btn">
                        <svg><rect x="0" y="0" fill="none" width="166" height="45"></rect></svg>
                        {{ trans('mes.Відновити') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>