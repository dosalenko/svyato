<div class="breadcrumb-block">
    <ul class="breadcrumb-list">
        <li class="breadcrumb-item">
            <a href="{{route('main')}}" class="breadcrumb-link">Головна</a>
        </li>
        <li class="breadcrumb-item active">
            <a href="" class="breadcrumb-link">{{ $current }}</a>
        </li>
    </ul>
</div>