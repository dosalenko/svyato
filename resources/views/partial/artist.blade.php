<?php
$category_lang = 'category_' . Lang::getLocale();
?>

<div class="user-block">
    {{--<div class="user-image" style="background: url({{ $artist->user->id  }}); background-position: center; background-repeat: no-repeat; background-size: cover;">--}}
    <div class="user-image"
         style="background: url('/img/bg/home.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
        <div class="users-last-like">
            @php $userLikes = $artistLikes->where('artist_id', $artist->id) @endphp
            @foreach($userLikes as $userLike)
                <a href="" class="user-like"
                   style="background: url({{ asset($userLike->image) }}); background-position: center; background-repeat: no-repeat; background-size: cover;"></a>
            @endforeach
            <a href="" class="user-like last js-like-modal" data-toggle="modal" data-target="#user-like-modal"
               data-users="{{$userLikes->toJson()}}"
               data-number="+{{ $artist->likes_cnt }}"
               style="background: url('/img/bg/home.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;"></a>
        </div>
        @if(Auth::check())
            <form class="like-add" name="like_form" method="post" action="{{ route('save-like') }}">
                @csrf
                <input type="hidden" class="user_id_liker" name="user_id" value="{{ Auth::id() }}">
                {{--<input type="hidden" name="artist_id" value="{{ $artist->user->id }}">--}}
                <input type="hidden" class="artist_id_liked" name="artist_id" value="{{ $artist->id }}">
                <input id="user-heart{{  $artist->id }}" type="checkbox" value="1"
                       @if($userLikes->where('user_id', Auth::id())->isNotEmpty())
                       checked
                        @endif><label class="js-add-like" id="1" for="user-heart{{  $artist->id  }}"
                                      aria-label="like">❤</label>
            </form>
        @endif
    </div>
    <div class="user-info">
        <h5 class="user-name"><a
                    href="{{ route('user-page', ['user_id' => $artist->user_id]) }}">{{ $artist->name }}</a> @if($artist->is_pro && $main !== true)
                <span>pro</span>@endif </h5>
    </div>
    <ul class="user-list-city">
        <i class="fas fa-map-marker-alt"></i>

        @php $cities = $artistCities->where('artist_id', $artist->id);@endphp

        @if(count($cities)>0)
            @foreach($cities as $city)
                <li><a href="{{ route('catalog') . '?cities=' . $city->city_id }}">

                        @if ( Lang::getLocale() == 'ua')
                            {{$city->name_ua}}
                        @elseif ( Lang::getLocale() == 'ru')
                            {{$city->name_ru}}
                        @endif


                    </a>

                </li>
            @endforeach
        @else
            <li>
                @php if ($artist->city_id>0){
                $artist_city = \App\City::where('id', $artist->city_id )->first();
                 echo $artist_city->{$city_lang} ;
                } else {
                 echo '';
                }

                @endphp


            </li>
        @endif
    </ul>
    @if(isset($main) && $main !== true)
        <ul class="user-list-category">
            <i class="fas fa-user-tag"></i>
            @inject('categoriesModel', App\Category)


            @php $allCategories = $categoriesModel::all(['id', 'category_'.Lang::getLocale()])  @endphp


            @if(count($artist->artistsCategories)>0)


            @foreach($artist->artistsCategories as $artistCategory)
                @php $category = $allCategories->where('id', $artistCategory->category_id)->first() @endphp
                    <li>
                        <a href="{{ route('catalog') . '?category=' . $category->id }}"><?= $category->{$category_lang} ?></a>
                    </li>
            @endforeach

            @else

                <li>


                    @php
                        $single_cat= \App\Category::where('id', $artist->category_id )->first();
                        echo $single_cat->{$category_lang};
                    @endphp

                </li>
            @endif


        </ul>
    @endif
    <div class="user-info-service">
        <div class="phone-block">
            <a href="" class="phone-user"><i class="fas fa-phone"></i>{{ $artist->phone }}</a>
        </div>
        @if(!empty($artist->old_price))
            <div class="price-block"><i class="far fa-money-bill-alt"></i><span class="old-price">{{ $artist->old_price }}
                    грн.</span> <span class="new-price">{{ $artist->price }} грн.</span></div>
        @endif
    </div>
</div>