<div class="main-window">
    @inject('artistPhotos', 'App\ArtistPhoto')
    @php $bestPhoto = $artistPhotos->bestWeekPhoto()->first() @endphp
    <img class="main-img-mask" src="{{ asset($bestPhoto->photo) }}" />
    <div class="mask"></div>
    <div class="dot-background"></div>
    <div class="main-img" style="background-image: url({{ asset($bestPhoto->photo) }}); background-position: top center; background-size: cover;"></div>
    <div class="container">
        <div class="breadcrumb-block">
            <div class="title-block">
                <h1>{!! $title !!}</h1>
            </div>
        </div>
    </div>
    <p class="user-img-info">Краще фото тижня<a href="">{{ $bestPhoto->name }}</a></p>
</div>