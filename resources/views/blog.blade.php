@extends('layouts.app')

@section('content')
    <div id="main-app">
        @include('partial.week_photo', ['title' =>  trans('mes.Блог порталу').' "'.trans('mes.Моє Свято').'"'])
        @include('partial.breadcrumb', ['current' => trans('mes.Блог') ])
        @if(Auth::check())
            <div class="news-send-blog">
                <p>
                    {{ trans('mes.Додати свій запис в блог') }}
                </p>

                <a href="#" data-toggle="modal" data-target="#new-news" class="write-news"><svg><rect x="0" y="0" fill="none" width="166" height="45"></rect></svg>
                    {{ trans('mes.Опублікувати новину') }}
                </a>

            </div>
        @endif
        <div class="blog-page">
            <div class="container-fluid">
                <div class="row">
                    @foreach($articles as $article)
                        <?php

                        if (request()->has('category') && $article->categories[0]->id!=request()->category) continue;

                        ?>
                        <div class="news-block" style="background: url({{asset($article->image)}}); background-position: center top; background-repeat: no-repeat; background-size: cover;">
                            <a href="{{ route('blog-post', ['post_id' => $article->id]) }}">
                                <div class="mask"></div>
                                <div class="date-news">
                                    <span class="month">{{ $article->created_at->format('m') }}</span>
                                    <span class="day">{{ $article->created_at->format('d') }}</span>
                                    <span class="year">{{ $article->created_at->format('Y') }}</span>
                                </div>
                                <div class="title-news">
                                    <h3> {{ $article->title }} </h3>
                                </div>
                                <div class="post-info">
                                    <a href="{{ route('user-page', ['user_id' => $article->user_id]) }}" class="label"><i class="fas fa-user"></i> {{ $article->author }}</a>
                                    <a href="#" class="label"><i class="fas fa-comments"></i> {{ $article->comments_cnt }}</a>
                                </div>
                            </a>
                        </div>
                         @endforeach
                         @if (request()->has('category'))

                         @else
                            {{ $articles->links() }}
                         @endif

                </div>
        </div>
    </div>
     @if(Auth::check())
        <div class="modal fade modal-edit-news" id="new-news" tabindex="-1" role="dialog" aria-labelledby="new-news" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ trans('mes.Створення нової публікації') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" enctype="multipart/form-data" action="{{ route('add-article') }}">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::id() }}" required>
                        <div class="modal-body">
                            <div class="media"></div>
                            <label>{{ trans('mes.Заголовок новини') }}
                                <input type="text" name="title" required placeholder="{{ trans('mes.Наприклад: Як правильно обрати ракурс для фото дівчині?') }}">
                            </label>
                            <label>{{ trans('mes.Категорія') }}
                                <select class="form-control" required name="article_category_id">
                                    @foreach($article_categories as $category)
                                        <option value="{{ $category->id }}">
                                        <?php
                                        $lang= 'name_'.Lang::getLocale();
                                        echo $category->{$lang};

                                        ?></option>
                                    @endforeach
                                </select>
                            </label>
                            <label>{{ trans('mes.Фото-прев\'ю новини') }}
                                <input required type="file" id="img-news" name="image">
                                <span class="check-img">{{ trans('mes.Обрати фото') }}</span>
                            </label>
                            <label>{{ trans('mes.Текст новини') }}
                                <textarea required id="article-ckeditor" name="text"></textarea>
                            </label>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="new-news-btn"><svg><rect x="0" y="0" fill="none" width="166" height="45"></rect></svg> {{ trans('mes.Опублікувати') }}</button>
                        </div>
                    </form>
                </div>
                <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
            </div>
        </div>
     @endif
@endsection