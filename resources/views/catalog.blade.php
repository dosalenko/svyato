@extends('layouts.app')

@section('content')

    <?php
    $region_lang = 'region_' . Lang::getLocale();
    $category_lang = 'category_' . Lang::getLocale();
    $city_lang = 'name_' . Lang::getLocale();
    $seo_lang = 'text_' . Lang::getLocale();
    $party_lang = 'name_' . Lang::getLocale();
    ?>

    <div id="main-app">
        @include('partial.week_photo', ['title' => "<span>{$currentCategory}</span> на {$currentPartyCategory} <span>{$currentPlace}</span"])

        <div class="breadcrumb-block">

            <ul class="breadcrumb-list">
                <li class="breadcrumb-item">
                    <a href="{{ route('main') }}" class="breadcrumb-link">{{ trans('mes.Головна') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="" class="breadcrumb-link">{{ trans('mes.Вся Україна') }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="" class="breadcrumb-link">{{ trans('mes.Всі категорії') }}</a>
                </li>
            </ul>
        </div>
        <div class="user-cataloge ">

            <div class="row">
                <div class="filter-block">
                    <form class="body-calc js-search" action="{{ route('catalog') }}" method="get">
                        <h4 class="title-m-calc">{{ trans('mes.Що святкуєте?') }}</h4>
                        <div class="parent-category">




                            <ul>
                                @foreach($partyCategories as $category)
                                    <li>
                                        <input {{ app('request')->input('party_category')==$category->id  ? 'checked' : '' }} type="radio" id="radio{{ $category->id }}" name="party_category"
                                               value="{{ $category->id }}">
                                        <label for="radio{{ $category->id }}">
                                            <span class="check"></span>{{ $category->{$party_lang} }}</label>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                        <div class="category-city">
                            <h4 class="title-m-calc">{{ trans('mes.Категорії виконавців') }}:</h4>
                            <div class="select-calc js-category">
                                <span class="modal-c-icon"><i class="fas fa-user-tag"></i></span>


                                <multiselect v-model="valueCategory" :options="listCategory" :multiple="true" :close-on-select="false"  values="<?=$category_lang?>" placeholder="Оберіть категорію" class="category-dropdown" label="<?=$category_lang?>" track-by="<?=$category_lang?>">
                                    <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
                                </multiselect>




                            </div>
                            <h4 class="title-m-calc">{{ trans('mes.В якому місті святкуєте?') }}</h4>
                            <div class="select-calc js-city">
                                <span class="modal-c-icon"><i class="fas fa-city"></i></span>


                                @if ( app()->getLocale() == 'ua')
                                    <multiselect v-model="valueCity" name="city_ua" :options="listCity" :multiple="true"
                                                 :close-on-select="false" group-values="city" group-label="region_ua"
                                                 :group-select="true" placeholder="{{ trans('mes.Оберіть місто') }}"
                                                 track-by="id" label="name_ua" class="city-dropdown">
                                        <span slot="noResult">{{ trans('mes.Нічого не знайдено') }}</span>
                                    </multiselect>

                                @elseif ( app()->getLocale() == 'ru')
                                    <multiselect v-model="valueCity" name="city_ru" :options="listCity" :multiple="true"
                                                 :close-on-select="false" group-values="city" group-label="region_ru"
                                                 :group-select="true" placeholder="{{ trans('mes.Оберіть місто') }}"
                                                 track-by="id" label="name_ru" class="city-dropdown">
                                        <span slot="noResult">{{ trans('mes.Нічого не знайдено') }}</span>
                                    </multiselect>


                                @endif


                            </div>
                        </div>
                        <h4 class="title-m-calc">{{ trans('mes.Бюджет на святкування') }}:</h4>
                        <div class="price-calc">

                            <div class="track-container">

                                <div class="track" ref="_vpcTrack"></div>
                                <div class="track-highlight" ref="trackHighlight"></div>
                                <span class="track-btn track1" ref="track1"></span>
                                <span class="track-btn track2" ref="track2"></span>
                                <span class="range-value min" data-price="1">@{{ minValue}} </span>
                                <span class="range-value max" data-price="1">@{{ maxValue }}</span>
                            </div>
                        </div>
                        <h4 class="title-m-calc">{{ trans('mes.Рейтинг по відгукам') }}:</h4>
                        <div class="rating-calc">


                            <div class="rating-smile">


                                <input {{ app('request')->input('rating-01')==5 ? 'checked' : '' }}
                                       type="radio" id="star-1" name="rating-01" value="5"
                                       data-description="{{ trans('mes.Чудово') }}"/>
                                <label class="star-1" for="star-1"></label>
                                <input {{ app('request')->input('rating-01')==4 ? 'checked' : '' }} type="radio"
                                       id="star-2" name="rating-01" value="4"
                                       data-description="{{ trans('mes.Добре') }}"/>
                                <label class="star-2" for="star-2"></label>
                                <input {{ app('request')->input('rating-01')==3 ? 'checked' : '' }}type="radio"
                                       id="star-3" name="rating-01" value="3"
                                       data-description="{{ trans('mes.Нормально') }}"/>
                                <label class="star-3" for="star-3"></label>
                                <input {{ app('request')->input('rating-01')==2 ? 'checked' : '' }} type="radio"
                                       id="star-4" name="rating-01" value="2"
                                       data-description="{{ trans('mes.Не сподобалось') }} "/>
                                <label class="star-4" for="star-4"></label>
                                <input {{ app('request')->input('rating-01')==1 ? 'checked' : '' }} type="radio"
                                       id="star-5" name="rating-01" value="1"
                                       data-description="{{ trans('mes.Жахливо') }}"/>
                                <label class="star-5" for="star-5"></label>

                            </div>
                            <div class="raiting-info"><span id="rating">0</span> / <span
                                        id="rating-txt">{{ trans('mes.рейтинг') }}</span></div>
                        </div>

                        <div class="other-calc">
                            <h4 class="title-m-calc">{{ trans('mes.Інші критерії') }}:</h4>
                            <ul>
                                <li>
                                    <input {{ app('request')->input('promocia')=='on' ? 'checked' : '' }} type="checkbox"
                                           id="promocia" name="promocia">
                                    <label for="promocia">
                                        <span> </span>
                                        {{ trans('mes.Акційні пропозиції') }}
                                    </label>
                                </li>
                                <li>
                                    <input {{ app('request')->input('promocia')==1 ? 'checked' : '' }} type="checkbox"
                                           id="online" name="online" value="1">
                                    <label for="online"><span></span>{{ trans('mes.Зараз онлайн') }}</label>
                                </li>
                                <li>
                                    <input {{ app('request')->input('top')==1 ? 'checked' : '' }} type="checkbox"
                                           id="checkbox1" name="top" value="1">
                                    <label for="checkbox1"><span></span>{{ trans('mes.Тільки TOP') }}</label>
                                </li>
                                <li>
                                    <input {{ app('request')->input('pro')==1 ? 'checked' : '' }} type="checkbox"
                                           id="checkbox2" name="pro" value="1">
                                    <label for="checkbox2"><span></span>{{ trans('mes.Тільки PRO') }}</label>
                                </li>
                            </ul>
                        </div>
                        <div class="other-calc">
                            <h4 class="title-m-calc">{{ trans('mes.Сортування') }}:</h4>
                            <div class="sort-user">
                                <ul>
                                    <li>
                                        <input {{ app('request')->input('sort_user')=='new' ? 'checked' : '' }} type="radio"
                                               id="new-user" name="sort_user" value="new">
                                        <label for="new-user">
                                            <span class="check"></span>
                                            {{ trans('mes.Спочатку нові користувачі') }}
                                        </label>

                                    </li>
                                    <li>
                                        <input {{ app('request')->input('sort_user')=='likes'? 'checked' : '' }} type="radio"
                                               id="popular-user" name="sort_user" value="likes">
                                        <label for="popular-user">
                                            <span class="check"></span>
                                            {{ trans('mes.Спочатку популярні користувачі') }}
                                        </label>

                                    </li>
                                    <li>
                                        <input {{ app('request')->input('sort_user')=='cheap' ? 'checked' : '' }} type="radio"
                                               id="cheap-user" name="sort_user" value="cheap">
                                        <label for="cheap-user">
                                            <span class="check"></span>
                                            {{ trans('mes.Спочатку дешеві пропозиції') }}
                                        </label>

                                    </li>
                                    <li>
                                        <input {{ app('request')->input('sort_user')=='expensive' ? 'checked' : '' }} type="radio"
                                               id="expensive-user" name="sort_user" value="expensive">
                                        <label for="expensive-user">
                                            <span class="check"></span>
                                            {{ trans('mes.Спочатку дорогі пропозиції') }}
                                        </label>

                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="send-calculation">
                            <button class="send-btn">
                                <svg>
                                    <rect x="0" y="0" fill="none" width="166" height="45"/>
                                </svg>
                                {{ trans('mes.Фільтрувати') }}
                            </button>
                        </div>
                    </form>
                </div>
                <div class="user-content">
                    @if(Auth::check())
                        <div class="info-user-top">
                            <p>{{ trans('mes.Хочете щоб Ваша анкета була на першій сторінці?') }}
                                <a href="{{ route('ads') }}">
                                    {{ trans('mes.Купити TOP') }}
                                </a></p>
                        </div>
                    @endif
                    @foreach($artists as $artist)
                        @include('partial.artist', ['artist' => $artist, 'main' => false])
                    @endforeach

                    {{ $artists->links() }}
                </div>
            </div>
            <div class="container">
                <div class="performers-block">
                    <h2 class="title">
                        {{ trans('mes.Категорії виконавців на') }}  {{ $currentPartyCategory }}
                    </h2>
                    <ul>
                        @foreach($artistsOnParty as $artistCategory => $onePartyArtist)
                            <li>
                                <a href="{{ route('catalog') . '?category=' . $onePartyArtist->first()->id }}">{{ $artistCategory }}
                                    <span class="count">{{ $onePartyArtist->count() }}</span></a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="city-block">
                    <h2 class="title">
                        {{ $currentCategory }} на  {{ $currentPartyCategory }} {{ $currentPlace }}
                    </h2>
                    <ul>
                        <?php

                        $region_lang = 'region_' . Lang::getLocale();

                        ?>
                        @foreach($artistsByRegion as $regionArtist)
                            <li><a href="/catalog/?region_id={{$regionArtist->id }}&region_name={{$regionArtist->{$region_lang} }}">
                                    <span> <?= $regionArtist->{$region_lang} ?> </span> <span
                                            class="count">{{ $regionArtist->count }}</span></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="container seo-block">
            <h2>{{ $currentCategory }} на {{ $currentPartyCategory }} {{ $currentPlace }}
            </h2>{!! $seo->{$seo_lang} !!}</div>
    </div>
    @include('partial.best_photo');
@endsection
