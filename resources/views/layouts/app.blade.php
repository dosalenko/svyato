<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <meta name="robots" content="noindex, nofollow"/>


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{trans('mes.МОЄ СВЯТО')}}</title>

    <!-- Scripts -->
    <script  defer src="{{ asset('js/jquery-2.1.1.js') }}"></script>
    <script  defer src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script  defer src="{{ asset('js/typed.min.js') }}"></script>
    <script  defer src="{{ asset('vue/js/vue.js') }}"></script>
    <script  defer src="{{ asset('vue/js/vue-select.js') }}"></script>
    <script  defer src="{{ asset('js/slick.js') }}"></script>
    <script  defer src="{{ asset('js/vue.js') }}"></script>
    <script  defer src="{{ asset('js/main.js') }}"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js"></script>
    {{--<script defer src="https://w.soundcloud.com/player/api.js"></script>--}}

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Capriola|Roboto">

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vue/css/vue.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slider.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/gallery.css') }}">

    <style>
        header.active,
        .additionally-category.active {
            display: none !important;
        }

        fieldset.active,
        .search-cities-block.active,
        .s-btn.active {
            opacity: .3;
            pointer-events: none;
        }

        .main-window.home-window .header-center.opacity {
            opacity: 0;
        }

        .toggle-s-calculation.active,
        .toggle-s-cities.active {
            pointer-events: none;
        }

        .mask.active {
            background: rgba(0, 0, 0, .8) !important;
        }

    </style>
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/basic.min.css">--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">--}}
    {{--<script  defer src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone-amd-module.min.js"></script>--}}
    {{--<script  defer src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>--}}


</head>
<body>
@include('partial.header')
@yield('content')
@include('partial.footer')

</body>
</html>
