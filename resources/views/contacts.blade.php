@extends('layouts.app')

@section('content')
    <div id="main-app">
        @include('partial.week_photo', ['title' => trans('mes.Контактна інформація')])
        @include('partial.breadcrumb', ['current' =>trans('mes.Контакти')])
        <div class="contact-page container">
            <div class="row">
                <div class="contact-info">
                    <h4>{{trans('mes.Слідкуйте за нами')}}:</h4>
                    <ul class="soc-link">
                        <li><a href="{{ $facebook }}"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="{{ $instagram }}"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="{{ $youtube }}"><i class="fab fa-youtube"></i></a></li>

                    </ul>
                    <h4>{{trans('mes.Виникли питання?')}}</h4>
                    <ul>
                        <li><a href=""><i class="fas fa-phone"></i> {{ $phone }}</a></li>
                        <li><a href=""><i class="fas fa-paper-plane"></i> {{ $email }}</a></li>
                        <li><a href=""><i class="fab fa-viber"></i> {{ $viber }}</a></li>
                        <li><a href=""><i class="fab fa-telegram"></i> {{ $telegramm }}</a></li>
                    </ul>
                </div>
                <div class="contact-form">
                    <form method="post" action="{{ route('feedback') }}">
                        @csrf
                        <div class="group-input">
                            <label>
                                <input name="feedback_name" type="text" required placeholder="{{trans('mes.Як до Вас звертатись?')}}">
                            </label>
                        </div>
                        <div class="group-input">
                            <label>
                                <input name="feedback_addres" type="text" required placeholder="{{trans('mes.Куди можна вам написати?')}}">
                            </label>
                        </div>
                        <div class="group-input">
                            <label>
                                <input name="feedback_phone" type="text" placeholder="{{trans('mes.Номер телефону для зв\'язку з Вами?')}}">
                            </label>
                        </div>
                        <div class="group-input">
                            <label>
                                <textarea name="feedback_text" rows="5" required placeholder="{{trans('mes.Опишіть,що Ви хотіли б дізнатись')}}"></textarea>
                            </label>
                        </div>
                        <div class="send-mail">
                            <button class="send-btn">
                                <svg>
                                    <rect x="0" y="0" fill="none" width="166" height="45" />
                                </svg>
                                {{trans('mes.Відправити')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection