@extends('layouts.app')

@section('content')

    <?php

    use App\User;
    use App\Artist;
    use App\Message;


    $region_lang = 'region_' . Lang::getLocale();
    $category_lang = 'category_' . Lang::getLocale();
    $city_lang = 'name_' . Lang::getLocale();
    $description_lang = 'description' . Lang::getLocale();
    $name_lang = 'name_' . Lang::getLocale();
    $party_lang = 'name_' . Lang::getLocale();

    ?>

    <div id="main-app">
        <div class="user-page">
            <div class="main-window">
                <img class="main-img-mask" src="{{ $artist->user->background_image }}"/>
                <div class="mask"></div>
                <div class="dot-background"></div>
                <div class="main-img"
                     style="background-image: url({{ $artist->user->background_image }}); background-position: top center; background-size: cover;"></div>
                <div class="container">
                    <div class="user-detail">
                        <div class="img-block">
                            <div class="img-item">


                                <img class="user-img" src="{{ $artist->user->image ?:'/img/bg/home.jpg' }}">

                            </div>
                            <div class="user-link">
                                <div class="site-user">
                                    <a href=""><i class="fas fa-globe-americas"></i></a>
                                </div>
                                <ul class="social-user pro-1">
                                    <li class="title-link">{{ trans('mes.Слідкуйте за мною') }}:</li>
                                    <li><a href="{{ $artist->instagram }}"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="{{ $artist->facebook }}"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="{{ $artist->youtube }}"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="{{ $artist->vimeo }}"><i class="fab fa-vimeo-v"></i></a></li>
                                </ul>
                                <ul class="messenger-user pro-2">
                                    <li class="title-link">Пишіть мені</li>
                                    <li><a href="{{ $artist->viber }}"><i class="fab fa-viber"></i></a></li>
                                    <li><a href="{{ $artist->facebook_messenger }}"><i
                                                    class="fab fa-facebook-messenger"></i></a></li>
                                    <li><a href="{{ $artist->telegramm }}"><i class="fab fa-telegram"></i></a></li>
                                    <li><a href="{{ $artist->whatsapp }}"><i class="fab fa-whatsapp"></i></a></li>
                                </ul>
                            </div>
                            <div class="btn-group">

                                <a href="#" data-toggle="modal" data-target="#new_message">
                                    {{ trans('mes.Напишіть мені') }}
                                </a>
                                <a href="#" class="pro-1">Зателефонувати</a>
                            </div>
                        </div>
                        <div class="description-block">
                            <div class="name-block">
                                <h1 class="name-user">
                                    {{ $artist->{User::getNameByLang($artist->id, Lang::getLocale())}  }}
                                    </h1>
                                <div class="label-price">
                                    <span class="price">{{ $artist->price }} грн </span>
                                </div>
                                <form class="like-add"><input id="user1-heart" type="checkbox"><label for="user1-heart"
                                                                                                      aria-label="like">❤
                                        <span class="number">{{ $artist->likes_cnt }}</span>
                                    </label></form>
                            </div>
                            <div class="list-info">
                                <ul class="detail-user">
                                    <li class="detail-user-item">
                                        <i class="fas fa-phone-volume"></i>
                                        <a href="">{{ $artist->phone }}</a>
                                    </li>
                                    <li class="detail-user-item">
                                        <i class="fas fa-paper-plane"></i>
                                        <a href="">{{ $artist->email }}</a>
                                    </li>
                                    <li class="detail-user-item">
                                        <i class="fas fa-user-tag"></i>
                                        @php
                                            $single_cat= \App\Category::where('id', $artist->category_id )->first();
                                        @endphp

                                        @if ($artist->category_id>0)
                                            <a href="{{ route('catalog') . '?category=' . $single_cat->{$category_lang}  }}">{{ $single_cat->{$category_lang} }}</a>
                                        @endif

                                    </li>
                                    <li class="detail-user-item">
                                        <i class="fas fa-map-marker-alt"></i>
                                        @php
                                            $single_region= \App\Region::where('id', $artist->region_id )->first();
                                        @endphp

                                        @if ($artist->region_id>0)
                                            <a href="">{{ $single_region->{$region_lang} }}</a>
                                        @endif

                                    </li>
                                </ul>

                            </div>
                            <div class="text-user">
                                <p>{{ $artist->{$description_lang} }}</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="user-tab">
                    <ul class="nav nav-tabs user-tab-content" role="tablist" id="user-tab-content">
                        <li class="nav-item">
                            <a class="nav-link active" id="photo-tab" data-toggle="tab" href="#photo" role="tab"
                               aria-controls="photo" aria-selected="true">{{ trans('mes.Фото') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="video-tab" data-toggle="tab" href="#video" role="tab"
                               aria-controls="video" aria-selected="false">{{ trans('mes.Відео') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="video-tab" data-toggle="tab" href="#music" role="tab"
                               aria-controls="music" aria-selected="false">{{ trans('mes.Аудіо') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab"
                               aria-controls="reviews" aria-selected="false">{{ trans('mes.Відгуки') }}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="photo" role="tabpanel" aria-labelledby="photo-tab">
                            <div class="gallery-show">
                                <div class="gallery-container">
                                    @foreach($photos as $photo)


                                        <div class="gallery-item">


                                            <a data-fancybox="group-photo" class="lightbox fancybox"
                                               href="/upload/gallery/{{ $photo->photo }}" data-lightbox="roadtrip">


                                                <img class="img-fluid" src="/upload/gallery/sm_{{ $photo->photo }}">
                                            </a>

                                            <form class="like-photo"><input id="photo-{{$photo->id}}"
                                                                            type="checkbox"><label
                                                        for="photo-{{ $photo->id }}" aria-label="like">❤
                                                    <span class="number">{{ $photo->likes }}</span>
                                                </label>
                                            </form>
                                            <br>


                                        </div>



                                    @endforeach
                                </div>
                                {{ $photos->fragment('photo')->links() }}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
                            <div class="gallery-show video-gallery">
                                <div class="gallery-container">
                                    @foreach($videos as $video)
                                        <div class="gallery-item">
                                            <a data-fancybox="group-video" class="lightbox iframe"
                                               href="{{ $video->video }}">
                                                @php $videoId = \explode('/', $video->video) @endphp
                                                @if(\mb_strpos($video->video, 'youtu') !== false)
                                                    <img class="img-fluid"
                                                         src="//img.youtube.com/vi/{{ \end($videoId) }}/sddefault.jpg">
                                                @elseif(\mb_strpos($video->video, 'vimeo') !== false)
                                                    <img class="img-fluid"
                                                         src="https://i.vimeocdn.com/video/{{ \end($videoId) }}_640x480.jpg">
                                                @endif
                                            </a>
                                            <form class="like-photo"><input id="video-{{ $video->id }}" type="checkbox"><label
                                                        for="video-{{ $video->id }}" aria-label="like">❤
                                                    <span class="number">{{ $video->likes }}</span>
                                                </label>
                                            </form>
                                        </div>
                                    @endforeach
                                </div>
                                {{ $videos->fragment('video')->links() }}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="music" role="tabpanel" aria-labelledby="music-tab">


                            @foreach($audio as $au)
                                <iframe id="audio-user" scrolling="no" frameborder="no"
                                        src="https://w.soundcloud.com/player/?url={{ $au->audio }}&color=%23ff0000&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>

                            @endforeach


                        </div>
                        <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                            @if(Auth::check())
                                @if(Auth::id() !== $artist->user_id)
                                    <form method="post" action="{{ route('add-review') }}">
                                        @csrf
                                        <input type="hidden" name="artist_id" value="{{ $artist->id }}">
                                        <input type="hidden" name="from_user_id" value="{{ Auth::id() }}">
                                        <input type="hidden" name="user_id" value="{{ $artist->user_id }}">
                                        <h3>Залишити відгук для <span>{{ $artist->name }}</span>:</h3>
                                        <div class="row">
                                            <div class="add-reviews">
                                                <textarea type="text" name="text"
                                                          placeholder="{{ trans('mes.Напишіть відгук для') }} “{{ $artist->name }}”"
                                                          rows="4"></textarea>
                                                <button class="send-rev">
                                                    <svg>
                                                        <rect x="0" y="0" fill="none" width="170" height="45"></rect>
                                                    </svg>
                                                    Опублікувати
                                                </button>
                                            </div>
                                            <div class="star-block">
                                                <div class="rating-smile">
                                                    <span>Ваша оцінка:</span>
                                                    <input type="radio" id="star-1" name="rating-01" value="5"
                                                           data-description="Замечательно"> <label for="star-1"
                                                                                                   class="star-1"></label>
                                                    <input type="radio" id="star-2" name="rating-01" value="4"
                                                           data-description="Хорошо"> <label for="star-2"
                                                                                             class="star-2"></label>
                                                    <input type="radio" id="star-3" name="rating-01" value="3"
                                                           data-description="Нормально"> <label for="star-3"
                                                                                                class="star-3"></label>
                                                    <input type="radio" id="star-4" name="rating-01" value="2"
                                                           data-description="Не понравилось"> <label for="star-4"
                                                                                                     class="star-4"></label>
                                                    <input type="radio" id="star-5" name="rating-01" value="1"
                                                           data-description="Ужасно"> <label for="star-5"
                                                                                             class="star-5"></label>
                                                </div>
                                                <div class="rating-info"><span id="rating">0</span> / <span
                                                            id="rating-txt">рейтинг</span></div>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            @else
                                <p class="not-authorized">Щоб залишити відгук <span>
                                        {{--для User Name--}}
                                    </span> будь ласка <a
                                            href="#" data-toggle="modal" data-target="#authorization-modal">
                                        зареєструйтесь / увійдіть</a> в систему</p>
                            @endif
                            <div class="rev-block">
                                @foreach($reviews as $review)
                                    <div class="rev-item">
                                        <div class="rev-user">
                                            <img src="{{ $review->user->image }}">
                                            <h4>{{ $review->user->email }}</h4>
                                            <p><span class="count-rev">1 </span> відгук</p>

                                        </div>
                                        <div class="rev-description">
                                            <span class="count-rating">{{ $review->likes }}.0</span>
                                            <span class="date-rev">{{ $review->updated_at->format('d.m.Y') }}</span>
                                            <p>{{ $review->text }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            {{ $reviews->fragment('reviews')->links() }}
                        </div>
                    </div>
                </div>
                <div class="other-user">

                    @if ($artist->region_id>0)


                        <h2 class="title">
                            Інші {{ \Stringy\Stringy::create($artist->category->category)->toLowerCase() }}

                            в {{ $single_region->{$region_lang}  }}</h2>

                    @endif
                    <div class="other-user">
                        @foreach($artists as $oneArtist)
                            <div class="user-item">
                                <a href="{{ route('user-page', ['user_id' => $oneArtist->user_id]) }}">
                                    <img src="{{ $oneArtist->image }}">
                                </a>
                                <h5 class="user-name"><a
                                            href="{{ route('user-page', ['user_id' => $oneArtist->user_id]) }}">{{ $oneArtist->name }}</a> @if($oneArtist->is_pro == 1)
                                        <span>pro</span>@endif</h5>
                                <ul class="user-list-city"><i class="fas fa-map-marker-alt"></i>
                                    @php $cities = $artistCities->where('artist_id', $oneArtist->id) @endphp
                                    @foreach($cities as $city)
                                        <li>
                                            <a href="{{ route('catalog') . '?cities=' . $city->city_id }}">{{ $city->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <ul class="user-list-category"><i class="fas fa-user-tag"></i>
                                    <li>
                                        <a href="{{ route('catalog') . '?category=' . $oneArtist->category_id }}"> {{ $oneArtist->category }}</a>
                                    </li>
                                </ul>
                                <div class="info-network">
                                    @if($active->search($oneArtist->user_id) !== false)
                                        <span class="online">online</span>
                                    @else
                                        <span class="offline">offline </span>
                                    @endif
                                </div>
                                <div class="price-block"><i class="far fa-money-bill-alt"></i><span class="old-price">{{$oneArtist->old_price}}
                                        грн.</span> <span class="new-price">{{ $oneArtist->price }} грн.</span></div>
                                <form class="like-add"><input id="user5-heart" type="checkbox"><label for="user5-heart"
                                                                                                      aria-label="like">❤
                                        <span class="number">{{ $oneArtist->likes_cnt }}</span>
                                    </label></form>
                            </div>
                        @endforeach
                    </div>
                </div>

                @if ($artist->category_id>0)
                    <div class="city-block">
                        <h2 class="title">
                            {{ $artist->category->{$category_lang} }} на {{ $partyCategory->{$party_lang} }} у Вашому регіоні
                        </h2>
                        <ul>
                            @foreach($regions as $region)
                                <li>
                                    <a href="/catalog/?categoryId={{ $artist->category->id }}&region_id={{ $region->id }}&region_name={{ $region->{$region_lang} }}">{{ $region->{$region_lang} }}
                                        <span class="count">{{ $region->count }}</span></a>


                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif


            </div>
        </div>
    </div>



    @if(Auth::check())

        <div class="modal fade modal-edit-news" id="new_message" tabindex="-1" role="dialog" aria-labelledby="new-news"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ trans('mes.Нове повідомлення') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" enctype="multipart/form-data" action="/message/create">
                        @csrf
                        <input type="hidden" name="to_id" value="{{  $artist->user_id  }}" required>
                        <div class="modal-body">

                            <label>{{ trans('mes.Ваше повідомлення') }}
                                <input type="text" name="text" required>

                            </label>


                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="new-news-btn">
                                <svg>
                                    <rect x="0" y="0" fill="none" width="166" height="45"></rect>
                                </svg> {{ trans('mes.Відравити') }}</button>
                        </div>
                    </form>
                </div>


            </div>
        </div>

    @endif

@endsection