<div class="form-group {{ $errors->has('html_ua') ? 'has-error' : ''}}">
    <label for="html_uat" class="control-label">{{ 'Html Ua' }}</label>
    <textarea class="form-control" rows="5" name="html_ua" type="textarea" id="html_ua" >{{ isset($about->html_ua) ? $about->html_ua : ''}}</textarea>
    {!! $errors->first('html_ua', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('html_ru') ? 'has-error' : ''}}">
    <label for="html_ru" class="control-label">{{ 'Html Ru' }}</label>
    <textarea class="form-control" rows="5" name="html_ru" type="textarea" id="html_ru" >{{ isset($about->html_ru) ? $about->html_ru : ''}}</textarea>
    {!! $errors->first('html_ru', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Зберегти' : 'Create' }}">
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'html_ua' );
    CKEDITOR.replace( 'html_ru' );
</script>
