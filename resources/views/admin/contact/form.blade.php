<div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
    <label for="facebook" class="control-label">{{ 'Facebook' }}</label>
    <input class="form-control" name="facebook" type="text" id="facebook" value="{{ isset($contact->facebook) ? $contact->facebook : ''}}" >
    {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('instagram') ? 'has-error' : ''}}">
    <label for="instagram" class="control-label">{{ 'Instagram' }}</label>
    <input class="form-control" name="instagram" type="text" id="instagram" value="{{ isset($contact->instagram) ? $contact->instagram : ''}}" >
    {!! $errors->first('instagram', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('youtube') ? 'has-error' : ''}}">
    <label for="youtube" class="control-label">{{ 'Youtube' }}</label>
    <input class="form-control" name="youtube" type="text" id="youtube" value="{{ isset($contact->youtube) ? $contact->youtube : ''}}" >
    {!! $errors->first('youtube', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'Phone' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ isset($contact->phone) ? $contact->phone : ''}}" >
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($contact->email) ? $contact->email : ''}}" >
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('viber') ? 'has-error' : ''}}">
    <label for="viber" class="control-label">{{ 'Viber' }}</label>
    <input class="form-control" name="viber" type="text" id="viber" value="{{ isset($contact->viber) ? $contact->viber : ''}}" >
    {!! $errors->first('viber', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telegramm') ? 'has-error' : ''}}">
    <label for="telegramm" class="control-label">{{ 'Telegramm' }}</label>
    <input class="form-control" name="telegramm" type="text" id="telegramm" value="{{ isset($contact->telegramm) ? $contact->telegramm : ''}}" >
    {!! $errors->first('telegramm', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Зберегти' : 'Create' }}">
</div>
