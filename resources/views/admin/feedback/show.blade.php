@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Feedback {{ $feedback->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/feedback') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/feedback/' . $feedback->id . '/edit') }}" title="Edit Feedback"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/feedback' . '/' . $feedback->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Feedback" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $feedback->id }}</td>
                                    </tr>
                                    <tr><th> Feedback Name </th><td> {{ $feedback->feedback_name }} </td></tr><tr><th> Feedback Addres </th><td> {{ $feedback->feedback_addres }} </td></tr><tr><th> Feedback Phone </th><td> {{ $feedback->feedback_phone }} </td></tr><tr><th> Feedback Text </th><td> {{ $feedback->feedback_text }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
