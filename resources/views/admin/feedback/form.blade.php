<div class="form-group {{ $errors->has('feedback_name') ? 'has-error' : ''}}">
    <label for="feedback_name" class="control-label">{{ 'Feedback Name' }}</label>
    <input class="form-control" name="feedback_name" type="text" id="feedback_name" value="{{ isset($feedback->feedback_name) ? $feedback->feedback_name : ''}}" >
    {!! $errors->first('feedback_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('feedback_addres') ? 'has-error' : ''}}">
    <label for="feedback_addres" class="control-label">{{ 'Feedback Addres' }}</label>
    <input class="form-control" name="feedback_addres" type="text" id="feedback_addres" value="{{ isset($feedback->feedback_addres) ? $feedback->feedback_addres : ''}}" >
    {!! $errors->first('feedback_addres', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('feedback_phone') ? 'has-error' : ''}}">
    <label for="feedback_phone" class="control-label">{{ 'Feedback Phone' }}</label>
    <input class="form-control" name="feedback_phone" type="text" id="feedback_phone" value="{{ isset($feedback->feedback_phone) ? $feedback->feedback_phone : ''}}" >
    {!! $errors->first('feedback_phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('feedback_text') ? 'has-error' : ''}}">
    <label for="feedback_text" class="control-label">{{ 'Feedback Text' }}</label>
    <textarea class="form-control" rows="5" name="feedback_text" type="textarea" id="feedback_text" >{{ isset($feedback->feedback_text) ? $feedback->feedback_text : ''}}</textarea>
    {!! $errors->first('feedback_text', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
