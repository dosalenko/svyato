@extends('adminlte::page')

@section('title', 'Звернення з сайту')

@section('content_header')
    <h1>Звернення з сайту</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">


            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">


                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ім'я</th>
                                    <th>Куди відповісти?</th>
                                    <th>Номер телефону</th>
                                    <th>Текст</th>
                                    <th>Дата написання</th>
                                    <th>Дії</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($feedback as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->feedback_name }}</td>
                                        <td>{{ $item->feedback_addres }}</td>
                                        <td>{{ $item->feedback_phone }}</td>
                                        <td>{{ $item->feedback_text }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>


                                            <form method="POST" action="{{ url('/admin/feedback' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm"
                                                        title="Delete Feedback"
                                                        onclick="return confirm(&quot;Підтвердити видалення&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i> Видалити
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $feedback->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
