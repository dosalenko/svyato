<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">Ім'я</label>
    <input class="form-control" name="name" type="text" id="name"
           value="{{ isset($artist->name) ? $artist->name : ''}}">
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">Про себе</label>
    <input class="form-control" name="description" type="text" id="description"
           value="{{ isset($artist->description) ? $artist->description : ''}}">
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('instagram') ? 'has-error' : ''}}">
    <label for="instagram" class="control-label">{{ 'Instagram' }}</label>
    <input class="form-control" name="instagram" type="text" id="instagram"
           value="{{ isset($artist->instagram) ? $artist->instagram : ''}}">
    {!! $errors->first('instagram', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
    <label for="facebook" class="control-label">{{ 'Facebook' }}</label>
    <input class="form-control" name="facebook" type="text" id="facebook"
           value="{{ isset($artist->facebook) ? $artist->facebook : ''}}">
    {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('youtube') ? 'has-error' : ''}}">
    <label for="youtube" class="control-label">{{ 'Youtube' }}</label>
    <input class="form-control" name="youtube" type="text" id="youtube"
           value="{{ isset($artist->youtube) ? $artist->youtube : ''}}">
    {!! $errors->first('youtube', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('vimeo') ? 'has-error' : ''}}">
    <label for="vimeo" class="control-label">{{ 'Vimeo' }}</label>
    <input class="form-control" name="vimeo" type="text" id="vimeo"
           value="{{ isset($artist->vimeo) ? $artist->vimeo : ''}}">
    {!! $errors->first('vimeo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('viber') ? 'has-error' : ''}}">
    <label for="viber" class="control-label">{{ 'Viber' }}</label>
    <input class="form-control" name="viber" type="text" id="viber"
           value="{{ isset($artist->viber) ? $artist->viber : ''}}">
    {!! $errors->first('viber', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('telegramm') ? 'has-error' : ''}}">
    <label for="telegramm" class="control-label">{{ 'Telegramm' }}</label>
    <input class="form-control" name="telegramm" type="text" id="telegramm"
           value="{{ isset($artist->telegramm) ? $artist->telegramm : ''}}">
    {!! $errors->first('telegramm', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email"
           value="{{ isset($artist->email) ? $artist->email : ''}}">
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">Телефон</label>
    <input class="form-control" name="phone" type="text" id="phone"
           value="{{ isset($artist->phone) ? $artist->phone : ''}}">
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    <label for="price" class="control-label">Ціна послуг</label>
    <input class="form-control" name="price" type="text" id="price"
           value="{{ isset($artist->price) ? $artist->price : ''}}">
    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('likes_cnt') ? 'has-error' : ''}}">
    <label for="likes_cnt" class="control-label">Кількість лайків</label>
    <input class="form-control" name="likes_cnt" type="text" id="likes_cnt"
           value="{{ isset($artist->likes_cnt) ? $artist->likes_cnt : ''}}">
    {!! $errors->first('likes_cnt', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('show_on_main') ? 'has-error' : ''}}">
    <label for="show_on_main" class="control-label">Показувати на головній (1/0)</label>
    <input class="form-control" name="show_on_main" type="text" id="show_on_main"
           value="{{ isset($artist->show_on_main) ? $artist->show_on_main : ''}}">
    {!! $errors->first('show_on_main', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
