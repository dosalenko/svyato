@extends('adminlte::page')

@section('title', 'Виконавці')

@section('content_header')
    <h1>Виконавці</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">

                        <a href="{{ url('/admin/artist') }}" title="назад">
                            <button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/artist/' . $artist->id . '/edit') }}" title="Edit Artist">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Редагувати
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/artist' . '/' . $artist->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Видалити"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Видалити
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $artist->id }}</td>
                                </tr>
                                <tr>
                                    <th> Ім'я</th>
                                    <td> {{ $artist->name }} </td>
                                </tr>
                                <tr>
                                    <th> Про себе</th>
                                    <td> {{ $artist->description }} </td>
                                </tr>
                                <tr>
                                    <th> Instagram</th>
                                    <td> {{ $artist->instagram }} </td>
                                </tr>
                                <tr>
                                    <th> Facebook</th>
                                    <td> {{ $artist->facebook }} </td>
                                </tr>
                                <tr>
                                    <th> Youtube</th>
                                    <td> {{ $artist->youtube }} </td>
                                </tr>
                                <tr>
                                    <th> Vimeo</th>
                                    <td> {{ $artist->vimeo }} </td>
                                </tr>
                                <tr>
                                    <th> Viber</th>
                                    <td> {{ $artist->viber }} </td>
                                </tr>

                                <tr>
                                    <th> Telegramm</th>
                                    <td> {{ $artist->telegramm }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $artist->email }} </td>
                                </tr>
                                <tr>
                                    <th> Телефон</th>
                                    <td> {{ $artist->phone }} </td>
                                </tr>


                                <tr>
                                    <th> Область</th>
                                    <td> {{ $artist->region->region_ua }} </td>
                                </tr>
                                <tr>
                                    <th> Категорія</th>
                                    <td> {{ $artist->category->category_ua }} </td>
                                </tr>
                                <tr>
                                    <th> Ціна послуг</th>
                                    <td> {{ $artist->price }} </td>
                                </tr>
                                <tr>
                                    <th> Кількість лайків</th>
                                    <td> {{ $artist->likes_cnt }} </td>
                                </tr>
                                <tr>
                                    <th> Показується на головній? </th>
                                    <td>  @if ($artist->show_on_main==1)  Так @else Ні @endif </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
