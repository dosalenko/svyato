@extends('adminlte::page')

@section('title', 'Виконавці')

@section('content_header')
    <h1>Виконавці</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">

                    <div class="card-body">



                        <form method="GET" action="{{ url('/admin/artist') }}" accept-charset="UTF-8"
                              class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Пошук..."
                                       value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Дії</th>
                                    <th>#</th>
                                    <th>Ім'я</th>
                                    <th>Про себе</th>

                                    <th>Область</th>
                                    <th>Категорія</th>
                                    <th>Ціна послуг</th>
                                    <th>Кількість лайків</th>
                                    {{--<th>Показується на головній?</th>--}}

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($artist as $item)
                                    <tr>
                                        <td>
                                            <a href="{{ url('/admin/artist/' . $item->id) }}" title="View Artist">
                                                <button class="btn btn-info btn-sm"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i> Повний профіль
                                                </button>
                                            </a>
                                            <a href="{{ url('/admin/artist/' . $item->id . '/edit') }}"
                                               title="Редагувати">
                                                <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i> Редагувати
                                                </button>
                                            </a>

                                            <form method="POST" action="{{ url('/admin/artist' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm"
                                                        title="Видалити"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i> Видалити
                                                </button>
                                            </form>

                                        </td>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ mb_substr($item->description,0,100) }}...</td>


                                        <td>{{ $item->region->region_ua }}</td>
                                        <td>{{ $item->category->category_ua }}</td>
                                        <td>{{ $item->price }}</td>
                                        <td>{{ $item->likes_cnt }}</td>
                                        {{-- $item->show_on_main --}}

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $artist->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
