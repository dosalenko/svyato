<div class="form-group {{ $errors->has('text_ua') ? 'has-error' : ''}}">
    <label for="text_uat" class="control-label">{{ 'text Ua' }}</label>
    <textarea class="form-control" rows="5" name="text_ua" type="textarea" id="text_ua" >{{ isset($seo->text_ua) ? $seo->text_ua : ''}}</textarea>
    {!! $errors->first('text_ua', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('text_ru') ? 'has-error' : ''}}">
    <label for="text_ru" class="control-label">{{ 'text Ru' }}</label>
    <textarea class="form-control" rows="5" name="text_ru" type="textarea" id="text_ru" >{{ isset($seo->text_ru) ? $seo->text_ru : ''}}</textarea>
    {!! $errors->first('text_ru', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Зберегти' : 'Create' }}">
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'text_ua' );
    CKEDITOR.replace( 'text_ru' );
</script>
