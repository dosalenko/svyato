{{$region->region_ua}}

<div class="form-group {{ $errors->has('img_region') ? 'has-error' : ''}}">
    <label for="img_region" class="control-label">Зображення </label>
    <input class="form-control" name="img_region" type="file" id="img_region" value="{{ isset($region->img_region) ? $region->img_region : ''}}" >
    {!! $errors->first('img_region', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Зберегти' : 'Зберегти' }}">
</div>
