@extends('adminlte::page')

@section('title', 'Категорії')

@section('content_header')
    <h1>Регіони</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Область</th>
                                    <th>Зображення</th>
                                    <th>Редагувати</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($region as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->region_ua }}</td>
                                        <td><img width="200" src="{{ $item->img_region }}"></td>
                                        <td>

                                            <a href="{{ url('/admin/region/' . $item->id . '/edit') }}"
                                               title="Edit Region">
                                                <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i> Змінити зображення
                                                </button>
                                            </a>


                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $region->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop