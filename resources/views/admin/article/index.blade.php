@extends('adminlte::page')

@section('title', 'Блог')

@section('content_header')
    <h1>Блог</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">


            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body">
                        {{--<a href="{{ url('/admin/article/create') }}" class="btn btn-success btn-sm"--}}
                           {{--title="Add New Article">--}}
                            {{--<i class="fa fa-plus" aria-hidden="true"></i>--}}
                            {{--Додати запис--}}
                        {{--</a>--}}

                        <form method="GET" action="{{ url('/admin/article') }}" accept-charset="UTF-8"
                              class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Пошук..."
                                       value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Заголовок</th>
                                    <th>Категорія</th>
                                    <th>Автор</th>
                                    <th>Текст</th>
                                    <th>Зображення</th>
                                    <th>Дата створення</th>
                                    <th>Дії


                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($article as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}




                                        </td>
                                        <td>{{ $item->title }}</td>
                                        <td>
                                            @foreach($item->categories as $category)
                                                {{  $category->name_ua }}
                                            @endforeach
                                        </td>
                                        <td>{{ $item->author }}</td>
                                        <td>{{ mb_substr(strip_tags($item->text),0,100) }}...</td>

                                        <td><img width="100" src="{{ $item->image }}"></td>
                                        <td>{{$item->created_at}}</td>
                                        <td>

                                            <a href="{{ url('/admin/article/' . $item->id . '/edit') }}"
                                               title="Edit Article">
                                                <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i> Редагування
                                                </button>
                                            </a>

                                            <form method="POST" action="{{ url('/admin/article' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm"
                                                        title="Delete Article"
                                                        onclick="return confirm(&quot;Підтвердити видалення?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i> Видалити
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $article->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
