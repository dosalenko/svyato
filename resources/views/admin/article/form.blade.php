<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Заголовок' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($article->title) ? $article->title : ''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('author') ? 'has-error' : ''}}">
    <label for="author" class="control-label">{{ 'Автор' }}</label>
    <input class="form-control" name="author" type="text" id="author" value="{{ isset($article->author) ? $article->author : ''}}" >
    {!! $errors->first('author', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('text') ? 'has-error' : ''}}">
    <label for="text" class="control-label">{{ 'Текст' }}</label>
    <textarea class="form-control" rows="5" name="text" type="textarea" id="article-ckeditor" >{{ isset($article->text) ? $article->text : ''}}</textarea>

    {!! $errors->first('text', '<p class="help-block">:message</p>') !!}


</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Зображення' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($article->image) ? $article->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Зберегти' : 'Create' }}">
</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'article-ckeditor' );
</script>

