@extends('adminlte::page')

@section('title', 'Категорії')

@section('content_header')
    <h1>Категорії</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">


            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ url('/admin/category/create') }}" class="btn btn-success btn-sm"
                           title="Додати нову категорію">
                            <i class="fa fa-plus" aria-hidden="true"></i> Додати нову категорію
                        </a>


                        <br/>
                        <br/>
                        <div class=" table-responsive">
                            <table class="table table-hover  table-striped">
                                <thead>
                                <tr>
                                    <th>Дії</th>
                                    <th>#</th>

                                    <th>Назва (укр)</th>
                                    <th>Назва (рус)</th>

                                    <th>Зображення</th>
                                    <th>Icon</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($category as $item)
                                    <tr>
                                        <td>
                                            <a href="{{ url('/admin/category/' . $item->id . '/edit') }}"
                                               title="Edit Category">
                                                <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i>
                                                    Редагувати
                                                </button>
                                            </a>
                                            <br>
                                            <form method="POST" action="{{ url('/admin/category' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-default btn-sm"
                                                        title="Delete Category"
                                                        onclick="return confirm(&quot;Підтвердити видалення&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i> Видалити
                                                </button>
                                            </form>
                                        </td>

                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->category_ua }}</td>
                                        <td>{{ $item->category_ru }}</td>
                                        <td><img width="60" src="{{ $item->img_category }}"></td>
                                        <td><img width="60" src="{{ $item->icon_category }}"></td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $category->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


