
<div class="form-group {{ $errors->has('img_category') ? 'has-error' : ''}}">
    <label for="img_category" class="control-label">Зображення</label>
    <input class="form-control" name="img_category" type="file" id="img_category" value="{{ isset($category->img_category) ? $category->img_category : ''}}" >
    {!! $errors->first('img_category', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('icon_category') ? 'has-error' : ''}}">
    <label for="icon_category" class="control-label">Icon</label>
    <input class="form-control" name="icon_category" type="file" id="icon_category" value="{{ isset($category->icon_category) ? $category->icon_category : ''}}" >
    {!! $errors->first('icon_category', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('category_ua') ? 'has-error' : ''}}">
    <label for="category_ua" class="control-label">Назва (укр) </label>
    <input class="form-control" name="category_ua" type="text" id="category_ua" value="{{ isset($category->category_ua) ? $category->category_ua : ''}}" >
    {!! $errors->first('category_ua', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('category_ru') ? 'has-error' : ''}}">
    <label for="category_ru" class="control-label">Назва (рус) </label>
    <input class="form-control" name="category_ru" type="text" id="category_ru" value="{{ isset($category->category_ru) ? $category->category_ru : ''}}" >
    {!! $errors->first('category_ru', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Зберегти' : 'Додати' }}">
</div>
