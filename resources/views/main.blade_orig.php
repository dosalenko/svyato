@extends('layouts.app')

@section('content')
    <div id="main-app" >
        @inject('artistPhotos', 'App\ArtistPhoto')
        @php $bestPhoto = $artistPhotos->bestWeekPhoto()->first() @endphp
        {{--<img class="main-img-mask" src="{{ asset($bestPhoto->photo) }}" />--}}
        <div class="main-window home-window">
            <img class="main-img-mask" src="{{ asset($bestPhoto->photo) }}" />
            <div class="mask"></div>
            <div class="dot-background"></div>
            <div class="main-img" style="background-image: url({{ asset($bestPhoto->photo) }}); background-position: top center; background-size: cover;"></div>
            <div class="container">
                <div class="header-center">
                    <div class="search-content ">
                        <div class="toggle-s-cities toggle-cities">
                            <i class="fas fa-city"></i>
                        </div>
                        <div class="toggle-s-calculation">
                            <i class="fas fa-calculator"></i>
                            <span class="js-price">Розширений пошук</span>
                        </div>
                        <form class="frm-city-category" method="get" action="{{ route('catalog') }}">
                            <fieldset>
                                <multiselect v-model="valueCategory" :options="listCategory" :multiple="true" :close-on-select="false" values="category" placeholder="Оберіть категорію" class="category-dropdown" label="category" track-by="category">
                                    <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
                                </multiselect>
                                <span class="category-select">
                                <i class="fas fa-user-tag"></i>
                            </span>
                            </fieldset>
                            <div class="search-cities-block">
                                <multiselect v-model="valueCity" :options="listCity" :multiple="true" :close-on-select="false" group-values="city" group-label="region" :group-select="true" placeholder="Оберіть місто" track-by="name" label="name" class="city-dropdown">
                                    <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
                                </multiselect>
                                <span class="city-select">
                                <i class="fas fa-city"></i>
                            </span>
                            </div>
                            <div class="search-btn">
                                <button class="s-btn"><i class="fas fa-search"></i></button>
                            </div>
                            <div class="additionally-city ">
                                <span class="close-city-additionally"><i class="fas fa-times"></i></span>
                                <div class="list-a-city">
                                    <ul>
                                        <li v-for="(city, index) of listCity">
                                            <input type="checkbox" :id="'region-' + index" :checked="valueCity.filter(i => city.city.some(k => k.name === i.name)).length" @input="setRegionbox($event, city)" />
                                            <label :for="'region-' + index">
                                                <img :src="city.img_region" :alt="city.region" />
                                                <span>@{{ city.region }}</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="additionally-f-category ">
                                <span class="close-category-additionally"><i class="fas fa-times"></i></span>
                                <div class="list-a-category">
                                    <ul>
                                        <li v-for="(cat, index) of listCategory">
                                            <input type="checkbox" :id="'category-f-' + index" :checked="valueCategory.includes(cat)" @input="setCategorybox($event, cat)"><label :for="'category-' + index">
                                                <img :src="cat.img_category" :alt="cat.category" />
                                                <span>@{{ cat.category }}</span></label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>

                        <div class="additionally-category">
                            <ul class="bottom-listCategory">
                                <li v-for="(cat, index) of listCategory">
                                    <input type="checkbox" :id="'category-' + index" :checked="valueCategory.includes(cat)" @input="setCategorybox($event, cat)"><label :for="'category-' + index">
                                        <img :src="cat.icon_category" :alt="cat.category" />
                                        @{{ cat.category }}</label>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>

            </div>
            <div class="pro-user">
                <span class="show-u-pro"><i class="fas"></i></span>
                <div class="pro-block hide-pro">
                    <h4 class="title-u-pro">Популярні виконавці</h4>
                    <div class="pro-list">

                        @foreach($bestArtists as $bestArtist)
                        <div class="pro-item">
                            <a href="{{ route('user-page', ['user_id' => $bestArtist->id]) }}">
                                {{--<span class="icon-category"><i class="fas fa-camera-retro"></i></span>--}}
                                <span class="raiting-user">{{ \round($bestArtist->likes, 1) }}</span>
                                <img class="img-user" src="{{ $bestArtist->user->image }}">
                                <div class="description-user">
                                    <h3 class="name-user">{{ $bestArtist->name }}</h3>
                                    <ul class="region-user">
                                        <i class="fas fa-map-marker-alt"></i>
                                        @php $cities = $artistCities->where('artist_id', $bestArtist->id) @endphp
                                        @foreach($cities as $city)
                                            <li>{{ $city->name }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </a>
                        </div>
                        @endforeach

                        <div class="all-user">
                            <a href="{{ route('catalog') }}" class="plus-icon"><i class="fas"></i><span class="tooltip-user">Всі виконавці</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <p class="user-img-info">Краще фото тижня<a href="">Фірман Сергій</a></p>
        </div>
        <div class="calculation-modal hide">
            <div class="container">
                <div class="calculation-head">
                    <span class="close-calculation-modal">
                    <i class="fas fa-long-arrow-alt-left"></i>
                </span>
                    <h4 class="title-calc">Розширений пошук виконавців</h4>
                </div>

                <form class="body-calc js-search" action="{{ route('catalog') }}" method="get">
                    <div class="parent-category">
                        <ul>
                            <li>
                            @foreach($partyCategories as $category)
                                <li>
                                    <input type="radio" id="radio{{ $category->id }}" name="party_category" value="{{ $category->id }}">
                                    <label for="radio{{ $category->id }}">
                                        <img src="https://image.flaticon.com/icons/svg/927/927567.svg" alt="">
                                        <span class="check"></span>{{ $category->name }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="category-city">
                        <div class="select-calc js-category">
                            <span class="modal-c-icon"><i class="fas fa-user-tag"></i></span>
                            <multiselect v-model="valueCategory" :options="listCategory" :multiple="true" :close-on-select="false" values="category" placeholder="Оберіть категорію" class="category-dropdown" label="category" track-by="category">
                                <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
                            </multiselect>
                        </div>
                        <div class="select-calc js-city">
                            <span class="modal-c-icon"><i class="fas fa-city"></i></span>
                            <multiselect v-model="valueCity" :options="listCity" :multiple="true" :close-on-select="false" group-values="city" group-label="region" :group-select="true" placeholder="Оберіть місто" track-by="name" label="name" class="city-dropdown">
                                <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
                            </multiselect>
                        </div>
                    </div>
                    <div class="calc-group">
                        <div class="price-calc">
                            <h4 class="title-m-calc">Бюджет на святкування:</h4>
                            <div class="track-container">

                                <div class="track" ref="_vpcTrack"></div>
                                <div class="track-highlight" ref="trackHighlight"></div>
                                <span class="track-btn track1" ref="track1"></span>
                                <span class="track-btn track2" ref="track2"></span>
                                <span class="range-value min" data-price="0">@{{ minValue}} </span> <span class="range-value max" data-price="0">@{{ maxValue }}</span>
                            </div>
                        </div>
                        <div class="rating-calc">
                            <h4 class="title-m-calc">Рейтинг по відгукам:</h4>

                            <div class="rating-smile">
                                <input type="radio" id="star-1" name="rating-01" value="5" data-description="Замечательно" />
                                <label class="star-1" for="star-1"></label>
                                <input type="radio" id="star-2" name="rating-01" value="4" data-description="Хорошо" />
                                <label class="star-2" for="star-2"></label>
                                <input type="radio" id="star-3" name="rating-01" value="3" data-description="Нормально" />
                                <label class="star-3" for="star-3"></label>
                                <input type="radio" id="star-4" name="rating-01" value="2" data-description="Не понравилось" />
                                <label class="star-4" for="star-4"></label>
                                <input type="radio" id="star-5" name="rating-01" value="1" data-description="Ужасно" />
                                <label class="star-5" for="star-5"></label>
                            </div>
                            <div class="raiting-info"><span id="rating">0</span> / <span id="rating-txt">рейтинг</span></div>
                        </div>
                    </div>
                    <div class="other-calc">
                        <ul>
                            <li>
                                <input type="checkbox" id="promocia" name="promocia" checked>
                                <label for="promocia">
                                    <img src="https://image.flaticon.com/icons/svg/927/927567.svg" alt=""><span> Акційні пропозиції</span>
                                </label>
                            </li>
                            <li>
                                <input type="checkbox" id="online" name="online">
                                <label for="online">
                                    <img src="https://image.flaticon.com/icons/svg/927/927567.svg" alt="">
                                    <span>Cейчас онлайн</span></label>
                            </li>
                            <li>
                                <input type="checkbox" id="checkbox1" name="pro">
                                <label for="checkbox1">
                                    <img src="https://image.flaticon.com/icons/svg/927/927567.svg" alt="">
                                    <span>Только PRO</span></label>
                            </li>
                            <li>
                                <input type="checkbox" id="checkbox2" name="top">
                                <label for="checkbox2">
                                    <img src="https://image.flaticon.com/icons/svg/927/927567.svg" alt="">
                                    <span>Только TOP</span></label>
                            </li>


                        </ul>
                    </div>
                    <div class="send-calculation">
                        <button class="send-btn">Шукати</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="user-cataloge-home">
        <div class="container-fluid">
            <div class="title-block">
                <h2>Популярні категорії і виконавці для Вашого свята</h2>
            </div>
            <div class="row">
                @foreach($topCategory as $i => $category)
                <div class="category-info">
                    <a href="{{ route('catalog') . '?category=' .  $category->id}}">
                        <div class="bg-category" style="background: url(/img/bg/home.jpg); background-position: center; background-repeat: no-repeat; background-size: cover;"></div>
                        <h3>{{ $category->category }}</h3>
                        </a>
                </div>
                <div class="user-category">
                    <div id="category{{$i}}" class="carousel slide" data-ride="carousel" data-interval="10000">
                        <div class="carousel-inner ">
                            @php $categoryArtists = $artists->filter(function ($oneArtist) use ($category){ return $oneArtist->artistsCategories->pluck('category_id')->contains($category->id); }) @endphp
                            @foreach($categoryArtists as $index => $artist)
                            <div class="carousel-item @if($index === 0)active @endif">
                                @include('partial.artist', ['artist' => $artist, 'main' => true])
                            </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#category{{$i}}" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#category{{$i}}" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="all-user">
                <a href="{{ route('catalog') }}" class="all-user-btn">Всі виконавці</a>
            </div>
        </div>
    </div>
    <div class="city-cataloge one-version">
        <div class="title-block">
            <h2>Шукаєте виконавця на свято у своєму місті?</h2>
        </div>
        @foreach($artistsByRegion as $regionArtists)
        <div class="city-list">
            <div class="city-item">
                <a href="" class="city-link">
                    <div class="city-bg" style="background: url('/img/bg/home.jpg');
                    background-position: center;
                    background-size: cover;
                    background-repeat: no-repeat;"></div>
                    <h3>{{ $regionArtists->region }} <span class="user-counter">{{ $regionArtists->count }}</span> <span>виконавців</span></h3>
                </a>
            </div>
            @endforeach

        </div>
    </div>

@endsection