<?php

use Illuminate\Database\Seeder;

class ArtistCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artist_cities')->insert([
            ['id' => 1, 'artist_id' => 1, 'city_id' => 1, 'region_id' => 2],
            ['id' => 2, 'artist_id' => 1, 'city_id' => 2, 'region_id' => 3],
            ['id' => 3, 'artist_id' => 2, 'city_id' => 1, 'region_id' => 4],
            ['id' => 4, 'artist_id' => 2, 'city_id' => 2, 'region_id' => 5],
            ['id' => 5, 'artist_id' => 3, 'city_id' => 1, 'region_id' => 6],
            ['id' => 6, 'artist_id' => 3, 'city_id' => 3, 'region_id' => 7],
        ]);
    }
}
