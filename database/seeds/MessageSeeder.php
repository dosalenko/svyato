<?php

use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            ['id' => 1, 'from_id' => 2, 'to_id' => 1, 'text' => 'Привет'],
            ['id' => 2, 'from_id' => 3, 'to_id' => 1, 'text' => 'Привет !'],
            ['id' => 3, 'from_id' => 3, 'to_id' => 1, 'text' => 'Привет !!!'],
        ]);
    }
}
