<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([


            array('id' => '2','region_ua' => 'Автономна Республіка Крим','region_ru' => 'Автономная Республика Крым','region_en' => 'Autonomous Republic of Crimea'),
            array('id' => '3','region_ua' => 'Волинська','region_ru' => 'Волынская','region_en' => 'Volyn'),
            array('id' => '4','region_ua' => 'Вінницька','region_ru' => 'Винницкая','region_en' => 'Vinnitsa'),
            array('id' => '5','region_ua' => 'Дніпропетровська','region_ru' => 'Днепропетровская','region_en' => 'Dnipropetrovsk'),
            array('id' => '6','region_ua' => 'Донецька','region_ru' => 'Донецкая','region_en' => 'Donetsk'),
            array('id' => '7','region_ua' => 'Житомирська','region_ru' => 'Житомирская','region_en' => 'Zhytomyr'),
            array('id' => '8','region_ua' => 'Закарпатська','region_ru' => 'Закарпатская','region_en' => 'Zakarpattya'),
            array('id' => '9','region_ua' => 'Запорізька','region_ru' => 'Запорожская','region_en' => 'Zaporizhia'),
            array('id' => '10','region_ua' => 'Івано-Франківська','region_ru' => 'Ивано-Франковская','region_en' => 'Ivano-Frankivsk'),
            array('id' => '11','region_ua' => 'Київська','region_ru' => 'Киевская','region_en' => 'Kiev region'),
            array('id' => '12','region_ua' => 'Кіровоградська','region_ru' => 'Кировоградская','region_en' => 'Kirovograd'),
            array('id' => '13','region_ua' => 'Львівська','region_ru' => 'Львовская','region_en' => 'Lviv'),
            array('id' => '14','region_ua' => 'Луганська','region_ru' => 'Луганская','region_en' => 'Lugansk'),
            array('id' => '15','region_ua' => 'Миколаївська','region_ru' => 'Николаевская','region_en' => 'Mykolayiv'),
            array('id' => '16','region_ua' => 'Одеська','region_ru' => 'Одесская','region_en' => 'Odessa'),
            array('id' => '17','region_ua' => 'Полтавська','region_ru' => 'Полтавская','region_en' => 'Poltava'),
            array('id' => '18','region_ua' => 'Рівненська','region_ru' => 'Ровенская','region_en' => 'Rivne'),
            array('id' => '19','region_ua' => 'Сумська','region_ru' => 'Сумская','region_en' => 'Sumy'),
            array('id' => '20','region_ua' => 'Тернопільська','region_ru' => 'Тернопольская','region_en' => 'Ternopil'),
            array('id' => '21','region_ua' => 'Харківська','region_ru' => 'Харьковская','region_en' => 'Kharkiv'),
            array('id' => '22','region_ua' => 'Херсонська','region_ru' => 'Херсонская','region_en' => 'Kherson'),
            array('id' => '23','region_ua' => 'Хмельницька','region_ru' => 'Хмельницкая','region_en' => 'Khmelnitsky'),
            array('id' => '24','region_ua' => 'Черкаська','region_ru' => 'Черкасская','region_en' => 'Cherkassy'),
            array('id' => '25','region_ua' => 'Чернівецька','region_ru' => 'Черновицкая','region_en' => 'Chernivtsi'),
            array('id' => '26','region_ua' => 'Чернігівська','region_ru' => 'Черниговская','region_en' => 'Chernihiv')


        ]);
    }
}
