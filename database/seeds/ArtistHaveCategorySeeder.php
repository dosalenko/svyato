<?php

use Illuminate\Database\Seeder;

class ArtistHaveCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artist_have_categories')->insert([
            ['id' => 1, 'artist_id' => 1, 'category_id' => 1],
            ['id' => 2, 'artist_id' => 1, 'category_id' => 2],
            ['id' => 3, 'artist_id' => 1, 'category_id' => 3],
            ['id' => 4, 'artist_id' => 2, 'category_id' => 1],
            ['id' => 5, 'artist_id' => 2, 'category_id' => 2],
            ['id' => 6, 'artist_id' => 2, 'category_id' => 3],
            ['id' => 7, 'artist_id' => 2, 'category_id' => 4],
            ['id' => 8, 'artist_id' => 3, 'category_id' => 1],
            ['id' => 9, 'artist_id' => 3, 'category_id' => 2],
            ['id' => 10, 'artist_id' =>3, 'category_id' => 3],

        ]);
    }
}
