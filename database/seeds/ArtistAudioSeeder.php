<?php

use Illuminate\Database\Seeder;

class ArtistAudioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artist_audios')->insert([
            ['id' => 1, 'artist_id' => 1, 'audio' => 'https://api.soundcloud.com/users/281724041'],
            ['id' => 2, 'artist_id' => 2, 'audio' => 'https://api.soundcloud.com/users/281724042'],
            ['id' => 3, 'artist_id' => 3, 'audio' => 'https://api.soundcloud.com/users/281724043'],
        ]);
    }
}
