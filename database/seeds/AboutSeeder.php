<?php

use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abouts')->insert([
            'id' => '1',
            'html_ua' => '            <h2>asd</h2>
            <p>Lorem <span>Ipsum</span> - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами <a href="#"> Lorem Ipsum</a> в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>
            <ul>
                <li>asd asd sad </li>
                <li>asd asd sad </li>
                <li>asd asd sad </li>
                <li>asd asd <span>asd</span> </li>
            </ul>
            <img class="img-fluid" src="/img/bg/home.jpg">
            <iframe src="https://www.youtube.com/embed/YDYOSjEl3Nk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>',
            'html_ru' => '            <h2>asd</h2>
            <p>Lorem <span>Ipsum</span> - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами <a href="#"> Lorem Ipsum</a> в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>
            <ul>
                <li>asd asd sad </li>
                <li>asd asd sad </li>
                <li>asd asd sad </li>
                <li>asd asd <span>asd</span> </li>
            </ul>
            <img class="img-fluid" src="/img/bg/home.jpg">
            <iframe src="https://www.youtube.com/embed/YDYOSjEl3Nk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>',

            ]);
    }
}
