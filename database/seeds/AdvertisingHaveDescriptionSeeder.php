<?php

use Illuminate\Database\Seeder;

class AdvertisingHaveDescriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertising_have_descriptions')->insert([
            ['id' => 1, 'advertising_id' => 1, 'description_id' => 1, 'status' => 1],
            ['id' => 2, 'advertising_id' => 2, 'description_id' => 1, 'status' => 1],
            ['id' => 3, 'advertising_id' => 3, 'description_id' => 1, 'status' => 1],
            ['id' => 4, 'advertising_id' => 1, 'description_id' => 2, 'status' => 1],
            ['id' => 5, 'advertising_id' => 2, 'description_id' => 2, 'status' => 1],
            ['id' => 6, 'advertising_id' => 3, 'description_id' => 2, 'status' => 1],
            ['id' => 7, 'advertising_id' => 1, 'description_id' => 3, 'status' => 1],
            ['id' => 8, 'advertising_id' => 2, 'description_id' => 3, 'status' => 1],
            ['id' => 9, 'advertising_id' => 3, 'description_id' => 3, 'status' => 1],
            ['id' => 10, 'advertising_id' => 1, 'description_id' => 4, 'status' => 1],
            ['id' => 11, 'advertising_id' => 2, 'description_id' => 4, 'status' => 1],
            ['id' => 12, 'advertising_id' => 3, 'description_id' => 4, 'status' => 1],
            ['id' => 13, 'advertising_id' => 1, 'description_id' => 5, 'status' => 1],
            ['id' => 14, 'advertising_id' => 2, 'description_id' => 5, 'status' => 1],
            ['id' => 15, 'advertising_id' => 3, 'description_id' => 5, 'status' => 1],
            ['id' => 16, 'advertising_id' => 1, 'description_id' => 6, 'status' => 1],
            ['id' => 17, 'advertising_id' => 2, 'description_id' => 6, 'status' => 1],
            ['id' => 18, 'advertising_id' => 3, 'description_id' => 6, 'status' => 1],
            ['id' => 19, 'advertising_id' => 1, 'description_id' => 7, 'status' => 1],
            ['id' => 20, 'advertising_id' => 2, 'description_id' => 7, 'status' => 1],
            ['id' => 21, 'advertising_id' => 3, 'description_id' => 7, 'status' => 1],
            ['id' => 22, 'advertising_id' => 1, 'description_id' => 8, 'status' => 0],
            ['id' => 23, 'advertising_id' => 2, 'description_id' => 8, 'status' => 1],
            ['id' => 24, 'advertising_id' => 3, 'description_id' => 8, 'status' => 1],
            ['id' => 25, 'advertising_id' => 1, 'description_id' => 9, 'status' => 0],
            ['id' => 26, 'advertising_id' => 2, 'description_id' => 9, 'status' => 1],
            ['id' => 27, 'advertising_id' => 3, 'description_id' => 9, 'status' => 1],
            ['id' => 28, 'advertising_id' => 1, 'description_id' => 10, 'status' => 0],
            ['id' => 29, 'advertising_id' => 2, 'description_id' => 10, 'status' => 1],
            ['id' => 30, 'advertising_id' => 3, 'description_id' => 10, 'status' => 1],
            ['id' => 31, 'advertising_id' => 1, 'description_id' => 11, 'status' => 0],
            ['id' => 32, 'advertising_id' => 2, 'description_id' => 11, 'status' => 1],
            ['id' => 33, 'advertising_id' => 3, 'description_id' => 11, 'status' => 1],
            ['id' => 34, 'advertising_id' => 1, 'description_id' => 12, 'status' => 0],
            ['id' => 35, 'advertising_id' => 2, 'description_id' => 12, 'status' => 0],
            ['id' => 36, 'advertising_id' => 3, 'description_id' => 12, 'status' => 1],
            ['id' => 37, 'advertising_id' => 1, 'description_id' => 13, 'status' => 0],
            ['id' => 38, 'advertising_id' => 2, 'description_id' => 13, 'status' => 0],
            ['id' => 39, 'advertising_id' => 3, 'description_id' => 13, 'status' => 1],
        ]);
    }
}
