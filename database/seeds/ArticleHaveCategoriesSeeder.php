<?php

use Illuminate\Database\Seeder;

class ArticleHaveCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles_have_categories')->insert([
            ['id' => '1', 'article_category_id' => 6, 'article_id' => 10],
            ['id' => '2', 'article_category_id' => 5, 'article_id' => 9],
            ['id' => '3', 'article_category_id' => 6, 'article_id' => 8],
            ['id' => '4', 'article_category_id' => 4, 'article_id' => 7],
            ['id' => '5', 'article_category_id' => 3, 'article_id' => 6],
            ['id' => '6', 'article_category_id' => 2, 'article_id' => 5],
            ['id' => '7', 'article_category_id' => 1, 'article_id' => 4],
            ['id' => '8', 'article_category_id' => 4, 'article_id' => 3],
            ['id' => '9', 'article_category_id' => 3, 'article_id' => 2],
            ['id' => '10', 'article_category_id' => 2, 'article_id' => 1],
        ]);
    }
}
