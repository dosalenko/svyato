<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['id' => 1, 'category_ua' => 'Фотографи', 'category_ru' => 'Фотографы','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 2, 'category_ua' => 'Відеооператори', 'category_ru' => 'Видеооператоры','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Відеооператори', 'name_ru' => 'Відеооператор'],
            ['id' => 3, 'category_ua' => 'Музиканти', 'category_ru' => 'Музыканты','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Музикант', 'name_ru' => 'Музыкант'],
            ['id' => 4, 'category_ua' => 'Тамада', 'category_ru' => 'Тамада','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Тамада', 'name_ru' => 'Тамада'],
            ['id' => 5, 'category_ua' => 'Оформлення залів', 'category_ru' => 'Оформление залов','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 6, 'category_ua' => 'Торти та караваї', 'category_ru' => 'Торты и караваи','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 7, 'category_ua' => 'Автомобілі', 'category_ru' => 'Автомобили','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 8, 'category_ua' => 'Весільні сукні', 'category_ru' => 'Свадебные платья','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 9, 'category_ua' => 'Плаття для подружок нареченої', 'category_ru' => 'Платья для подружек невесты','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 10, 'category_ua' => 'Шоу програма', 'category_ru' => 'Шоу-программа','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 11, 'category_ua' => 'Салони краси', 'category_ru' => 'Салоны красоты','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 12, 'category_ua' => 'Запрошення та аксесуари', 'category_ru' => 'Приглашение и аксессуары','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 13, 'category_ua' => 'Феєрверки та спецефекти', 'category_ru' => 'Фейерверки и спецэффекты','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 14, 'category_ua' => 'Перший танець', 'category_ru' => 'Первый танец','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 15, 'category_ua' => 'Фуршетний стіл', 'category_ru' => 'Фуршетный стол','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 16, 'category_ua' => 'Весільні букети', 'category_ru' => 'Свадебные букеты','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф', 'name_ru' => 'Фотограф'],
            ['id' => 17, 'category_ua' => 'РАЦСи', 'category_ru' => 'ЗАГСы','img_category'=> 'https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg', 'icon_category'=> 'https://image.flaticon.com/icons/svg/927/927567.svg', 'name_ua' => 'Фотограф','name_ru' => 'Фотограф'],









        ]);
    }
}
