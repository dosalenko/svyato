<?php

use Illuminate\Database\Seeder;

class ContactsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            'id' => 1,
            'facebook' => 'https://www.facebook.com/',
            'instagram' => 'https://www.instagram.com/?hl=ru',
            'youtube' => 'https://www.youtube.com/',
            'phone' => '+380 000 000 000',
            'viber' => '+380 000 000 000',
            'telegramm' => '+380 000 000 000',
            'email' => 'example@gmail.ccom',
        ]);
    }
}
