<?php

use Illuminate\Database\Seeder;

class ArtistPhotosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artist_photos')->insert([
            ['id' => 1, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 2, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 3, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 4, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 5, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 6, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 7, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 8, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 9, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 10, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 11, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 12, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 13, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 14, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 15, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 16, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 17, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],
            ['id' => 18, 'artist_id' => 1, 'photo' => '/img/bg/home.jpg'],

            ['id' => 19, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 20, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 21, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 22, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 23, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 24, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 25, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 26, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 27, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 28, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 29, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 30, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 31, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 32, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 33, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 34, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 35, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],
            ['id' => 36, 'artist_id' => 2, 'photo' => '/img/bg/home.jpg'],

            ['id' => 37, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 38, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 39, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 40, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 41, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 42, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 43, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 44, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 45, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 46, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 47, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 48, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 49, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 50, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 51, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 52, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 53, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
            ['id' => 54, 'artist_id' => 3, 'photo' => '/img/bg/home.jpg'],
        ]);
    }
}
