<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['id' => 1, 'email' => 'admin', 'password' => bcrypt('secret'), 'confidentality' => 1, 'artist_id' => 1, 'image' => '/img/bg/home.jpg'],
            ['id' => 2, 'email' => 'Ivan', 'password' => bcrypt('secret'), 'confidentality' => 1, 'artist_id' => 2, 'image' => '/img/bg/home.jpg'],
            ['id' => 3, 'email' => 'Roman', 'password' => bcrypt('secret'), 'confidentality' => 1, 'artist_id' => 3, 'image' => '/img/bg/home.jpg'],
            ['id' => 4, 'email' => 'Serhiy', 'password' => bcrypt('secret'), 'confidentality' => 1, 'artist_id' => 4, 'image' => '/img/bg/home.jpg'],
        ]);
    }
}
