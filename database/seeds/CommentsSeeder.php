<?php

use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            ['id' => 1, 'text' => 'Lorem ipsum', 'user_id' => 1, 'created_at' => '2018-12-26 20:10:00','article_id' => 1],
            ['id' => 2, 'text' => 'Lorem ipsum', 'user_id' => 2, 'created_at' => '2018-12-26 20:15:00','article_id' => 1],
            ['id' => 3, 'text' => 'Lorem ipsum', 'user_id' => 3, 'created_at' => '2018-12-26 20:00:00', 'article_id' => 1],
        ]);
    }
}
