<?php

use Illuminate\Database\Seeder;

class ArticleCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('article_categories')->insert([
            ['id' => '1', 'name_ua' => 'фотографія','name_ru' => 'фотография'],
            ['id' => '2', 'name_ua' => 'відео','name_ru' => 'видео'],
            ['id' => '3', 'name_ua' => 'ресторан','name_ru' => 'ресторан'],
            ['id' => '4', 'name_ua' => 'тамада','name_ru' => 'тамада'],
            ['id' => '5', 'name_ua' => 'танець','name_ru' => 'танец'],
            ['id' => '6', 'name_ua' => 'церемонія','name_ru' => 'церемония'],
        ]);
    }
}
