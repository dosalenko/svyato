<?php

use Illuminate\Database\Seeder;

class ArtistVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artist_videos')->insert([
            ['id' => 1, 'artist_id' => 1, 'video' => 'https://www.youtube.com/embed/96gvvczIqAQ'],
            ['id' => 2, 'artist_id' => 1, 'video' => 'https://vimeo.com/257923996'],
            ['id' => 3, 'artist_id' => 2, 'video' => 'https://www.youtube.com/embed/96gvvczIqAQ'],
            ['id' => 4, 'artist_id' => 2, 'video' => 'https://vimeo.com/257923996'],
            ['id' => 5, 'artist_id' => 3, 'video' => 'https://www.youtube.com/embed/96gvvczIqAQ'],
            ['id' => 6, 'artist_id' => 3, 'video' => 'https://www.youtube.com/embed/96gvvczIqAQ'],
            ['id' => 7, 'artist_id' => 1, 'video' => 'https://www.youtube.com/embed/96gvvczIqAQ'],
            ['id' => 8, 'artist_id' => 3, 'video' => 'https://vimeo.com/257923996'],
            ['id' => 9, 'artist_id' => 1, 'video' => 'https://www.youtube.com/embed/96gvvczIqAQ'],
        ]);
    }
}
