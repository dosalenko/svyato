<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ContactsSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(DescriptionSeeder::class);
        $this->call(AdvertisingSeeder::class);
        $this->call(AdvertisingHaveDescriptionSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(ArtistSeeder::class);
        $this->call(ArtistCitySeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ArtistPhotosSeeder::class);
        $this->call(ArtistVideoSeeder::class);
        $this->call(ArtistAudioSeeder::class);
        $this->call(ArtistFeedbackSeeder::class);
        $this->call(MessageSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(ArticleCategoriesSeeder::class);
        $this->call(ArticleHaveCategoriesSeeder::class);
        $this->call(CommentsSeeder::class);
        $this->call(SeoSeeder::class);
        $this->call(ArtistHaveCategorySeeder::class);
        $this->call(PartyCategorySeeder::class);
        $this->call(ArtistHavePartyCategorySeeder::class);
        $this->call(ArtistHaveLikesSeeder::class);
    }
}
