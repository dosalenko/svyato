<?php

use Illuminate\Database\Seeder;

class AdvertisingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisings')->insert([
            ['id' => 1, 'year_price' => 990, 'month_price' => 99, 'active_pro' => 0, 'name' => 'Start'],
            ['id' => 2, 'year_price' => 980, 'month_price' => 98, 'active_pro' => 0, 'name' => 'Sdandart'],
            ['id' => 3, 'year_price' => 970, 'month_price' => 97, 'active_pro' => 1, 'name' => 'Recommend'],
        ]);
    }
}
