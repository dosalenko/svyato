<?php

use Illuminate\Database\Seeder;

class PartyCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('party_categories')->insert([
            ['id' => 1, 'name' => 'Весілля'],
            ['id' => 2, 'name' => 'Корпоративи'],
            ['id' => 3, 'name' => 'День народження'],
        ]);
    }
}
