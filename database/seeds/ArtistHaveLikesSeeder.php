<?php

use Illuminate\Database\Seeder;

class ArtistHaveLikesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artists_have_likes')->insert([
            ['id' => 1, 'artist_id' => 1, 'user_id' => 2, 'created_at' => '2018-12-26 20:10:00', 'updated_at' => '2018-12-26 22:10:00',],
            ['id' => 2, 'artist_id' => 1, 'user_id' => 1, 'created_at' => '2018-12-26 20:10:00', 'updated_at' => '2018-12-26 22:10:00',],
            ['id' => 3, 'artist_id' => 1, 'user_id' => 3, 'created_at' => '2018-12-26 20:10:00', 'updated_at' => '2018-12-26 22:10:00',],
            ['id' => 4, 'artist_id' => 2, 'user_id' => 1, 'created_at' => '2018-12-26 20:10:00', 'updated_at' => '2018-12-26 22:10:00',],
            ['id' => 5, 'artist_id' => 2, 'user_id' => 3, 'created_at' => '2018-12-26 20:10:00', 'updated_at' => '2018-12-26 22:10:00',],
            ['id' => 6, 'artist_id' => 3, 'user_id' => 2, 'created_at' => '2018-12-26 20:10:00', 'updated_at' => '2018-12-26 22:10:00',],
        ]);
    }
}
