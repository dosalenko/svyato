<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('youtube')->nullable();
            $table->string('vimeo')->nullable();
            $table->string('viber')->nullable();
            $table->string('facebook-messenger')->nullable();
            $table->string('telegramm')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('region_id')->nullable();
            $table->string('category_id')->nullable();
            $table->string('price')->nullable();
            $table->string('likes_cnt')->nullable();
            $table->string('show_on_main')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artists');
    }
}
