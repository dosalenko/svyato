<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('youtube')->nullable();
            $table->string('vimeo')->nullable();
            $table->string('viber')->nullable();
            $table->string('phone')->nullable();
            $table->string('facebook-messenger')->nullable();
            $table->string('telegramm')->nullable();
            $table->string('email')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('price')->default('0');
            $table->boolean('is_pro')->default(0);
            $table->boolean('is_top')->default(0);
            $table->integer('likes_cnt')->default(0);
            $table->integer('user_id');
            $table->integer('region_id')->default(0);
            $table->integer('city_id')->default(0);
            $table->integer('category_id')->default(0);
            $table->integer('old_price')->nullable();
            $table->integer('add_category_id')->default(0);
            $table->integer('advertising_id')->default(0);
            $table->integer('show_on_main')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
