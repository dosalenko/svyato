<?php

use Illuminate\Database\Migrations\Migration;

class AddFieldstouserColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('name_ua')->default('');
            $table->string('name_ru')->default('');
            $table->longText('description_ua')->default('');
            $table->longText('description_ru')->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
