<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_ua');
            $table->string('category_ru');
            $table->string('img_category')->default('https://images.pexels.com/photos/1055272/pexels-photo-1055272.jpeg');
            $table->string('icon_category')->default('https://image.flaticon.com/icons/svg/927/927567.svg');
            $table->string('name_ua');
            $table->string('name_ru');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
